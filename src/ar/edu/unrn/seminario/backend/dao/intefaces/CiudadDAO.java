package ar.edu.unrn.seminario.backend.dao.intefaces;

import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Ciudad;

import java.util.List;

public interface CiudadDAO {

    Ciudad findByNombre(String nombre) throws ErrorDeAccesoADatosException;

    List<Ciudad> findAll() throws ErrorDeAccesoADatosException;

    void create(String nombre) throws ErrorDeAccesoADatosException;

    void update(String nombreAntiguo, String nuevoNombre) throws ErrorDeAccesoADatosException;

}
