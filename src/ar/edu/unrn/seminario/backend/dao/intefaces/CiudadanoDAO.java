package ar.edu.unrn.seminario.backend.dao.intefaces;


import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Ciudadano;

import java.util.List;

public interface CiudadanoDAO {

    void create(Ciudadano ciudadano) throws ErrorDeAccesoADatosException;

    void update(String dni, Ciudadano ciudadano) throws ErrorDeAccesoADatosException;

    List<Ciudadano> listarCiudadanos() throws ErrorDeAccesoADatosException;

    Ciudadano findByDNI(String dni) throws ErrorDeAccesoADatosException;
}
