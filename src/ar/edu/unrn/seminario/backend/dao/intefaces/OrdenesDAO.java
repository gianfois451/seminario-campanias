package ar.edu.unrn.seminario.backend.dao.intefaces;

import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.OrdenDeRetiro;

import java.util.List;

public interface OrdenesDAO {

    void saveOrdenDeRetiro(OrdenDeRetiro ordenDeRetiro) throws ErrorDeAccesoADatosException;

    void reasignarOrdenDeRetiro(Long numeroDePedido, String dniPersonal) throws ErrorDeAccesoADatosException;

    List<OrdenDeRetiro> listarOrdenesPorCampania(Long codigoDeCampania) throws ErrorDeAccesoADatosException;

    OrdenDeRetiro findByNumero(Long numeroDePedido) throws ErrorDeAccesoADatosException;

    void update(OrdenDeRetiro ordenDeRetiro) throws ErrorDeAccesoADatosException;


}
