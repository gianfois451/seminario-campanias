package ar.edu.unrn.seminario.backend.dao.intefaces;


import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Campania;

import java.util.List;

public interface CampaniaDAO {

    void create(Campania campania) throws ErrorDeAccesoADatosException;

    void update(Campania campania) throws ErrorDeAccesoADatosException;

    Campania findByCodigo(Long codigo) throws ErrorDeAccesoADatosException;

    List<Campania> findAll() throws ErrorDeAccesoADatosException;

}
