package ar.edu.unrn.seminario.backend.dao.intefaces;

import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.LugarDeDonacion;

import java.util.List;

public interface LugaresDAO {

    LugarDeDonacion findByNombre(String nombre) throws ErrorDeAccesoADatosException;
    List<LugarDeDonacion> findAllLugares() throws ErrorDeAccesoADatosException;
    List<LugarDeDonacion> findAllLugaresByCampania(Long codigoCampania) throws ErrorDeAccesoADatosException;
    void crearLugarDeDonacion(LugarDeDonacion lugarDeDonacion) throws ErrorDeAccesoADatosException;
    void updateLugarDeDonacion(String nombreOriginal, LugarDeDonacion lugarDeDonacion) throws ErrorDeAccesoADatosException;
    void asignarLugarACampania(String nombreLugar, Long codigoCampania) throws ErrorDeAccesoADatosException;
    void eliminarLugarDeCampania(String nombreLugar, Long codigoCampania) throws ErrorDeAccesoADatosException;

}
