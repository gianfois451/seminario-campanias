package ar.edu.unrn.seminario.backend.dao.intefaces;


import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Persona;

import java.util.List;

public interface PersonaDAO {

    Persona findByDNI(String dni) throws ErrorDeAccesoADatosException;

    List<Persona> findAll() throws ErrorDeAccesoADatosException;

    List<Persona> findAllNoCiudadano() throws ErrorDeAccesoADatosException;

    List<Persona> findAllNoPersonal() throws ErrorDeAccesoADatosException;

}
