package ar.edu.unrn.seminario.backend.dao.intefaces;

import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Institucion;

import java.util.List;

public interface InstitucionDAO {

    void create(Institucion institucion) throws ErrorDeAccesoADatosException;

    Institucion findByNombre(String nombre) throws ErrorDeAccesoADatosException;

    void update(String nombre, Institucion institucion) throws ErrorDeAccesoADatosException;

    List<Institucion> findAll() throws ErrorDeAccesoADatosException;

}
