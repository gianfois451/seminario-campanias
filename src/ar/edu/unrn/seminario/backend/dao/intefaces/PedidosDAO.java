package ar.edu.unrn.seminario.backend.dao.intefaces;

import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Ciudadano;
import ar.edu.unrn.seminario.backend.modelo.PedidoDeRetiro;

import java.util.List;

public interface PedidosDAO {

    PedidoDeRetiro find(Long numeroDeRetiro) throws ErrorDeAccesoADatosException;

    Long create(PedidoDeRetiro pedidoDeRetiro) throws ErrorDeAccesoADatosException;

    void update(PedidoDeRetiro pedidoDeRetiro) throws ErrorDeAccesoADatosException;

    List<PedidoDeRetiro> findByCampania(Long codigoCampania) throws ErrorDeAccesoADatosException;

    List<PedidoDeRetiro> findByCiudadano(Ciudadano ciudadano) throws ErrorDeAccesoADatosException;

    void eliminarPedido(Long numeroDeRetiro) throws ErrorDeAccesoADatosException;

}
