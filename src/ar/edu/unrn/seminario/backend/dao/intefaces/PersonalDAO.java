package ar.edu.unrn.seminario.backend.dao.intefaces;

import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Institucion;
import ar.edu.unrn.seminario.backend.modelo.Persona;
import ar.edu.unrn.seminario.backend.modelo.Personal;

import java.util.List;

public interface PersonalDAO {

    Personal findByDNI(String dni) throws ErrorDeAccesoADatosException;

    List<Personal> findByInstitucion(String institucion) throws ErrorDeAccesoADatosException;

    List<Personal> listarPersonalDisponiblePorNumeroDePedido(Long numeroPedido) throws ErrorDeAccesoADatosException;

    void agregarAInstitucion(Personal personal, String institucion) throws ErrorDeAccesoADatosException;

    void eliminarDeInstitucion(String dni) throws ErrorDeAccesoADatosException;

    void update(String dniOriginal, Personal personal) throws ErrorDeAccesoADatosException;

}
