package ar.edu.unrn.seminario.backend.dao.intefaces;

import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Donacion;

import java.util.List;

public interface DonacionesDAO {

    void guardarDonacionByPedido(Donacion donacion, Long numeroDePedido) throws ErrorDeAccesoADatosException;

    void guardarDonacion(Donacion donacion, Long codigoCampania) throws ErrorDeAccesoADatosException;


    List<Donacion> listarDonacionesPorCampania(Long codigoDeCampania) throws ErrorDeAccesoADatosException;

    void eliminarDonacion(Long codigoDeDonacion) throws ErrorDeAccesoADatosException;


}
