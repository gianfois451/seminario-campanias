package ar.edu.unrn.seminario.backend.dao.intefaces;

import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Visita;

public interface VisitasDAO {

    Long guardarVisita(Visita visita, Long numeroDePedido) throws ErrorDeAccesoADatosException;

    void modificarVisita(Visita visita) throws ErrorDeAccesoADatosException;

    void eliminarVisita(Long numeroDeVisita) throws ErrorDeAccesoADatosException;

}
