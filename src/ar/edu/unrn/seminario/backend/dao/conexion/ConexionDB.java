package ar.edu.unrn.seminario.backend.dao.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionDB {

    private static final String USER = "root";
    private static final String PASSWORD = "root";

    public static Connection abrir() throws SQLException {
        return  DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/campanias?useSSL=false", USER, PASSWORD);
    }

    public static Connection abrir(boolean autoCommit) throws SQLException {
        Connection connection = abrir();
        connection.setAutoCommit(autoCommit);
        return connection;
    }

}
