package ar.edu.unrn.seminario.backend.dao.implementacionJDBC;

import ar.edu.unrn.seminario.backend.dao.conexion.ConexionDB;
import ar.edu.unrn.seminario.backend.dao.intefaces.OrdenesDAO;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.exception.ErrorDeBaseDeDatosException;
import ar.edu.unrn.seminario.backend.modelo.*;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class OrdenesDAOJdbc implements OrdenesDAO {

    private static final String TABLA = "ordenes";
    private static final String INSERT = "INSERT INTO " + TABLA + " (pedido_id, estado, fecha, personal_id) VALUES (?,?,?,?)";
    private static final String REASIGNAR = "UPDATE " + TABLA + " o SET personal_id = " +
            "(SELECT per.id FROM personal per JOIN personas p ON per.persona_id = p.id WHERE p.dni = ?)" +
            " WHERE o.pedido_id = ?";


    private static final String SELECT = "select o.pedido_id as ped_id, o.estado, o.fecha, ped.id, ped.descripcion, ped.fecha_emision, ped.requiere_vehiculo," +
            " per.nombre, per.apellido, per.dni, d.calle, d.numero, d.latitud, d.longitud, d.ciudad_id, " +
            " ciudad.nombre as nombre_ciudad, persona_personal.nombre as nombre_personal, persona_personal.apellido " +
            " as apellido_personal, persona_personal.dni as dni_personal, p.email, p.foto" +
            " from ordenes o JOIN pedidos ped on o.pedido_id = ped.id JOIN ciudadanos c on ped.ciudadano_id = c.id" +
            " join personas per on c.persona_id = per.id join direcciones d on per.direccion_id = d.id join ciudades ciudad" +
            " on d.ciudad_id = ciudad.id JOIN personal p on o.personal_id = p.id JOIN personas persona_personal" +
            " ON persona_personal.id = p.persona_id";

    private static final String FIND = SELECT + " Where ped.id = ?";
    private static final String UPDATE_ESTADO = "UPDATE ordenes SET estado = ? WHERE pedido_id = ?";
    private static final String SELECT_BY_CAMPANIA = SELECT + " WHERE ped.campania_id = ?";

    private static final String SELECT_VISITAS = "SELECT id, observacion, fecha from visitas where orden_id = ?";

    private OrdenDeRetiro resultAEntidad(ResultSet resultSet) throws SQLException {
        return new OrdenDeRetiro(
                resultSet.getLong("ped_id"),
                resultSet.getString("estado"),
                new PedidoDeRetiro(
                        resultSet.getLong("id"),
                        null,
                        resultSet.getDate("fecha_emision"),
                        resultSet.getString("descripcion"),
                        resultSet.getInt("requiere_vehiculo") == 1,
                        new Ciudadano(
                                new Persona(
                                        resultSet.getString("nombre"),
                                        resultSet.getString("apellido"),
                                        new Direccion(
                                                resultSet.getString("calle"),
                                                resultSet.getInt("numero"),
                                                new Ciudad(
                                                        resultSet.getLong("ciudad_id"),
                                                        resultSet.getString("nombre_ciudad")
                                                ),
                                                resultSet.getDouble("latitud"),
                                                resultSet.getDouble("longitud")

                                        ),
                                        resultSet.getString("dni")
                                )
                        )
                ),
                new Personal(
                        resultSet.getString("nombre_personal"),
                        resultSet.getString("apellido_personal"),
                        null,
                        resultSet.getString("dni_personal"),
                        resultSet.getString("email"),
                        resultSet.getString("foto")
                )
        );
    }

    private Visita resultAVisita(ResultSet resultSet) throws SQLException {
        return new Visita(
                resultSet.getLong("id"),
                resultSet.getDate("fecha"),
                resultSet.getString("observacion"));
    }

    ;

    @Override
    public void saveOrdenDeRetiro(OrdenDeRetiro ordenDeRetiro) throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(INSERT);
        ) {
            statement.setLong(1, ordenDeRetiro.getPedidoDeRetiro().getId());
            statement.setString(2, ordenDeRetiro.getEstadoString());
            statement.setDate(3, new Date(ordenDeRetiro.getFechaEmision().getTime()));
            statement.setLong(4, ordenDeRetiro.getPersonalAsignado().getId());
            statement.execute();
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void reasignarOrdenDeRetiro(Long numeroDePedido, String dniPersonal) throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(REASIGNAR);
        ) {
            statement.setString(1, dniPersonal);
            statement.setLong(2, numeroDePedido);
            statement.execute();
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public List<OrdenDeRetiro> listarOrdenesPorCampania(Long codigoDeCampania) throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(SELECT_BY_CAMPANIA);
        ) {
            statement.setLong(1, codigoDeCampania);
            try (
                    ResultSet resultSet = statement.executeQuery();
            ) {
                List<OrdenDeRetiro> list = new LinkedList<>();
                while (resultSet.next()) {
                   OrdenDeRetiro ordenDeRetiro = resultAEntidad(resultSet);

                    try (
                        PreparedStatement visitasStatement = connection.prepareStatement(SELECT_VISITAS);
                    ) {
                        visitasStatement.setLong(1, ordenDeRetiro.getId());
                        try (ResultSet resultSetVisitas = visitasStatement.executeQuery()) {
                            LinkedList<Visita> visitas = new LinkedList<>();
                            while (resultSetVisitas.next()){
                                visitas.add(resultAVisita(resultSetVisitas));
                            }
                            ordenDeRetiro.setVisitas(visitas);
                        }
                    }

                   list.add(ordenDeRetiro);

                }
                return list;
            }
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public OrdenDeRetiro findByNumero(Long numeroDePedido) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(FIND);
        ) {
            statement.setLong(1, numeroDePedido);
            try (
                    ResultSet resultSet = statement.executeQuery();
            ) {
                if (resultSet.next()) {
                    OrdenDeRetiro orden = resultAEntidad(resultSet);
                    try (
                            PreparedStatement visitasStatement = connection.prepareStatement(SELECT_VISITAS);
                    ) {
                        visitasStatement.setLong(1, orden.getId());
                        try (ResultSet resultSetVisitas = visitasStatement.executeQuery()) {
                            LinkedList<Visita> visitas = new LinkedList<>();
                            while (resultSetVisitas.next()){
                                visitas.add(resultAVisita(resultSetVisitas));
                            }
                            orden.setVisitas(visitas);
                        }
                    }
                    return orden;
                }
                return null;
            }
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void update(OrdenDeRetiro ordenDeRetiro) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(UPDATE_ESTADO);
        ) {

            statement.setString(1, ordenDeRetiro.getEstadoString());
            statement.setLong(2, ordenDeRetiro.getId());
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }
}
