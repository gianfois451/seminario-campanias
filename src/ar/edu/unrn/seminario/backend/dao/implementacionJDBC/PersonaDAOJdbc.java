package ar.edu.unrn.seminario.backend.dao.implementacionJDBC;

import ar.edu.unrn.seminario.backend.dao.conexion.ConexionDB;
import ar.edu.unrn.seminario.backend.dao.intefaces.PersonaDAO;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.exception.ErrorDeBaseDeDatosException;
import ar.edu.unrn.seminario.backend.modelo.Ciudad;
import ar.edu.unrn.seminario.backend.modelo.Direccion;
import ar.edu.unrn.seminario.backend.modelo.Persona;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class PersonaDAOJdbc implements PersonaDAO {
    private static final String TABLA = "personas";
    private static final String SELECT = "SELECT p.*, d.calle, d.numero, d.latitud, d.longitud, d.ciudad_id," +
            "c.nombre as nombre_ciudad FROM " + TABLA + " p JOIN direcciones d ON p.direccion_id = d.id JOIN ciudades c ON c.id = d.ciudad_id ";
    private static final String FIND_ALL_NO_CIUDADANOS = SELECT + " where p.id not in (select c.persona_id from ciudadanos c)";
    private static final String FIND_ALL_NO_PERSONAL = SELECT + " where p.id not in (select persona_id from personal)";
    private static final String FIND_BY_DNI = SELECT + " where p.dni = ?";

    private Persona resultAEntidad(ResultSet rs) throws SQLException {
        return new Persona(
                rs.getLong("id"),
                rs.getString("nombre"),
                rs.getString("apellido"),
                new Direccion(
                        rs.getLong("direccion_id"),
                        rs.getString("calle"),
                        rs.getInt("numero"),
                        new Ciudad(
                                rs.getLong("ciudad_id"),
                                rs.getString("nombre_ciudad")
                        ),
                        rs.getDouble("latitud"),
                        rs.getDouble("longitud")

                ),
                rs.getString("dni")
        );
    }

    @Override
    public Persona findByDNI(String dni) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(FIND_BY_DNI);
        ) {
            statement.setString(1, dni);
            try (
                    ResultSet resultSet = statement.executeQuery();
            ) {
                if (resultSet.first()) {
                    return this.resultAEntidad(resultSet);
                }
                return null;
            }
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public List<Persona> findAll() throws ErrorDeAccesoADatosException {
        return this.findList(SELECT);
    }

    @Override
    public List<Persona> findAllNoCiudadano() throws ErrorDeAccesoADatosException {
        return this.findList(FIND_ALL_NO_CIUDADANOS);
    }

    @Override
    public List<Persona> findAllNoPersonal() throws ErrorDeAccesoADatosException {
        return this.findList(FIND_ALL_NO_PERSONAL);
    }

    public List<Persona> findList(String query) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query);
        ) {
            List<Persona> list = new LinkedList<>();
            while (resultSet.next()) {
                Persona persona = this.resultAEntidad(resultSet);
                list.add(persona);
            }
            return list;
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

}
