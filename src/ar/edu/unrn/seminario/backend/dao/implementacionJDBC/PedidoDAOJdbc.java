package ar.edu.unrn.seminario.backend.dao.implementacionJDBC;

import ar.edu.unrn.seminario.backend.dao.intefaces.PedidosDAO;
import ar.edu.unrn.seminario.backend.dao.conexion.ConexionDB;
import ar.edu.unrn.seminario.backend.exception.DatoRepetidoException;
import ar.edu.unrn.seminario.backend.exception.EliminacionNoPermitidaException;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.exception.ErrorDeBaseDeDatosException;
import ar.edu.unrn.seminario.backend.modelo.Ciudadano;
import ar.edu.unrn.seminario.backend.modelo.PedidoDeRetiro;
import ar.edu.unrn.seminario.backend.modelo.Persona;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class PedidoDAOJdbc implements PedidosDAO {

    private static final String TABLA = "pedidos";

    private static final String SELECT = "SELECT p.id, p.fecha_emision, p.descripcion, p.requiere_vehiculo, per.nombre, per.apellido, per.dni" +
            " FROM " + TABLA + " p JOIN ciudadanos c ON c.id = p.ciudadano_id JOIN personas per ON per.id = c.persona_id";

    private static final String FIND_BY_ID = SELECT + " WHERE p.id = ?";

    private static final String SELECT_BY_CAMPANIA = SELECT + " WHERE campania_id = ? and p.id not in (select pedido_id from ordenes)";
    private static final String SELECT_BY_CIUDADANO = SELECT + " WHERE ciudadano_id = ?";

    private static final String INSERT = "INSERT INTO "+TABLA+" (fecha_emision, descripcion, requiere_vehiculo, ciudadano_id, campania_id ) VALUES (?,?,?,?,?)";
    private static final String UPDATE = "UPDATE "+TABLA+" SET descripcion = ?, requiere_vehiculo = ?, ciudadano_id = " +
            "(SELECT c.id FROM ciudadanos c JOIN personas p ON c.persona_id = p.id WHERE p.dni = ?) WHERE id = ?";

    private static final String DELETE = "DELETE FROM "+TABLA+" where id = ?";


    private PedidoDeRetiro resultAEntidad(ResultSet resultSet) throws SQLException {
        return new PedidoDeRetiro(
                resultSet.getLong("id"),
                null,
                resultSet.getDate("fecha_emision"),
                resultSet.getString("descripcion"),
                resultSet.getInt("requiere_vehiculo") == 1,
                new Ciudadano(
                        new Persona(
                                resultSet.getString("nombre"),
                                resultSet.getString("apellido"),
                                null,
                                resultSet.getString("dni")
                        )
                ));
    }

    @Override
    public PedidoDeRetiro find(Long numeroDeRetiro) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(FIND_BY_ID);
        ) {
            statement.setLong(1, numeroDeRetiro);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return this.resultAEntidad(resultSet);
                }
            }
            return null;
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public Long create(PedidoDeRetiro pedidoDeRetiro) throws ErrorDeBaseDeDatosException {
        try (
            Connection connection = ConexionDB.abrir();
            PreparedStatement statement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        ) {
            statement.setDate(1, new Date(pedidoDeRetiro.getFechaEmision().getTime()));
            statement.setString(2, pedidoDeRetiro.getDescripcion());
            statement.setInt(3, pedidoDeRetiro.isRequiereVehiculoDeCarga() ? 1 : 0);
            statement.setLong(4, pedidoDeRetiro.getCiudadano().getId());
            statement.setLong(5, pedidoDeRetiro.getCampania().getId());
            statement.execute();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    pedidoDeRetiro.setId(keys.getLong(1));
                    return pedidoDeRetiro.getId();
                }
            }
            return null;
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void update(PedidoDeRetiro pedidoDeRetiro) throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(UPDATE);
        ) {
            statement.setString(1, pedidoDeRetiro.getDescripcion());
            statement.setInt(2, pedidoDeRetiro.isRequiereVehiculoDeCarga() ? 1 : 0);
            statement.setString(3, pedidoDeRetiro.getCiudadano().getPersona().getDni());
            statement.setLong(4, pedidoDeRetiro.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public List<PedidoDeRetiro> findByCampania(Long codigoCampania) throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(SELECT_BY_CAMPANIA);
        ) {
            List<PedidoDeRetiro> list = new LinkedList<>();
            statement.setLong(1, codigoCampania);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    PedidoDeRetiro pedidoDeRetiro = this.resultAEntidad(resultSet);
                    list.add(pedidoDeRetiro);
                }
            }
            return list;
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public List<PedidoDeRetiro> findByCiudadano(Ciudadano ciudadano) throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(SELECT_BY_CIUDADANO);
        ) {
            List<PedidoDeRetiro> list = new LinkedList<>();
            statement.setLong(1, ciudadano.getId());
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    PedidoDeRetiro pedidoDeRetiro = this.resultAEntidad(resultSet);
                    list.add(pedidoDeRetiro);
                }
            }
            return list;
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void eliminarPedido(Long numeroDeRetiro) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(DELETE);
        ) {
            statement.setLong(1, numeroDeRetiro);
            statement.execute();
        } catch (SQLIntegrityConstraintViolationException e) {
            throw new EliminacionNoPermitidaException("No se puede eliminar el pedido ya que posee una orden asignada.");
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }


}
