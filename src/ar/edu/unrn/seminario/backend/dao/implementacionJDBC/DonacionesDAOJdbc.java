package ar.edu.unrn.seminario.backend.dao.implementacionJDBC;

import ar.edu.unrn.seminario.backend.dao.conexion.ConexionDB;
import ar.edu.unrn.seminario.backend.dao.intefaces.DonacionesDAO;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.exception.ErrorDeBaseDeDatosException;
import ar.edu.unrn.seminario.backend.modelo.Ciudad;
import ar.edu.unrn.seminario.backend.modelo.Direccion;
import ar.edu.unrn.seminario.backend.modelo.Donacion;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class DonacionesDAOJdbc implements DonacionesDAO {
    private static final String TABLA = "donaciones";
    private static final String INSERT_BY_PEDIDO = "Insert into " + TABLA + " (fecha, descripcion, direccion_id, campania_id, orden_origen_id) values (?,?,?,(" +
            "select c.id from pedidos p JOIN campanias c on p.campania_id = c.id where p.id = ?), ?)";

    private static final String INSERT = "Insert into " + TABLA + " (fecha, descripcion, direccion_id, campania_id) values (?,?,?,?)";


    private static final String SELECT = "select d.id, d.fecha, d.descripcion, dir.calle, dir.numero, " +
            " dir.latitud, dir.longitud, dir.ciudad_id, c.nombre from donaciones d " +
            " JOIN direcciones dir on d.direccion_id = dir.id" +
            " JOIN ciudades c on dir.ciudad_id = c.id";

    private static final String SELECT_BY_CAMPANIA = SELECT + " WHERE d.campania_id = ?";

    private static final String DELETE = "Delete from " + TABLA + " where id = ?";

    private static final String INSERT_DIRECCION = "INSERT INTO direcciones (calle, numero, latitud, longitud, ciudad_id) " +
            "VALUES (?,?,?,?,?)";

    private Donacion resultAEntidad(ResultSet resultSet) throws SQLException {
        return new Donacion(
                resultSet.getLong("id"),
                resultSet.getDate("fecha"),
                resultSet.getString("descripcion"),
                new Direccion(
                        resultSet.getString("calle"),
                        resultSet.getInt("numero"),
                        new Ciudad(
                                resultSet.getLong("ciudad_id"),
                                resultSet.getString("nombre")
                        ),
                        resultSet.getDouble("latitud"),
                        resultSet.getDouble("longitud")

                )
        );
    }

    @Override
    public void guardarDonacionByPedido(Donacion donacion, Long numeroDePedido) throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir(false);
                PreparedStatement statement = connection.prepareStatement(INSERT_BY_PEDIDO, Statement.RETURN_GENERATED_KEYS);
        ) {
            if (donacion.getOrigen().getId() == null) {
                Direccion direccion = donacion.getOrigen();
                try (PreparedStatement direccionStatement = connection.prepareStatement(INSERT_DIRECCION, Statement.RETURN_GENERATED_KEYS)) {
                    direccionStatement.setString(1, direccion.getCalle());
                    direccionStatement.setInt(2, direccion.getNumero());
                    direccionStatement.setDouble(3, direccion.getLatitud());
                    direccionStatement.setDouble(4, direccion.getLongitud());
                    direccionStatement.setLong(5, direccion.getCiudad().getId());
                    direccionStatement.execute();
                    try (ResultSet keys = direccionStatement.getGeneratedKeys()) {
                        if (keys.next()) {
                            direccion.setId(keys.getLong(1));
                        }
                    }
                } catch (SQLException e) {
                    throw new ErrorDeBaseDeDatosException("direcciones", e.getMessage());
                }
            }
            statement.setDate(1, new Date(donacion.getFecha().getTime()));
            statement.setString(2, donacion.getDescripcion());
            statement.setLong(3, donacion.getOrigen().getId());
            statement.setLong(4, numeroDePedido);
            statement.setLong(5, numeroDePedido);
            statement.executeUpdate();
            connection.commit();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    donacion.setId(keys.getLong(1));
                }
            }
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void guardarDonacion(Donacion donacion, Long codigoCampania) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir(false);
                PreparedStatement statement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        ) {
            if (donacion.getOrigen().getId() == null) {
                Direccion direccion = donacion.getOrigen();
                try (PreparedStatement direccionStatement = connection.prepareStatement(INSERT_DIRECCION, Statement.RETURN_GENERATED_KEYS)) {
                    direccionStatement.setString(1, direccion.getCalle());
                    direccionStatement.setInt(2, direccion.getNumero());
                    direccionStatement.setDouble(3, direccion.getLatitud());
                    direccionStatement.setDouble(4, direccion.getLongitud());
                    direccionStatement.setLong(5, direccion.getCiudad().getId());
                    direccionStatement.execute();
                    try (ResultSet keys = direccionStatement.getGeneratedKeys()) {
                        if (keys.next()) {
                            direccion.setId(keys.getLong(1));
                        }
                    }
                } catch (SQLException e) {
                    throw new ErrorDeBaseDeDatosException("direcciones", e.getMessage());
                }
            }
            statement.setDate(1, new Date(donacion.getFecha().getTime()));
            statement.setString(2, donacion.getDescripcion());
            statement.setLong(3, donacion.getOrigen().getId());
            statement.setLong(4, codigoCampania);
            statement.executeUpdate();
            connection.commit();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    donacion.setId(keys.getLong(1));
                }
            }
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public List<Donacion> listarDonacionesPorCampania(Long codigoDeCampania) throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(SELECT_BY_CAMPANIA);
        ) {
            statement.setLong(1, codigoDeCampania);
            try (
                    ResultSet resultSet = statement.executeQuery();
            ) {
                List<Donacion> list = new LinkedList<>();
                while (resultSet.next()) {
                    Donacion donacion = resultAEntidad(resultSet);
                    list.add(donacion);

                }
                return list;
            }
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void eliminarDonacion(Long codigoDeDonacion) throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(DELETE);
        ) {
            statement.setLong(1, codigoDeDonacion);
            statement.execute();
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }
}
