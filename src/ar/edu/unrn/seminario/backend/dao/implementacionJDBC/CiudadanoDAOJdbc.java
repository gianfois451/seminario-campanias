package ar.edu.unrn.seminario.backend.dao.implementacionJDBC;

import ar.edu.unrn.seminario.backend.dao.conexion.ConexionDB;
import ar.edu.unrn.seminario.backend.dao.intefaces.CiudadanoDAO;
import ar.edu.unrn.seminario.backend.dao.intefaces.PersonaDAO;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.exception.ErrorDeBaseDeDatosException;
import ar.edu.unrn.seminario.backend.modelo.Ciudad;
import ar.edu.unrn.seminario.backend.modelo.Ciudadano;
import ar.edu.unrn.seminario.backend.modelo.Direccion;
import ar.edu.unrn.seminario.backend.modelo.Persona;

import java.sql.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class CiudadanoDAOJdbc implements CiudadanoDAO {

    private static final String TABLA = "ciudadanos";

    private static final String INSERT = "INSERT INTO " + TABLA + " (persona_id) VALUES (?)";

    private static final String SELECT = "SELECT c.id, p.nombre, p.apellido, p.dni, c.persona_id, d.calle, d.numero, " +
            "d.latitud, d.longitud, d.ciudad_id, ci.nombre as nombre_ciudad FROM " + TABLA + " c JOIN personas p ON c.persona_id = p.id JOIN direcciones d " +
            "ON d.id = p.direccion_id JOIN ciudades ci ON ci.id = d.ciudad_id";

    private static final String SELECT_FIND_BY_DNI = SELECT + " WHERE p.dni = ?";

    private static final String SELECT_PERSONA_ID_BY_DNI = "select p.id from personas p WHERE p.dni = ?";

    private static final String INSERT_PERSONA = "INSERT INTO personas (nombre, apellido, dni, direccion_id) VALUES (?,?,?,?)";

    private static final String INSERT_DIRECCION = "INSERT INTO direcciones (calle, numero, latitud, longitud, ciudad_id) " +
            "VALUES (?,?,?,?,?)";

    private static final String UPDATE_PERSONA = "UPDATE personas SET nombre= ?, apellido =?, dni = ? WHERE dni = ?";

    private static final String UPDATE_DIRECCION = "UPDATE direcciones SET calle = ?, numero = ?, latitud = ?, longitud = ?, " +
            "ciudad_id = ? WHERE id = (SELECT direccion_id FROM personas WHERE dni = ?)";

    private Ciudadano resultAEntidad(ResultSet resultSet) throws SQLException {
        return new Ciudadano(
                resultSet.getLong("c.id"),
                new Persona(
                        resultSet.getLong("persona_id"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellido"),
                        new Direccion(
                                resultSet.getString("calle"),
                                resultSet.getInt("numero"),
                                new Ciudad(
                                        resultSet.getLong("ciudad_id"),
                                        resultSet.getString("nombre_ciudad")
                                ),
                                resultSet.getDouble("latitud"),
                                resultSet.getDouble("longitud")

                        ),
                        resultSet.getString("dni")
                )
        );
    }

    @Override
    public void create(Ciudadano ciudadano) throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir(false);
                PreparedStatement statement = connection.prepareStatement(INSERT);
        ) {

            /* insert persona */
            Long personaId = this.findPersonaIdByDNI(ciudadano.getPersona().getDni());
            if (personaId == null) {
                Persona persona = ciudadano.getPersona();

                /* insert direccion */
                if (persona.getDireccion().getId() == null) {
                    Direccion direccion = persona.getDireccion();
                    try (PreparedStatement direccionStatement = connection.prepareStatement(INSERT_DIRECCION, Statement.RETURN_GENERATED_KEYS)) {
                        direccionStatement.setString(1, direccion.getCalle());
                        direccionStatement.setInt(2, direccion.getNumero());
                        direccionStatement.setDouble(3, direccion.getLatitud());
                        direccionStatement.setDouble(4, direccion.getLongitud());
                        direccionStatement.setLong(5, direccion.getCiudad().getId());
                        direccionStatement.execute();
                        try (ResultSet keys = direccionStatement.getGeneratedKeys()) {
                            if (keys.next()) {
                                direccion.setId(keys.getLong(1));
                            }
                        }
                    } catch (SQLException e) {
                        throw new ErrorDeBaseDeDatosException("direcciones", e.getMessage());
                    }
                }

                try (
                        PreparedStatement personaStatement = connection.prepareStatement(INSERT_PERSONA, Statement.RETURN_GENERATED_KEYS);
                ) {
                    personaStatement.setString(1, persona.getNombre());
                    personaStatement.setString(2, persona.getApellido());
                    personaStatement.setString(3, persona.getDni());
                    personaStatement.setLong(4, persona.getDireccion().getId());
                    personaStatement.execute();
                    try (ResultSet keys = personaStatement.getGeneratedKeys()) {
                        if (keys.next()) {
                            persona.setId(keys.getLong(1));
                        }
                    }
                } catch (SQLException e) {
                    throw new ErrorDeBaseDeDatosException("personas", e.getMessage());
                }

            } else {
                ciudadano.getPersona().setId(personaId);
            }

            statement.setLong(1, ciudadano.getPersona().getId());
            statement.execute();
            connection.commit();
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void update(String dni, Ciudadano ciudadano) throws ErrorDeAccesoADatosException {
        try (
            Connection connection = ConexionDB.abrir(false);
            PreparedStatement statement = connection.prepareStatement(UPDATE_PERSONA)
        ) {
            Persona persona = ciudadano.getPersona();

            /* update direccion */
            Direccion direccion = persona.getDireccion();
            try (PreparedStatement direccionStatement = connection.prepareStatement(UPDATE_DIRECCION)) {
                direccionStatement.setString(1, direccion.getCalle());
                direccionStatement.setInt(2, direccion.getNumero());
                direccionStatement.setDouble(3, direccion.getLatitud());
                direccionStatement.setDouble(4, direccion.getLongitud());
                direccionStatement.setLong(5, direccion.getCiudad().getId());
                direccionStatement.setString(6, dni);
                direccionStatement.executeUpdate();
            } catch (SQLException e) {
                throw new ErrorDeBaseDeDatosException("direcciones", e.getMessage());
            }


            statement.setString(1, persona.getNombre());
            statement.setString(2, persona.getApellido());
            statement.setString(3, persona.getDni());
            statement.setString(4, dni);
            statement.execute();
            connection.commit();
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public List<Ciudadano> listarCiudadanos() throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(SELECT);
        ) {
            //Se usa linked list por su eficiencia para insertar elementos
            List<Ciudadano> list = new LinkedList<>();
            while (resultSet.next()) {
                Ciudadano c = this.resultAEntidad(resultSet);
                list.add(c);
            }
            return list;
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public Ciudadano findByDNI(String dni) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(SELECT_FIND_BY_DNI);

        ) {
            Ciudadano ciudadano = null;
            statement.setString(1, dni);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.first()) {
                    ciudadano = this.resultAEntidad(resultSet);
                }
            }
            return ciudadano;
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    private Long findPersonaIdByDNI(String dni) throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(SELECT_PERSONA_ID_BY_DNI);

        ) {
            statement.setString(1, dni);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.first()) {
                    return resultSet.getLong("id");
                }
            }
            return null;
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }
}
