package ar.edu.unrn.seminario.backend.dao.implementacionJDBC;


import ar.edu.unrn.seminario.backend.dao.conexion.ConexionDB;
import ar.edu.unrn.seminario.backend.dao.intefaces.CampaniaDAO;
import ar.edu.unrn.seminario.backend.exception.DatoRepetidoException;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.exception.ErrorDeBaseDeDatosException;
import ar.edu.unrn.seminario.backend.modelo.Campania;
import ar.edu.unrn.seminario.backend.modelo.Institucion;
import ar.edu.unrn.seminario.backend.modelo.LugarDeDonacion;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class CampaniaDAOJdbc implements CampaniaDAO {

    private static final String TABLA = "campanias";
    private static final String SELECT = "Select c.*, i.nombre, i.cuil, i.contacto FROM " + TABLA + " c JOIN instituciones i ON c.institucion_id = i.id ";
    private static final String SELECT_FIND_BY_ID = SELECT + " WHERE c.id = ? ";
    private static final String INSERT = "INSERT INTO " + TABLA + " (id, fecha_inicio, fecha_fin, descripcion, motivo, institucion_id) VALUES (?,?,?,?,?,?)";
    private static final String UPDATE = "UPDATE " + TABLA + " SET fecha_inicio=?, fecha_fin=?, descripcion=?, motivo=?" + "WHERE id=?";
    private static final String INSERT_LUGARES_CAMPANIAS = "INSERT INTO lugares_campanias (lugar_id, campania_id) " +
            "VALUES ((SELECT id from lugares l where l.nombre = ?), ?)";
    private static final String CLEAN_LUGARES_CAMPANIAS = "DELETE FROM lugares_campanias where campania_id = ?";
    private Campania resultAEntidad(ResultSet resultSet) throws SQLException {
        return new Campania(
                resultSet.getLong("id"),
                resultSet.getDate("fecha_inicio"),
                resultSet.getDate("fecha_fin"),
                resultSet.getString("descripcion"),
                resultSet.getString("motivo"),
                new Institucion(
                        resultSet.getLong("institucion_id"),
                        resultSet.getString("nombre"),
                        resultSet.getString("cuil"),
                        resultSet.getString("contacto"),
                        null
                )
        );
    }


    @Override
    public void create(Campania campania) throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir(false);
                PreparedStatement statement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        ) {
            statement.setLong(1, campania.getId());
            statement.setDate(2, new Date(campania.getFechaInicio().getTime()));
            statement.setDate(3, new Date(campania.getFechaFin().getTime()));
            statement.setString(4, campania.getDescripcion());
            statement.setString(5, campania.getMotivo());
            statement.setLong(6, campania.getInstitucion().getId());
            statement.execute();

            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    campania.setId(keys.getLong(1));
                }
            }

            List<LugarDeDonacion> lugares = campania.getLugaresDeDonacion();
            for (LugarDeDonacion lugar: lugares) {
                try (
                        PreparedStatement lugarStatement = connection.prepareStatement(INSERT_LUGARES_CAMPANIAS);
                ) {
                    lugarStatement.setString(1, lugar.getNombre());
                    lugarStatement.setLong(2, campania.getId());
                    lugarStatement.execute();
                }
            }


            connection.commit();
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void update(Campania campania) throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir(false);
                PreparedStatement statement = connection.prepareStatement(UPDATE);
        ) {

            if(campania.getLugaresDeDonacion() != null){
                try(
                        PreparedStatement cleanLugaresStatement = connection.prepareStatement(CLEAN_LUGARES_CAMPANIAS);
                ){
                    cleanLugaresStatement.setLong(1, campania.getId());
                    cleanLugaresStatement.execute();
                    List<LugarDeDonacion> lugares = campania.getLugaresDeDonacion();
                    for (LugarDeDonacion lugar: lugares) {
                        try (
                                PreparedStatement lugarStatement = connection.prepareStatement(INSERT_LUGARES_CAMPANIAS);
                        ) {
                            lugarStatement.setString(1, lugar.getNombre());
                            lugarStatement.setLong(2, campania.getId());
                            lugarStatement.execute();
                        }
                    }
                }
            }

            statement.setDate(1, new Date(campania.getFechaInicio().getTime()));
            statement.setDate(2, new Date(campania.getFechaFin().getTime()));
            statement.setString(3, campania.getDescripcion());
            statement.setString(4, campania.getMotivo());
            statement.setLong(5, campania.getId());
            statement.execute();
            connection.commit();
        } catch (SQLIntegrityConstraintViolationException e) {
            String dato = e.getMessage().split("'")[1];
            throw new DatoRepetidoException(dato, e);
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public Campania findByCodigo(Long codigo) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(SELECT_FIND_BY_ID)
        ) {
            Campania campania = null;
            statement.setLong(1, codigo);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.first()) {
                    campania = this.resultAEntidad(resultSet);
                }
            }
            return campania;
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }

    }

    @Override
    public List<Campania> findAll() throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(SELECT);
        ) {
            List<Campania> list = new LinkedList<>();
            while (resultSet.next()) {
                list.add(this.resultAEntidad(resultSet));
            }
            return list;
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

}
