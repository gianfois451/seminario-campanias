package ar.edu.unrn.seminario.backend.dao.implementacionJDBC;

import ar.edu.unrn.seminario.backend.dao.conexion.ConexionDB;
import ar.edu.unrn.seminario.backend.dao.intefaces.InstitucionDAO;
import ar.edu.unrn.seminario.backend.exception.DatoRepetidoException;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.exception.ErrorDeBaseDeDatosException;
import ar.edu.unrn.seminario.backend.modelo.Ciudad;
import ar.edu.unrn.seminario.backend.modelo.Direccion;
import ar.edu.unrn.seminario.backend.modelo.Institucion;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class InstitucionDAOJdbc implements InstitucionDAO {

    private static final String TABLA = "instituciones";

    private static final String INSERT = "INSERT INTO " + TABLA + " (nombre, cuil, contacto, direccion_id) VALUES (?,?,?,?)";

    private static final String SELECT = "SELECT i.*, d.calle, d.numero, d.latitud, d.longitud, d.ciudad_id, c.nombre " +
            "as nombre_ciudad FROM " + TABLA + " i JOIN direcciones d ON d.id = i.direccion_id JOIN ciudades c " +
            "ON d.ciudad_id = c.id ";

    private static final String UPDATE = "UPDATE " + TABLA + " SET nombre = ?, cuil = ?, contacto = ? WHERE nombre = ?";

    private static final String FIND_BY_NOMBRE = SELECT + " WHERE i.nombre = ?";

    private static final String UPDATE_DIRECCION = "UPDATE direcciones SET calle = ?, numero = ?, latitud = ?, longitud = ?," +
            " ciudad_id = ? WHERE id = (SELECT direccion_id FROM instituciones WHERE nombre = ? )";

    private static final String INSERT_DIRECCION = "INSERT INTO direcciones (calle, numero, latitud, longitud, ciudad_id) " +
            "VALUES (?,?,?,?,?)";

    private Institucion resultAEntidad(ResultSet resultSet) throws SQLException {
        return new Institucion(resultSet.getLong("id"),
                resultSet.getString("nombre"),
                resultSet.getString("cuil"),
                resultSet.getString("contacto"),
                new Direccion(
                        resultSet.getLong("direccion_id"),
                        resultSet.getString("calle"),
                        resultSet.getInt("numero"),
                        new Ciudad(
                                resultSet.getLong("ciudad_id"),
                                resultSet.getString("nombre_ciudad")
                        ),
                        resultSet.getLong("latitud"),
                        resultSet.getLong("longitud")
                ));
    }


    @Override
    public void create(Institucion institucion) throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir(false);
                PreparedStatement statement = connection.prepareStatement(INSERT);
        ) {

            /* insert de direccion */
            if (institucion.getDireccion().getId() == null) {
                try (
                    PreparedStatement direccionStatement = connection.prepareStatement(INSERT_DIRECCION, Statement.RETURN_GENERATED_KEYS);
                ) {
                    direccionStatement.setString(1, institucion.getDireccion().getCalle());
                    direccionStatement.setInt(2, institucion.getDireccion().getNumero());
                    direccionStatement.setDouble(3, institucion.getDireccion().getLatitud());
                    direccionStatement.setDouble(4, institucion.getDireccion().getLongitud());
                    direccionStatement.setLong(5, institucion.getDireccion().getCiudad().getId());
                    direccionStatement.execute();
                    try (ResultSet keys = direccionStatement.getGeneratedKeys()) {
                        if (keys.next()) {
                            institucion.getDireccion().setId(keys.getLong(1));
                        }
                    }
                } catch (SQLException e) {
                    throw new ErrorDeBaseDeDatosException("direcciones", e.getMessage());
                }
            }

            statement.setString(1, institucion.getNombre());
            statement.setString(2, institucion.getCuil());
            statement.setString(3, institucion.getContacto());
            statement.setLong(4, institucion.getDireccion().getId());
            statement.execute();
            connection.commit();

        } catch (SQLIntegrityConstraintViolationException e) {
            String dato = e.getMessage().split("'")[1];
            throw new DatoRepetidoException(dato, e);
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public Institucion findByNombre(String nombre) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(FIND_BY_NOMBRE);
        ) {
            statement.setString(1, nombre);
            Institucion institucion = null;
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.first()) {
                    institucion = this.resultAEntidad(resultSet);
                }
            }
            return institucion;
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void update(String nombre, Institucion institucion) throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir(false);
                PreparedStatement statement = connection.prepareStatement(UPDATE);
        ) {

            Direccion direccion = institucion.getDireccion();

            /* update direccion: */
            try (
                    PreparedStatement direccionStatement = connection.prepareStatement(UPDATE_DIRECCION);
            ) {
                direccionStatement.setString(1, direccion.getCalle());
                direccionStatement.setInt(2, direccion.getNumero());
                direccionStatement.setDouble(3, direccion.getLatitud());
                direccionStatement.setDouble(4, direccion.getLongitud());
                direccionStatement.setLong(5, direccion.getCiudad().getId());
                direccionStatement.setString(6, nombre);
                direccionStatement.execute();
            } catch (SQLException e) {
                throw new ErrorDeBaseDeDatosException("direcciones", e.getMessage());
            }

            /* update institucion */
            statement.setString(1, institucion.getNombre());
            statement.setString(2, institucion.getCuil());
            statement.setString(3, institucion.getContacto());
            statement.setString(4, nombre);
            statement.executeUpdate();

            connection.commit();
        } catch (SQLIntegrityConstraintViolationException e) {
            String dato = e.getMessage().split("'")[1];
            throw new DatoRepetidoException(dato, e);
        } catch (SQLException | ErrorDeAccesoADatosException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public List<Institucion> findAll() throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(SELECT);
        ) {
            List<Institucion> list = new LinkedList<>();
            while (resultSet.next()) {
                list.add(this.resultAEntidad(resultSet));
            }
            return list;
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

}
