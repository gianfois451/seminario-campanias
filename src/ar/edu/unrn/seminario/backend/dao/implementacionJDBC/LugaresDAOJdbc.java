package ar.edu.unrn.seminario.backend.dao.implementacionJDBC;

import ar.edu.unrn.seminario.backend.dao.conexion.ConexionDB;
import ar.edu.unrn.seminario.backend.dao.intefaces.LugaresDAO;
import ar.edu.unrn.seminario.backend.exception.DatoRepetidoException;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.exception.ErrorDeBaseDeDatosException;
import ar.edu.unrn.seminario.backend.modelo.Ciudad;
import ar.edu.unrn.seminario.backend.modelo.Direccion;
import ar.edu.unrn.seminario.backend.modelo.LugarDeDonacion;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class LugaresDAOJdbc implements LugaresDAO {

    private static final String TABLA = "lugares";

    private static final String SELECT = "SELECT l.nombre, l.contacto, d.calle, d.numero, " +
            "d.latitud, d.longitud, d.ciudad_id, ci.nombre as nombre_ciudad FROM " + TABLA + " l  JOIN direcciones d " +
            "ON d.id = l.direccion_id JOIN ciudades ci ON ci.id = d.ciudad_id ";

    private static final String SELECT_BY_CAMPANIA = SELECT + " JOIN lugares_campanias lc ON lc.lugar_id = l.id " +
            "JOIN campanias c ON c.id = lc.campania_id where c.id = ?";

    private static final String SELECT_BY_NOMBRE = SELECT + " Where l.nombre = ?";

    private static final String INSERT = "INSERT INTO " + TABLA + " (nombre, contacto, direccion_id) VALUES (?,?,?)";

    private static final String UPDATE_DIRECCION = "UPDATE direcciones SET calle = ?, numero = ?, latitud = ?, longitud = ?," +
            " ciudad_id = ? WHERE id = (SELECT direccion_id FROM instituciones WHERE nombre = ? )";

    private static final String INSERT_DIRECCION = "INSERT INTO direcciones (calle, numero, latitud, longitud, ciudad_id) " +
            "VALUES (?,?,?,?,?)";

    private static final String UPDATE = "UPDATE " + TABLA + " SET nombre = ?, contacto = ? WHERE nombre = ?";

    private static final String INSERT_LUGARES_CAMPANIAS = "INSERT INTO lugares_campanias (lugar_id, campania_id) " +
            "VALUES ((SELECT id from lugares l where l.nombre = ?), ?)";

    private static final String DELETE_LUGARES_CAMPANIAS = "DELETE FROM lugares_campanias " +
            "WHERE lugar_id = (SELECT id from lugares l where l.nombre = ?) and campania_id = ?";


    private LugarDeDonacion resultAEntidad(ResultSet resultSet) throws SQLException {
        return new LugarDeDonacion(
                resultSet.getString("nombre"),
                resultSet.getString("contacto"),
                new Direccion(
                        resultSet.getString("calle"),
                        resultSet.getInt("numero"),
                        new Ciudad(
                                resultSet.getLong("ciudad_id"),
                                resultSet.getString("nombre_ciudad")
                        ),
                        resultSet.getDouble("latitud"),
                        resultSet.getDouble("longitud")

                )
        );
    }

    @Override
    public LugarDeDonacion findByNombre(String nombre) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(SELECT_BY_NOMBRE);
        ) {
            statement.setString(1, nombre);
            try (
                    ResultSet resultSet = statement.executeQuery();
            ) {
                if (resultSet.next()) {
                    return this.resultAEntidad(resultSet);
                }
            }
            return null;
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public List<LugarDeDonacion> findAllLugares() throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(SELECT);
        ) {
            List<LugarDeDonacion> list = new LinkedList<>();
            while (resultSet.next()) {
                list.add(this.resultAEntidad(resultSet));
            }
            return list;
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public List<LugarDeDonacion> findAllLugaresByCampania(Long codigoCampania) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(SELECT_BY_CAMPANIA);
        ) {
            statement.setLong(1, codigoCampania);
            try (
                    ResultSet resultSet = statement.executeQuery();
            ) {
                List<LugarDeDonacion> list = new LinkedList<>();
                while (resultSet.next()) {
                    list.add(this.resultAEntidad(resultSet));
                }
                return list;
            }
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void crearLugarDeDonacion(LugarDeDonacion lugarDeDonacion) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir(false);
                PreparedStatement statement = connection.prepareStatement(INSERT);
        ) {

            /* insert de direccion */
            if (lugarDeDonacion.getDireccion().getId() == null) {
                try (
                        PreparedStatement direccionStatement = connection.prepareStatement(INSERT_DIRECCION, Statement.RETURN_GENERATED_KEYS);
                ) {
                    direccionStatement.setString(1, lugarDeDonacion.getDireccion().getCalle());
                    direccionStatement.setInt(2, lugarDeDonacion.getDireccion().getNumero());
                    direccionStatement.setDouble(3, lugarDeDonacion.getDireccion().getLatitud());
                    direccionStatement.setDouble(4, lugarDeDonacion.getDireccion().getLongitud());
                    direccionStatement.setLong(5, lugarDeDonacion.getDireccion().getCiudad().getId());
                    direccionStatement.execute();
                    try (ResultSet keys = direccionStatement.getGeneratedKeys()) {
                        if (keys.next()) {
                            lugarDeDonacion.getDireccion().setId(keys.getLong(1));
                        }
                    }
                } catch (SQLException e) {
                    throw new ErrorDeBaseDeDatosException("direcciones", e.getMessage());
                }
            }

            statement.setString(1, lugarDeDonacion.getNombre());
            statement.setString(2, lugarDeDonacion.getContacto());
            statement.setLong(3, lugarDeDonacion.getDireccion().getId());
            statement.execute();
            connection.commit();
        } catch (SQLIntegrityConstraintViolationException e) {
            String dato = e.getMessage().split("'")[1];
            throw new DatoRepetidoException(dato, e);
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void updateLugarDeDonacion(String nombreOriginal, LugarDeDonacion lugarDeDonacion) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir(false);
                PreparedStatement statement = connection.prepareStatement(UPDATE);
        ) {

            Direccion direccion = lugarDeDonacion.getDireccion();

            /* update direccion: */
            try (
                    PreparedStatement direccionStatement = connection.prepareStatement(UPDATE_DIRECCION);
            ) {
                direccionStatement.setString(1, direccion.getCalle());
                direccionStatement.setInt(2, direccion.getNumero());
                direccionStatement.setDouble(3, direccion.getLatitud());
                direccionStatement.setDouble(4, direccion.getLongitud());
                direccionStatement.setLong(5, direccion.getCiudad().getId());
                direccionStatement.setString(6, nombreOriginal);
                direccionStatement.execute();
            } catch (SQLException e) {
                throw new ErrorDeBaseDeDatosException("direcciones", e.getMessage());
            }

            /* update institucion */
            statement.setString(1, lugarDeDonacion.getNombre());
            statement.setString(2, lugarDeDonacion.getContacto());
            statement.setString(3, nombreOriginal);
            statement.executeUpdate();

            connection.commit();
        } catch (SQLIntegrityConstraintViolationException e) {
            String dato = e.getMessage().split("'")[1];
            throw new DatoRepetidoException(dato, e);
        } catch (SQLException | ErrorDeAccesoADatosException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void asignarLugarACampania(String nombreLugar, Long codigoCampania) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(INSERT_LUGARES_CAMPANIAS);
        ) {
            statement.setString(1, nombreLugar);
            statement.setLong(2, codigoCampania);
            statement.execute();
        } catch (SQLIntegrityConstraintViolationException e) {
            String dato = e.getMessage().split("'")[1];
            throw new DatoRepetidoException(dato, e);
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void eliminarLugarDeCampania(String nombreLugar, Long codigoCampania) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(DELETE_LUGARES_CAMPANIAS);
        ) {
            statement.setString(1, nombreLugar);
            statement.setLong(2, codigoCampania);
            statement.execute();
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

}
