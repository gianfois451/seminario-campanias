package ar.edu.unrn.seminario.backend.dao.implementacionJDBC;

import ar.edu.unrn.seminario.backend.dao.conexion.ConexionDB;
import ar.edu.unrn.seminario.backend.dao.intefaces.VisitasDAO;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.exception.ErrorDeBaseDeDatosException;
import ar.edu.unrn.seminario.backend.modelo.Visita;

import java.sql.*;

public class VisitasDAOJdbc implements VisitasDAO {

    private static final String TABLA = "visitas";

    private static final String INSERT = "insert into " + TABLA + " (fecha, observacion, orden_id) values (?,?,?)";

    private static final String UPDATE = "update " + TABLA + " set fecha = ?, observacion = ? where id = ?";

    private static final String DELETE = "delete from " + TABLA + " where id = ?";

    @Override
    public Long guardarVisita(Visita visita, Long numeroDePedido) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        ) {
            statement.setDate(1, new Date(visita.getFecha().getTime()));
            statement.setString(2, visita.getObservacion());
            statement.setLong(3, numeroDePedido);
            statement.execute();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    visita.setId(keys.getLong(1));
                    return visita.getId();
                }
            }
            return null;
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void modificarVisita(Visita visita) throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(UPDATE);
        ) {
            statement.setDate(1, new Date(visita.getFecha().getTime()));
            statement.setString(2, visita.getObservacion());
            statement.setLong(3, visita.getId());
            statement.execute();
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void eliminarVisita(Long numeroDeVisita) throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(DELETE);
        ) {
            statement.setLong(1, numeroDeVisita);
            statement.execute();
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }
}
