package ar.edu.unrn.seminario.backend.dao.implementacionJDBC;

import ar.edu.unrn.seminario.backend.dao.conexion.ConexionDB;
import ar.edu.unrn.seminario.backend.dao.intefaces.CiudadDAO;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.exception.ErrorDeBaseDeDatosException;
import ar.edu.unrn.seminario.backend.modelo.Ciudad;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class CiudadDAOJdbc implements CiudadDAO {

    private static final String TABLA = "ciudades";
    private static final String FIND_BY_NAME = "SELECT * FROM ciudades WHERE nombre = ?";
    private static final String SELECT_ALL = "SELECT * FROM ciudades";
    private static final String INSERT = "INSERT INTO " + TABLA + " (nombre) values (?)";
    private static final String UPDATE = "UPDATE " + TABLA + " SET nombre=? WHERE nombre=?";

    @Override
    public Ciudad findByNombre(String nombre) throws ErrorDeAccesoADatosException {
        try (
            Connection connection = ConexionDB.abrir();
            PreparedStatement statement = connection.prepareStatement(FIND_BY_NAME);
        ) {
            Ciudad ciudad = null;
            statement.setString(1, nombre);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    ciudad = new Ciudad(resultSet.getLong("id"), resultSet.getString("nombre"));
                }
            }
            return ciudad;
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public List<Ciudad> findAll() throws ErrorDeAccesoADatosException {
        try (
            Connection connection = ConexionDB.abrir();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SELECT_ALL);
        ) {
            List<Ciudad> list = new LinkedList<>();
            while (resultSet.next()) {
                list.add(new Ciudad(resultSet.getLong("id"), resultSet.getString("nombre")));
            }
            return list;
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void create(String nombre) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(INSERT);
        ) {
            statement.setString(1, nombre);
            statement.execute();
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void update(String nombreAntiguo, String nuevoNombre) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(UPDATE);
        ) {
            statement.setString(1, nuevoNombre);
            statement.setString(2, nombreAntiguo);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }
}
