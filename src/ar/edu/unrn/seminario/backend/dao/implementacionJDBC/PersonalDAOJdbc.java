package ar.edu.unrn.seminario.backend.dao.implementacionJDBC;

import ar.edu.unrn.seminario.backend.dao.conexion.ConexionDB;
import ar.edu.unrn.seminario.backend.dao.intefaces.PersonalDAO;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.exception.ErrorDeBaseDeDatosException;
import ar.edu.unrn.seminario.backend.modelo.*;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class PersonalDAOJdbc implements PersonalDAO {

    private static final String TABLA = "personal";
    private static final String SELECT = "SELECT personal.id, personal.email, personal.foto, p.nombre, p.apellido, p.dni, personal.persona_id, d.calle, d.numero, " +
            "d.latitud, d.longitud, p.direccion_id, d.ciudad_id, ci.nombre as nombre_ciudad FROM " + TABLA + " personal JOIN personas p ON personal.persona_id = p.id JOIN direcciones d " +
            "ON d.id = p.direccion_id JOIN ciudades ci ON ci.id = d.ciudad_id";
    private static final String SELECT_BY_INSTITUCION = SELECT + " WHERE personal.institucion_id = (SELECT i.id FROM instituciones i WHERE i.nombre = ?)";

    private static final String FIND_POR_NUMERO_DE_PEDIDO = SELECT + " where personal.institucion_id in " +
            "(select i.id from pedidos ped JOIN campanias c on ped.campania_id = c.id JOIN instituciones i on c.institucion_id = i.id where ped.id = ?)";

    private static final String FIND_BY_DNI = SELECT + " WHERE p.dni = ?";

    private static final String INSERT = "INSERT INTO " + TABLA + " (persona_id, email, foto, institucion_id) VALUES (?,?,?, (SELECT id from instituciones where nombre = ?))";

    private static final String INSERT_PERSONA = "INSERT INTO personas (nombre, apellido, dni, direccion_id) VALUES (?,?,?,?)";

    private static final String INSERT_DIRECCION = "INSERT INTO direcciones (calle, numero, latitud, longitud, ciudad_id) " +
            "VALUES (?,?,?,?,?)";

    private static final String SELECT_PERSONA_ID_BY_DNI = "select p.id from personas p WHERE p.dni = ?";


    private static final String UPDATE_PERSONAL = "UPDATE personal SET email= ?, foto=? WHERE persona_id = (SELECT id from personas where dni = ?)";

    private static final String UPDATE_PERSONA = "UPDATE personas SET nombre= ?, apellido =?, dni = ? WHERE dni = ?";

    private static final String DELETE = "DELETE FROM personal WHERE persona_id = (SELECT id from personas where dni = ?)";

    private static final String UPDATE_DIRECCION = "UPDATE direcciones SET calle = ?, numero = ?, latitud = ?, longitud = ?, " +
            "ciudad_id = ? WHERE id = (SELECT direccion_id FROM personas WHERE dni = ?)";

    private Personal resultAEntidad(ResultSet rs) throws SQLException {
        Personal personal = new Personal(
                rs.getString("nombre"),
                rs.getString("apellido"),
                new Direccion(
                        rs.getLong("direccion_id"),
                        rs.getString("calle"),
                        rs.getInt("numero"),
                        new Ciudad(
                                rs.getLong("ciudad_id"),
                                rs.getString("nombre_ciudad")
                        ),
                        rs.getDouble("latitud"),
                        rs.getDouble("longitud")

                ),
                rs.getString("dni"),
                rs.getString("email"),
                rs.getString("foto")
        );
        personal.setId(rs.getLong("id"));
        return personal;
    }

    @Override
    public Personal findByDNI(String dni) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(FIND_BY_DNI);
        ) {
            statement.setString(1, dni);
            try(
                    ResultSet resultSet = statement.executeQuery();
            ) {
                if (resultSet.next()) {
                    return this.resultAEntidad(resultSet);
                }
            }
            return null;
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public List<Personal> findByInstitucion(String institucion) throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(SELECT_BY_INSTITUCION);
        ) {
            statement.setString(1, institucion);
            try(
                    ResultSet resultSet = statement.executeQuery();
            ) {
                List<Personal> list = new LinkedList<>();
                while (resultSet.next()) {
                    Personal p = this.resultAEntidad(resultSet);
                    list.add(p);
                }
                return list;
            }
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public List<Personal> listarPersonalDisponiblePorNumeroDePedido(Long numeroPedido) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(FIND_POR_NUMERO_DE_PEDIDO);
        ) {
            statement.setLong(1, numeroPedido);
            try(
                    ResultSet resultSet = statement.executeQuery();
            ) {
                List<Personal> list = new LinkedList<>();
                while (resultSet.next()) {
                    Personal p = this.resultAEntidad(resultSet);
                    list.add(p);
                }
                return list;
            }
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void agregarAInstitucion(Personal personal, String institucion) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir(false);
                PreparedStatement statement = connection.prepareStatement(INSERT);
        ) {

            Long personaId = this.findPersonaIdByDNI(personal.getPersona().getDni());
            /* insert persona */
            if (personaId == null) {
                Persona persona = personal.getPersona();

                /* insert direccion */
                if (persona.getDireccion().getId() == null) {
                    Direccion direccion = persona.getDireccion();
                    try (PreparedStatement direccionStatement = connection.prepareStatement(INSERT_DIRECCION, Statement.RETURN_GENERATED_KEYS)) {
                        direccionStatement.setString(1, direccion.getCalle());
                        direccionStatement.setInt(2, direccion.getNumero());
                        direccionStatement.setDouble(3, direccion.getLatitud());
                        direccionStatement.setDouble(4, direccion.getLongitud());
                        direccionStatement.setLong(5, direccion.getCiudad().getId());
                        direccionStatement.execute();
                        try (ResultSet keys = direccionStatement.getGeneratedKeys()) {
                            if (keys.next()) {
                                direccion.setId(keys.getLong(1));
                            }
                        }
                    } catch (SQLException e) {
                        throw new ErrorDeBaseDeDatosException("direcciones", e.getMessage());
                    }
                }

                try (
                        PreparedStatement personaStatement = connection.prepareStatement(INSERT_PERSONA, Statement.RETURN_GENERATED_KEYS);
                ) {
                    personaStatement.setString(1, persona.getNombre());
                    personaStatement.setString(2, persona.getApellido());
                    personaStatement.setString(3, persona.getDni());
                    personaStatement.setLong(4, persona.getDireccion().getId());
                    personaStatement.execute();
                    try (ResultSet keys = personaStatement.getGeneratedKeys()) {
                        if (keys.next()) {
                            persona.setId(keys.getLong(1));
                        }
                    }
                } catch (SQLException e) {
                    throw new ErrorDeBaseDeDatosException("personas", e.getMessage());
                }

            } else {
                personal.getPersona().setId(personaId);
            }

            statement.setLong(1, personal.getPersona().getId());
            statement.setString(2, personal.getEmail());
            statement.setString(3, personal.getFoto());
            statement.setString(4, institucion);
            statement.execute();
            connection.commit();
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void eliminarDeInstitucion(String dni) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir(false);
                PreparedStatement statement = connection.prepareStatement(DELETE);
        ) {


            statement.setString(1, dni);
            statement.execute();
            connection.commit();
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }

    @Override
    public void update(String dniOriginal, Personal personal) throws ErrorDeAccesoADatosException {
        try (
                Connection connection = ConexionDB.abrir(false);
                PreparedStatement statement = connection.prepareStatement(UPDATE_PERSONAL);
        ) {

            statement.setString(1, personal.getEmail());
            statement.setString(2, personal.getFoto());
            statement.setString(3, dniOriginal);
            statement.executeUpdate();

            try(PreparedStatement personaStatement = connection.prepareStatement(UPDATE_PERSONA)){
                Persona persona = personal.getPersona();

                /* update direccion */
                Direccion direccion = persona.getDireccion();
                try (PreparedStatement direccionStatement = connection.prepareStatement(UPDATE_DIRECCION)) {
                    direccionStatement.setString(1, direccion.getCalle());
                    direccionStatement.setInt(2, direccion.getNumero());
                    direccionStatement.setDouble(3, direccion.getLatitud());
                    direccionStatement.setDouble(4, direccion.getLongitud());
                    direccionStatement.setLong(5, direccion.getCiudad().getId());
                    direccionStatement.setString(6, dniOriginal);
                    direccionStatement.executeUpdate();
                } catch (SQLException e) {
                    throw new ErrorDeBaseDeDatosException("direcciones", e.getMessage());
                }


                personaStatement.setString(1, persona.getNombre());
                personaStatement.setString(2, persona.getApellido());
                personaStatement.setString(3, persona.getDni());
                personaStatement.setString(4, dniOriginal);
                personaStatement.executeUpdate();
            }
            connection.commit();
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }


    private Long findPersonaIdByDNI(String dni) throws ErrorDeBaseDeDatosException {
        try (
                Connection connection = ConexionDB.abrir();
                PreparedStatement statement = connection.prepareStatement(SELECT_PERSONA_ID_BY_DNI);

        ) {
            statement.setString(1, dni);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.first()) {
                    return resultSet.getLong("id");
                }
            }
            return null;
        } catch (SQLException e) {
            throw new ErrorDeBaseDeDatosException(TABLA, e.getMessage());
        }
    }
}
