package ar.edu.unrn.seminario.backend.test;

import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.CambioDeEstadoException;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.exception.OrdenFinalizadaException;
import ar.edu.unrn.seminario.backend.exception.OrdenNoFinalizadaException;
import ar.edu.unrn.seminario.backend.modelo.Donacion;
import ar.edu.unrn.seminario.backend.modelo.OrdenDeRetiro;

import java.util.HashSet;
import java.util.List;

public class Test {

    private static IFacade facade = new ApiFacade();


    //Correr el clean.sql en la base antes de ejecutar el test
    public static void main(String[] args) throws ErrorDeAccesoADatosException, CambioDeEstadoException, OrdenFinalizadaException, OrdenNoFinalizadaException {
        facade.altaDeInstitucion("Institucion 1", "123", "123",
                "mejico", "Patagones", 123, -40, -60);
        facade.altaDeInstitucion("Institucion 2", "124", "124",
                "rivadavia", "Viedma", 123, -40, -60);
        facade.altaDeInstitucion("Institucion 3", "125", "125",
                "colon", "Patagones", 123, -40, -60);

        assert (facade.listarInstituciones().size() == 3);

        facade.modificarInstitucion("Institucion 1", "Institucion Uno",
                "123", "123",
                "mejico", "patagones", 123, -40, -60);

        assert (facade.listarInstituciones().stream().filter(s -> s.getCuil().equals("123"))
                .findFirst().get().getNombre().equals("Institucion Uno"));


        facade.altaDeCampania(100L, "lalala", "lalalala", "12/11/2019",
                "14/11/2019", "Institucion Uno", new HashSet<>());

        facade.actualizarCampania(100L, "lelele", "lelelele", "12/11/2019",
                "14/11/2019", null);

        assert (facade.listarCampanias().size() == 1);

        facade.registrarCiudadano("1234", "Juan", "Perez",
                "colon", "patagones", 123, -40, -60);

        facade.registrarCiudadano("1235", "Pedro", "Perez",
                "saavedra", "patagones", 123, -40, -60);

        facade.registrarCiudadano("1236", "Maria", "Perez",
                "25 de mayo", "patagones", 123, -40, -60);

        assert (facade.listarCiudadanos().size() == 3);

        facade.modificarCiudadano("1236", "1255", "Maria", "Perez",
                "25 de mayo", "viedma", 123, -40, -60);

        Long numeroDePedido1 = facade.generarPedidoDeRetiro(100L, "1255", "Pedido",
                true);

        Long numeroDePedido2 = facade.generarPedidoDeRetiro(100L, "1255", "Pedido 2",
                true);

        facade.eliminarPedido(numeroDePedido2);

        assert (facade.listarPedidosDeRetiroPorCampania(100L).size() == 1);

        assert (facade.listarPersonalPorInstitucion("Institucion Uno").size() == 0);

        //String institucion, String dni, String nombre, String apellido, String calle, String ciudad,
        // int numeroDireccion, double latitud, double longitud, String email
        facade.agregarPersonalAInstitucion(
                "Institucion Uno",
                "4512345",
                "Pedro",
                "Sanchez",
                "Rivadavia",
                "viedma",
                451,
                -40,
                -60,
                "psanchez@gmail.com"
        );

        facade.agregarPersonalAInstitucion(
                "Institucion Uno",
                "124501",
                "Juan",
                "Rodriguez",
                "Rivadavia",
                "viedma",
                452,
                -40,
                -60,
                "juan@gmail.com"
        );

        facade.agregarPersonalAInstitucion(
                "Institucion Uno",
                "1234",
                "juanperez@gmail.com"
        );

        facade.modificarPersonal(
                "4512345",
                "4512345",
                "Pedro",
                "Sanchez",
                "Rivadavia",
                "viedma",
                451,
                -40,
                -60,
                "psanchez2@gmail.com"
        );

        assert (facade.listarPersonalPorInstitucion("Institucion Uno").size() == 3);

        facade.eliminarPersonalDeInstitucion("4512345");

        assert (facade.listarPersonalPorInstitucion("Institucion Uno").size() == 2);

        assert (facade.findAllLugares().size() == 0);

        assert (facade.findAllLugaresByCampania(100L).size() == 0);

        facade.crearLugarDeDonacion(
                "Lugar 1",
                "12345678",
                "Rivadavia",
                "viedma",
                451,
                -40,
                -60
        );

        facade.crearLugarDeDonacion(
                "Lugar 2",
                "12345678",
                "Colon",
                "viedma",
                127,
                -40,
                -60
        );

        facade.updateLugarDeDonacion(
                "Lugar 2",
                "Lugar 3",
                "12345678",
                "Colon",
                "viedma",
                128,
                -40,
                -60
        );

        assert (facade.findAllLugares().size() == 2);

        facade.asignarLugarACampania("Lugar 3", 100L);

        assert (facade.findAllLugaresByCampania(100L).size() == 1);

        facade.eliminarLugarDeCampania("Lugar 3", 100L);

        assert (facade.findAllLugaresByCampania(100L).size() == 0);

        facade.generarOrdenDeRetiro(numeroDePedido1, "1234");

        facade.reasignarOrdenDeRetiro(numeroDePedido1, "124501");

        List<OrdenDeRetiro> list = facade.listarOrdenesPorCampania(100L);

        assert(list.size() == 1);

        assert(list.get(0).getPersonalAsignado().getPersona().getDni().equals("124501"));

        try{
            facade.finalizarOrden(list.get(0).getId());
            assert(false);
        } catch (CambioDeEstadoException e) {
            assert(true);
        }

        Long visita1 = facade.agregarVisita(numeroDePedido1, "20/02/2020", "No estaba en el domicilio");
        Long visita2 = facade.agregarVisita(numeroDePedido1, "21/02/2020", "Se retiro la donacion");

        facade.modificarVisita(visita1, "22/02/2020", "No estaba en su casa");

        facade.eliminarVisita(visita2);

        list = facade.listarOrdenesPorCampania(100L);

        assert(list.get(0).getVisitas().size() == 1);
        assert(list.get(0).getVisitas().get(0).getObservacion().equals("No estaba en su casa"));

        try{
            facade.finalizarOrden(list.get(0).getId());
            assert(true);
        } catch (CambioDeEstadoException e) {
            assert(false);
        }

        list = facade.listarOrdenesPorCampania(100L);

        assert(list.get(0).getEstadoString().equals(OrdenDeRetiro.Estado.FINALIZADO.toString()));

        Donacion donacion = facade.generarDonacion(numeroDePedido1);

        assert(facade.listarDonacionesPorCampania(100L).size() == 1);

        facade.eliminarDonacion(donacion.getId());


    }
}
