package ar.edu.unrn.seminario.backend.helpers;

import java.util.Date;

public class ValidadorDeDatos {


	public static boolean esNull(Object o) {
		return o == null;
	}

	public static boolean esStringVacioONull(String s) {
		return esNull(s) || s.isEmpty();
	}

	public static boolean esRangoDeFechasInvalido(Date inicio, Date fin) {
		return inicio.after(fin);
	}

	public static boolean esDniInvalido(String dni) {
		return esStringVacioONull(dni) || !dni.matches("[\\d.]*");
	}

	public static boolean esCoordenadaInvalida(double latitud, double longitud) {
		return (latitud < -90 || latitud > 90 || longitud < -180 || latitud > 180);
	}



}
