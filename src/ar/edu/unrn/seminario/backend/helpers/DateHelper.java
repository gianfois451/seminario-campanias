package ar.edu.unrn.seminario.backend.helpers;

import ar.edu.unrn.seminario.backend.exception.FormatoDeFechaIncorrectoException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateHelper {

    public static final String FORMATO = "d/M/yyyy";
    private static DateFormat dateFormat;


    static {
        dateFormat = new SimpleDateFormat(FORMATO);
    }

    public static String fechaAString(Date fecha) {
        dateFormat.setCalendar(Calendar.getInstance());
        if(fecha == null){
            throw new FormatoDeFechaIncorrectoException();
        }
        return dateFormat.format(fecha);
    }

    public static Date stringAFecha(String string) throws ParseException {
        return dateFormat.parse(string);
    }



}
