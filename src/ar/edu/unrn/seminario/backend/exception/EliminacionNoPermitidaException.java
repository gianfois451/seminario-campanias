package ar.edu.unrn.seminario.backend.exception;

public class EliminacionNoPermitidaException extends RuntimeException {

    public EliminacionNoPermitidaException(String mensaje){
        super(mensaje);
    }

}
