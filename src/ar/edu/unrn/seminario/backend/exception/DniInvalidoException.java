package ar.edu.unrn.seminario.backend.exception;

public class DniInvalidoException extends RuntimeException {

	public DniInvalidoException(String dni) {
	    super("El dni " + dni + " no es valido.");
	}

}
