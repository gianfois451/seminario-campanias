package ar.edu.unrn.seminario.backend.exception;

public class OrdenFinalizadaException extends Exception {

    public OrdenFinalizadaException(){
        super("La orden se encuentra finalizada");
    }

}
