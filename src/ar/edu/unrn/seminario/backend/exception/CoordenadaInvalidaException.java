package ar.edu.unrn.seminario.backend.exception;

public class CoordenadaInvalidaException extends RuntimeException {

	public CoordenadaInvalidaException(double latitud, double longitud) {
		super("["+ latitud + "," + longitud +"] no es una coordenada valida");
	}
}
