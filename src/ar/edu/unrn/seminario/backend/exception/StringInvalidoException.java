package ar.edu.unrn.seminario.backend.exception;

public class StringInvalidoException extends RuntimeException {

    private String nombreDelDato;

	public StringInvalidoException(String nombreDelDato) {
	    super("El dato '" + nombreDelDato + "' es invalido");
	    this.nombreDelDato = nombreDelDato;
	}

    public String getNombreDelDato() {
        return nombreDelDato;
    }
}
