package ar.edu.unrn.seminario.backend.exception;

public class OrdenNoFinalizadaException extends Exception {

    public OrdenNoFinalizadaException(){
        super("La orden no se encuentra finalizada");
    }

}
