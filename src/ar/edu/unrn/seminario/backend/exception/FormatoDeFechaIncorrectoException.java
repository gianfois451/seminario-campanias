package ar.edu.unrn.seminario.backend.exception;

public class FormatoDeFechaIncorrectoException extends RuntimeException {
    public FormatoDeFechaIncorrectoException(){
        super("Formato de fecha incorrecto");
    }
}
