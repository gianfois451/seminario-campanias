package ar.edu.unrn.seminario.backend.exception;

public class DatoNullException extends RuntimeException {

    private String nombreDelDato;

	public DatoNullException(String nombreDelDato) {
		super(nombreDelDato + " es null.");
		this.nombreDelDato = nombreDelDato;
	}

    public String getNombreDelDato() {
        return nombreDelDato;
    }
}
