package ar.edu.unrn.seminario.backend.exception;

public class DatoRepetidoException extends RuntimeException {

    public DatoRepetidoException(String dato, Exception e){
        super("El dato " + dato + " no puede estar duplicado. \nMensaje original: " + e.getMessage() );
    }

}
