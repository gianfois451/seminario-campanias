package ar.edu.unrn.seminario.backend.exception;

public class ErrorDeBaseDeDatosException extends ErrorDeAccesoADatosException {

    public ErrorDeBaseDeDatosException(String tabla, String message){
        super("Error accediendo a la base de datos, accediendo a la tabla " + tabla
                + ".\nMensaje original: " + message);
    }
}
