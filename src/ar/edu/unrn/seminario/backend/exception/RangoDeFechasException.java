package ar.edu.unrn.seminario.backend.exception;

import ar.edu.unrn.seminario.backend.helpers.DateHelper;

import java.util.Date;

public class RangoDeFechasException extends RuntimeException {

    public RangoDeFechasException(Date inicio, Date fin) {
        super("El rango de fechas " + DateHelper.fechaAString(inicio) + " - " +
                DateHelper.fechaAString(fin) + " no es correcto.");
    }

}
