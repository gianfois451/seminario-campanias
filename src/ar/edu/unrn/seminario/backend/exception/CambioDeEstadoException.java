package ar.edu.unrn.seminario.backend.exception;

public class CambioDeEstadoException extends Exception {

	private String estadoActual, estadoDeseado;

	public CambioDeEstadoException(String estadoActual, String estadoDeseado) {
		super("No se puede pasar de '" + estadoActual + "' a '" + estadoDeseado + "'" );
		this.estadoActual = estadoActual;
		this.estadoDeseado = estadoDeseado;
	}

    public String getEstadoActual() {
        return estadoActual;
    }

    public String getEstadoDeseado() {
        return estadoDeseado;
    }
}
