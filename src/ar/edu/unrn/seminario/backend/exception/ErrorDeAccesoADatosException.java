package ar.edu.unrn.seminario.backend.exception;

public class ErrorDeAccesoADatosException extends Exception {

    public ErrorDeAccesoADatosException() {
    }

    public ErrorDeAccesoADatosException(String s) {
        super(s);
    }
}
