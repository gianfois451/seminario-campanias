package ar.edu.unrn.seminario.backend.api;

import ar.edu.unrn.seminario.backend.exception.CambioDeEstadoException;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.exception.OrdenFinalizadaException;
import ar.edu.unrn.seminario.backend.exception.OrdenNoFinalizadaException;
import ar.edu.unrn.seminario.backend.modelo.*;

import java.text.ParseException;
import java.util.List;
import java.util.Set;

public interface IFacade {

    /* institucion */
    void altaDeInstitucion(String nombre, String cuil, String contacto, String calle, String ciudad,
                           int numeroDireccion, double latitud, double longitud) throws ErrorDeAccesoADatosException;

    void modificarInstitucion(String nombreAnterior, String nuevoNombre, String cuil, String contacto, String calle, String ciudad,
                              int numeroDireccion, double latitud, double longitud) throws ErrorDeAccesoADatosException;

    List<Institucion> listarInstituciones() throws ErrorDeAccesoADatosException;


    /* campania */

    void altaDeCampania(Long codigo, String descripcion, String motivo, String fechaInicio,
                        String fechaFin, String nombreInstitucion, Set<String> nombresLugares) throws ErrorDeAccesoADatosException;

    void actualizarCampania(Long codigo, String descripcion, String motivo, String fechaInicio, String fechaFin, Set<String> lugares)
            throws ErrorDeAccesoADatosException;

    List<Campania> listarCampanias() throws ErrorDeAccesoADatosException;


    /* ciudadanos */

    //TODO registrar ciudadano a partir de persona ya creada

    void registrarCiudadano(String dni, String nombre, String apellido, String calle, String ciudad,
                            int numeroDireccion, double latitud, double longitud) throws ErrorDeAccesoADatosException;

    /* el dni no es modificable, se utiliza para identificar al ciudadano */
    void modificarCiudadano(String dniOriginal, String dni, String nombre, String apellido, String calle, String ciudad,
                            int numeroDireccion, double latitud, double longitud) throws ErrorDeAccesoADatosException;

    List<Ciudadano> listarCiudadanos() throws ErrorDeAccesoADatosException;

    List<Persona> listarPersonasNoCiudadanos() throws ErrorDeAccesoADatosException;

    List<Persona> listarPersonasNoPersonal() throws ErrorDeAccesoADatosException;



    /* personal */

    List<Personal> listarPersonalPorInstitucion(String institucion) throws ErrorDeAccesoADatosException;

    List<Personal> listarPersonalDisponiblePorNumeroDePedido(Long numeroDePedido) throws ErrorDeAccesoADatosException;

    void agregarPersonalAInstitucion(String institucion, String dni, String nombre, String apellido, String calle, String ciudad,
                                     int numeroDireccion, double latitud, double longitud, String email) throws ErrorDeAccesoADatosException;

    // a partir de persona ya existente
    void agregarPersonalAInstitucion(String institucion, String dni, String email) throws ErrorDeAccesoADatosException;

    void modificarPersonal(String dniOriginal, String dni, String nombre, String apellido, String calle, String ciudad,
                           int numeroDireccion, double latitud, double longitud, String email) throws ErrorDeAccesoADatosException;

    void eliminarPersonalDeInstitucion(String dni) throws ErrorDeAccesoADatosException;

    /* pedidos */

    /* retorna el codigo de retiro */
    Long generarPedidoDeRetiro(Long codigoCampania, String dniCiudadado, String descripcion, boolean requiereVehiculoDeCarga)
            throws ErrorDeAccesoADatosException;

    void modificarPedidoDeRetiro(Long numeroDeRetiro, String dniCiudadado, String descripcion, boolean requiereVehiculoDeCarga)
            throws ErrorDeAccesoADatosException;

    List<PedidoDeRetiro> listarPedidosDeRetiroPorCampania(Long codigoCampania) throws ErrorDeAccesoADatosException;

    List<PedidoDeRetiro> listarPedidosDeRetiroPorCiudadano(String dni) throws ErrorDeAccesoADatosException;

    void eliminarPedido(Long numeroDeRetiro) throws ErrorDeAccesoADatosException;

    /* ciudades */

    List<Ciudad> listarCiudades() throws ErrorDeAccesoADatosException;

    void crearCiudad(String nombre) throws ErrorDeAccesoADatosException;

    void updateCiudad(String nombreAntiguo, String nuevoNombre) throws ErrorDeAccesoADatosException;

    /* lugares */

    List<LugarDeDonacion> findAllLugares() throws ErrorDeAccesoADatosException;

    List<LugarDeDonacion> findAllLugaresByCampania(Long codigoCampania) throws ErrorDeAccesoADatosException;

    void crearLugarDeDonacion(String nombre, String contacto, String calle, String ciudad, int numeroDireccion,
                              double latitud, double longitud) throws ErrorDeAccesoADatosException;

    void updateLugarDeDonacion(String nombreOriginal, String nombre, String contacto, String calle, String ciudad,
                               int numeroDireccion, double latitud, double longitud) throws ErrorDeAccesoADatosException;

    void asignarLugarACampania(String nombreLugar, Long codigoCampania) throws ErrorDeAccesoADatosException;

    void eliminarLugarDeCampania(String nombreLugar, Long codigoCampania) throws ErrorDeAccesoADatosException;

    /* ordenes de retiro */

    void generarOrdenDeRetiro(Long numeroDePedido, String dniPersonal) throws ErrorDeAccesoADatosException;

    void reasignarOrdenDeRetiro(Long numeroDePedido, String dniPersonal) throws ErrorDeAccesoADatosException;

    List<OrdenDeRetiro> listarOrdenesPorCampania(Long codigoDeCampania) throws ErrorDeAccesoADatosException;

    OrdenDeRetiro buscarOrdenPorNumeroDePedido(Long numeroDePedido) throws ErrorDeAccesoADatosException;

    void finalizarOrden(Long numeroDePedido) throws ErrorDeAccesoADatosException, CambioDeEstadoException;

    Donacion generarDonacion(Long numeroDePedido) throws ErrorDeAccesoADatosException, OrdenNoFinalizadaException;

    /* visitas */

    /* retorna numero de visita */
    Long agregarVisita(Long numeroDePedido, String fecha, String observacion) throws ErrorDeAccesoADatosException, OrdenFinalizadaException;

    void modificarVisita(Long numeroDeVisita, String fecha, String observacion) throws ErrorDeAccesoADatosException;

    void eliminarVisita(Long numeroDeVisita) throws ErrorDeAccesoADatosException;

    /* donaciones */

    List<Donacion> listarDonacionesPorCampania(Long codigoDeCampania) throws ErrorDeAccesoADatosException;

    void eliminarDonacion(Long codigoDeDonacion) throws ErrorDeAccesoADatosException;

    void guardarDonacion(Long campania, String lugar, String descripcion, String fecha) throws ErrorDeAccesoADatosException, ParseException;

}







