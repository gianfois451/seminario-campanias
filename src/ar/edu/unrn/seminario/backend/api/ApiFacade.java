package ar.edu.unrn.seminario.backend.api;

import ar.edu.unrn.seminario.backend.dao.implementacionJDBC.DonacionesDAOJdbc;
import ar.edu.unrn.seminario.backend.dao.implementacionJDBC.OrdenesDAOJdbc;
import ar.edu.unrn.seminario.backend.dao.implementacionJDBC.VisitasDAOJdbc;
import ar.edu.unrn.seminario.backend.dao.implementacionJDBC.*;
import ar.edu.unrn.seminario.backend.dao.intefaces.*;
import ar.edu.unrn.seminario.backend.exception.*;
import ar.edu.unrn.seminario.backend.helpers.DateHelper;
import ar.edu.unrn.seminario.backend.modelo.*;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class ApiFacade implements IFacade {

    private InstitucionDAO institucionDAO = new InstitucionDAOJdbc();
    private CampaniaDAO campaniaDAO = new CampaniaDAOJdbc();
    private CiudadanoDAO ciudadanoDAO = new CiudadanoDAOJdbc();
    private PedidosDAO pedidosDAO = new PedidoDAOJdbc();
    private CiudadDAO ciudadDAO = new CiudadDAOJdbc();
    private PersonaDAO personaDAO = new PersonaDAOJdbc();
    private PersonalDAO personalDAO = new PersonalDAOJdbc();
    private LugaresDAO lugaresDAO = new LugaresDAOJdbc();
    private OrdenesDAO ordenesDAO = new OrdenesDAOJdbc();
    private VisitasDAO visitasDAO = new VisitasDAOJdbc();
    private DonacionesDAO donacionesDAO = new DonacionesDAOJdbc();

    @Override
    public void altaDeInstitucion(String nombre, String cuil, String contacto, String calle, String nombreCiudad, int numeroDireccion,
                                  double latitud, double longitud) throws ErrorDeAccesoADatosException {
        Ciudad ciudad = ciudadDAO.findByNombre(nombreCiudad);
        institucionDAO.create(new Institucion(nombre, cuil, contacto,
                new Direccion(calle, numeroDireccion, ciudad, latitud, longitud)));
    }

    @Override
    public void modificarInstitucion(String nombreAnterior, String nuevoNombre, String cuil, String contacto, String calle,
                                     String nombreCiudad, int numeroDireccion, double latitud, double longitud) throws ErrorDeAccesoADatosException {

        Ciudad ciudad = ciudadDAO.findByNombre(nombreCiudad);
        institucionDAO.update(nombreAnterior, new Institucion(nuevoNombre, cuil, contacto,
                new Direccion(calle, numeroDireccion, ciudad, latitud, longitud)));
    }

    @Override
    public List<Institucion> listarInstituciones() throws ErrorDeAccesoADatosException {
        return institucionDAO.findAll();
    }

    @Override
    public void altaDeCampania(Long codigo, String descripcion, String motivo, String fechaInicio, String fechaFin,
                               String nombreInstitucion, Set<String> nombresLugares)
            throws ErrorDeAccesoADatosException, FormatoDeFechaIncorrectoException {

        Institucion institucion = institucionDAO.findByNombre(nombreInstitucion);
        try {
            Campania campania = new Campania(codigo, DateHelper.stringAFecha(fechaInicio), DateHelper.stringAFecha(fechaFin),
                    descripcion, motivo, institucion);
            nombresLugares.forEach(lugar -> campania.agregarLugarDeDonacion(new LugarDeDonacion(lugar)));
            campaniaDAO.create(campania);

        } catch (ParseException e) {
            throw new FormatoDeFechaIncorrectoException();
        }
    }

    @Override
    public void actualizarCampania(Long codigo, String descripcion, String motivo, String fechaInicio, String fechaFin, Set<String> nombresLugares)
            throws ErrorDeAccesoADatosException {

        try {
            Campania campania = new Campania(codigo, DateHelper.stringAFecha(fechaInicio), DateHelper.stringAFecha(fechaFin),
                    descripcion, motivo, null);
            if(nombresLugares != null){
                nombresLugares.forEach(lugar -> campania.agregarLugarDeDonacion(new LugarDeDonacion(lugar)));
            }
            campaniaDAO.update(campania);
        } catch (ParseException e) {
            throw new FormatoDeFechaIncorrectoException();
        }
    }

    @Override
    public List<Campania> listarCampanias() throws ErrorDeAccesoADatosException {
        return campaniaDAO.findAll();
    }

    @Override
    public void registrarCiudadano(String dni, String nombre, String apellido, String calle, String nombreCiudad,
                                   int numeroDireccion, double latitud, double longitud) throws ErrorDeAccesoADatosException {

        Ciudad ciudad = ciudadDAO.findByNombre(nombreCiudad);
        ciudadanoDAO.create(new Ciudadano(new Persona(nombre, apellido, new Direccion(calle, numeroDireccion, ciudad, latitud, longitud), dni)));
    }

    @Override
    public void modificarCiudadano(String dniOriginal, String dni, String nombre, String apellido, String calle, String nombreCiudad,
                                   int numeroDireccion, double latitud, double longitud) throws ErrorDeAccesoADatosException {

        Ciudad ciudad = ciudadDAO.findByNombre(nombreCiudad);
        ciudadanoDAO.update(dniOriginal, new Ciudadano(new Persona(nombre, apellido, new Direccion(calle, numeroDireccion, ciudad, latitud, longitud), dni)));
    }


    @Override
    public List<Ciudadano> listarCiudadanos() throws ErrorDeAccesoADatosException {
        return ciudadanoDAO.listarCiudadanos();
    }

    @Override
    public List<Persona> listarPersonasNoCiudadanos() throws ErrorDeAccesoADatosException {
        return personaDAO.findAllNoCiudadano();
    }

    @Override
    public List<Persona> listarPersonasNoPersonal() throws ErrorDeAccesoADatosException {
        return personaDAO.findAllNoPersonal();
    }

    @Override
    public List<Personal> listarPersonalPorInstitucion(String nombreInstitucion) throws ErrorDeAccesoADatosException {
        return personalDAO.findByInstitucion(nombreInstitucion);
    }

    @Override
    public List<Personal> listarPersonalDisponiblePorNumeroDePedido(Long numeroDePedido) throws ErrorDeAccesoADatosException {
        return personalDAO.listarPersonalDisponiblePorNumeroDePedido(numeroDePedido);
    }

    @Override
    public void agregarPersonalAInstitucion(String institucion, String dni, String nombre, String apellido, String calle, String nombreCiudad, int numeroDireccion, double latitud, double longitud, String email) throws ErrorDeAccesoADatosException {
        Ciudad ciudad = ciudadDAO.findByNombre(nombreCiudad);
        personalDAO.agregarAInstitucion(new Personal(
                nombre,
                apellido,
                new Direccion(
                        calle, numeroDireccion, ciudad, latitud, longitud
                ),
                dni,
                email,
                null
        ), institucion);
    }

    @Override
    public void agregarPersonalAInstitucion(String institucion, String dni, String email) throws ErrorDeAccesoADatosException {
        personalDAO.agregarAInstitucion(new Personal(
                personaDAO.findByDNI(dni),
                null,
                email
        ), institucion);
    }

    @Override
    public void modificarPersonal(String dniOriginal, String dni, String nombre, String apellido, String calle, String ciudad, int numeroDireccion, double latitud, double longitud, String email) throws ErrorDeAccesoADatosException {
        personalDAO.update(dniOriginal, new Personal(
                nombre,
                apellido,
                new Direccion(
                        calle, numeroDireccion, ciudadDAO.findByNombre(ciudad), latitud, longitud
                ),
                dni,
                email,
                null
        ));
    }

    @Override
    public void eliminarPersonalDeInstitucion(String dni) throws ErrorDeAccesoADatosException {
        personalDAO.eliminarDeInstitucion(dni);
    }


    @Override
    public Long generarPedidoDeRetiro(Long codigoCampania, String dniCiudadado, String descripcion,
                                      boolean requiereVehiculoDeCarga) throws ErrorDeAccesoADatosException {

        PedidoDeRetiro pedidoDeRetiro = new PedidoDeRetiro(campaniaDAO.findByCodigo(codigoCampania), new Date(),
                descripcion, requiereVehiculoDeCarga, ciudadanoDAO.findByDNI(dniCiudadado));
        return pedidosDAO.create(pedidoDeRetiro);
    }

    @Override
    public void modificarPedidoDeRetiro(Long numero, String dniCiudadado, String descripcion, boolean requiereVehiculoDeCarga) throws ErrorDeAccesoADatosException {
        PedidoDeRetiro pedidoDeRetiro = new PedidoDeRetiro(numero, null, new Date(), descripcion, requiereVehiculoDeCarga, ciudadanoDAO.findByDNI(dniCiudadado));
        pedidosDAO.update(pedidoDeRetiro);
    }

    @Override
    public List<PedidoDeRetiro> listarPedidosDeRetiroPorCampania(Long codigoCampania) throws ErrorDeAccesoADatosException {
        return pedidosDAO.findByCampania(codigoCampania);
    }

    @Override
    public List<PedidoDeRetiro> listarPedidosDeRetiroPorCiudadano(String dni) throws ErrorDeAccesoADatosException {
        return pedidosDAO.findByCiudadano(ciudadanoDAO.findByDNI(dni));
    }

    @Override
    public void eliminarPedido(Long numeroDeRetiro) throws ErrorDeAccesoADatosException {
        pedidosDAO.eliminarPedido(numeroDeRetiro);
    }

    @Override
    public List<Ciudad> listarCiudades() throws ErrorDeAccesoADatosException {
        return ciudadDAO.findAll();
    }

    @Override
    public void crearCiudad(String nombre) throws ErrorDeAccesoADatosException {
        ciudadDAO.create(nombre);
    }

    @Override
    public void updateCiudad(String nombreAntiguo, String nuevoNombre) throws ErrorDeAccesoADatosException {
        ciudadDAO.update(nombreAntiguo, nuevoNombre);
    }

    @Override
    public List<LugarDeDonacion> findAllLugares() throws ErrorDeAccesoADatosException {
        return lugaresDAO.findAllLugares();
    }

    @Override
    public List<LugarDeDonacion> findAllLugaresByCampania(Long codigoCampania) throws ErrorDeAccesoADatosException {
        return lugaresDAO.findAllLugaresByCampania(codigoCampania);
    }

    @Override
    public void crearLugarDeDonacion(String nombre, String contacto, String calle, String ciudad, int numeroDireccion, double latitud, double longitud) throws ErrorDeAccesoADatosException {
        lugaresDAO.crearLugarDeDonacion(new LugarDeDonacion(
                nombre,
                contacto,
                new Direccion(
                        calle, numeroDireccion, ciudadDAO.findByNombre(ciudad), latitud, longitud
                )
        ));
    }

    @Override
    public void updateLugarDeDonacion(String nombreOriginal, String nombre, String contacto, String calle, String ciudad, int numeroDireccion, double latitud, double longitud) throws ErrorDeAccesoADatosException {
        lugaresDAO.updateLugarDeDonacion(
                nombreOriginal,
                new LugarDeDonacion(
                        nombre,
                        contacto,
                        new Direccion(
                                calle, numeroDireccion, ciudadDAO.findByNombre(ciudad), latitud, longitud
                        )
                ));
    }

    @Override
    public void asignarLugarACampania(String nombreLugar, Long codigoCampania) throws ErrorDeAccesoADatosException {
        lugaresDAO.asignarLugarACampania(nombreLugar, codigoCampania);
    }

    @Override
    public void eliminarLugarDeCampania(String nombreLugar, Long codigoCampania) throws ErrorDeAccesoADatosException {
        lugaresDAO.eliminarLugarDeCampania(nombreLugar, codigoCampania);
    }

    @Override
    public void generarOrdenDeRetiro(Long numeroDePedido, String dniPersonal) throws ErrorDeAccesoADatosException {
        PedidoDeRetiro pedidoDeRetiro = pedidosDAO.find(numeroDePedido);
        Personal personal = personalDAO.findByDNI(dniPersonal);
        OrdenDeRetiro ordenDeRetiro = pedidoDeRetiro.generarOrdenDeRetiro(personal);
        ordenesDAO.saveOrdenDeRetiro(ordenDeRetiro);
    }

    @Override
    public void reasignarOrdenDeRetiro(Long numeroDePedido, String dniPersonal) throws ErrorDeAccesoADatosException {
        ordenesDAO.reasignarOrdenDeRetiro(numeroDePedido, dniPersonal);
    }

    @Override
    public List<OrdenDeRetiro> listarOrdenesPorCampania(Long codigoDeCampania) throws ErrorDeAccesoADatosException {
        return ordenesDAO.listarOrdenesPorCampania(codigoDeCampania);
    }

    @Override
    public OrdenDeRetiro buscarOrdenPorNumeroDePedido(Long numeroDePedido) throws ErrorDeAccesoADatosException {
        return ordenesDAO.findByNumero(numeroDePedido);
    }

    @Override
    public void finalizarOrden(Long numeroDePedido) throws ErrorDeAccesoADatosException, CambioDeEstadoException {
        OrdenDeRetiro ordenDeRetiro = ordenesDAO.findByNumero(numeroDePedido);
        ordenDeRetiro.finalizar();
        ordenesDAO.update(ordenDeRetiro);
    }

    @Override
    public Donacion generarDonacion(Long numeroDePedido) throws ErrorDeAccesoADatosException, OrdenNoFinalizadaException {
        OrdenDeRetiro ordenDeRetiro = ordenesDAO.findByNumero(numeroDePedido);
        Donacion donacion = ordenDeRetiro.generarDonacion();
        donacionesDAO.guardarDonacionByPedido(donacion, ordenDeRetiro.getId());
        return donacion;
    }

    @Override
    public Long agregarVisita(Long numeroDePedido, String observacion, String fecha) throws ErrorDeAccesoADatosException, OrdenFinalizadaException {
        try {
            Date date = DateHelper.stringAFecha(fecha);
            OrdenDeRetiro ordenDeRetiro = ordenesDAO.findByNumero(numeroDePedido);
            ordenDeRetiro.agregarVisita(date, observacion);
            ordenesDAO.update(ordenDeRetiro);
            return visitasDAO.guardarVisita(new Visita(date, observacion), numeroDePedido);
        } catch (ParseException e) {
            throw new FormatoDeFechaIncorrectoException();
        }
    }

    @Override
    public void modificarVisita(Long numeroDeVisita, String observacion, String fecha) throws ErrorDeAccesoADatosException {
        try {
            visitasDAO.modificarVisita(new Visita(numeroDeVisita, DateHelper.stringAFecha(fecha), observacion));
        } catch (ParseException e) {
            throw new FormatoDeFechaIncorrectoException();
        }
    }

    @Override
    public void eliminarVisita(Long numeroDeVisita) throws ErrorDeAccesoADatosException {
        visitasDAO.eliminarVisita(numeroDeVisita);
    }

    @Override
    public List<Donacion> listarDonacionesPorCampania(Long codigoDeCampania) throws ErrorDeAccesoADatosException {
        return donacionesDAO.listarDonacionesPorCampania(codigoDeCampania);
    }

    @Override
    public void eliminarDonacion(Long codigoDeDonacion) throws ErrorDeAccesoADatosException {
        donacionesDAO.eliminarDonacion(codigoDeDonacion);
    }

    @Override
    public void guardarDonacion(Long codigoCampania, String lugar, String descripcion, String fecha) throws ErrorDeAccesoADatosException, ParseException {
        LugarDeDonacion lugarDeDonacion = lugaresDAO.findByNombre(lugar);
        donacionesDAO.guardarDonacion(new Donacion(DateHelper.stringAFecha(fecha), descripcion, lugarDeDonacion.getDireccion()), codigoCampania);
    }
}
