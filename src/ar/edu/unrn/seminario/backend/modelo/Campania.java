package ar.edu.unrn.seminario.backend.modelo;

import ar.edu.unrn.seminario.backend.exception.DatoNullException;
import ar.edu.unrn.seminario.backend.exception.RangoDeFechasException;
import ar.edu.unrn.seminario.backend.exception.StringInvalidoException;
import ar.edu.unrn.seminario.backend.helpers.DateHelper;
import ar.edu.unrn.seminario.backend.helpers.ValidadorDeDatos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Campania extends EntidadGenerica {

    private Date fechaInicio, fechaFin;
    private String descripcion, motivo;
    private List<PedidoDeRetiro> pedidosDeRetiro;
    private List<OrdenDeRetiro> ordenesDeRetiro;
    private List<LugarDeDonacion> lugaresDeDonacion;
    private List<Donacion> donaciones;
    private Institucion institucion;

    public Campania(Date fechaInicio, Date fechaFin, String descripcion, String motivo, Institucion institucion) {

        if (ValidadorDeDatos.esRangoDeFechasInvalido(fechaInicio, fechaFin))
            throw new RangoDeFechasException(fechaInicio, fechaFin);

        if (ValidadorDeDatos.esStringVacioONull(descripcion))
            throw new StringInvalidoException("descripcion");

        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.descripcion = descripcion;
        this.motivo = motivo;
        this.institucion = institucion;
        this.lugaresDeDonacion = new ArrayList<>();
        this.pedidosDeRetiro = new ArrayList<>();
        this.ordenesDeRetiro = new ArrayList<>();
        this.donaciones = new ArrayList<>();
    }

    public Campania(Long id, Date fechaInicio, Date fechaFin, String descripcion, String motivo, Institucion institucion) {
        this(fechaInicio, fechaFin, descripcion, motivo, institucion);
        this.setId(id);
    }

    public void agregarLugarDeDonacion(LugarDeDonacion lugar) {
        this.lugaresDeDonacion.add(lugar);
    }


    public void agregarOrdenDeRetiro(OrdenDeRetiro orden) {
        this.ordenesDeRetiro.add(orden);
    }

    public void agregarDonacion(Donacion donacion) {
        this.donaciones.add(donacion);
    }

    public PedidoDeRetiro generarPedidoDeRetiro(Ciudadano ciudadano, String descripcion, boolean requiereVehiculoDeCarga) {
        PedidoDeRetiro pedidoDeRetiro = new PedidoDeRetiro(this, new Date(), descripcion, requiereVehiculoDeCarga, ciudadano);
        this.pedidosDeRetiro.add(pedidoDeRetiro);
        ciudadano.agregarPedidoDeRetiro(pedidoDeRetiro);
        return pedidoDeRetiro;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getMotivo() {
        return motivo;
    }

    public String getFechaInicioComoString() {
        return DateHelper.fechaAString(fechaInicio);
    }

    public String getFechaFinComoString() {
        return DateHelper.fechaAString(fechaFin);
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public List<LugarDeDonacion> getLugaresDeDonacion() {
        return lugaresDeDonacion;
    }

    public Institucion getInstitucion() {
        return institucion;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Campania{")
                .append("fechaInicio=")
                .append(fechaInicio)
                .append(", fechaFin=")
                .append(fechaFin)
                .append(", descripcion=")
                .append(descripcion)
                .append(", motivo=")
                .append(motivo)
                .append('}').toString();
    }

}
