package ar.edu.unrn.seminario.backend.modelo;

import ar.edu.unrn.seminario.backend.exception.DatoNullException;
import ar.edu.unrn.seminario.backend.exception.StringInvalidoException;
import ar.edu.unrn.seminario.backend.helpers.ValidadorDeDatos;

import java.util.Date;

public class Donacion extends EntidadGenerica {
    private Date fecha;
    private String descripcion;
    private Direccion origen;

    public Donacion(Date fecha, String descripcion, Direccion origen) {

        if(ValidadorDeDatos.esNull(fecha))
    		throw new DatoNullException("fecha");
        if(ValidadorDeDatos.esNull(origen))
            throw new DatoNullException("origen");
    	if(ValidadorDeDatos.esStringVacioONull(descripcion))
    		throw new StringInvalidoException("descripcion");

        this.fecha = fecha;
        this.descripcion = descripcion;
        this.origen = origen;
    }

    public Donacion(Long id, Date fecha, String descripcion, Direccion origen) {
        this(fecha, descripcion, origen);
        this.setId(id);
    }

    public Date getFecha() {
        return fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Direccion getOrigen() {
        return origen;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Donacion{")
                .append(", fecha=")
                .append(fecha)
                .append(", descripcion=")
                .append(descripcion)
                .append(", origen=")
                .append(origen)
                .append('}')
                .toString();
    }
}
