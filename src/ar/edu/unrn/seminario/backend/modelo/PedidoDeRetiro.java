package ar.edu.unrn.seminario.backend.modelo;

import ar.edu.unrn.seminario.backend.exception.DatoNullException;
import ar.edu.unrn.seminario.backend.helpers.ValidadorDeDatos;

import java.util.Date;

public class PedidoDeRetiro extends EntidadGenerica {

    private Date fechaEmision;
    private String descripcion;
    private boolean requiereVehiculoDeCarga;
    private Ciudadano ciudadano;
    private Campania campania;

    public PedidoDeRetiro(Campania campania, Date fechaEmision, String descripcion,
    		boolean requiereVehiculoDeCarga, Ciudadano ciudadano) {

        if (ValidadorDeDatos.esNull(fechaEmision))
            throw new DatoNullException("fecha de emision");


        this.fechaEmision = fechaEmision;
        this.descripcion = descripcion;
        this.requiereVehiculoDeCarga = requiereVehiculoDeCarga;
        this.ciudadano = ciudadano;
        this.campania = campania;
    }

    public PedidoDeRetiro(Long id, Campania campania, Date fechaEmision, String descripcion,
                          boolean requiereVehiculoDeCarga, Ciudadano ciudadano){
        this(campania, fechaEmision, descripcion, requiereVehiculoDeCarga, ciudadano);
        this.setId(id);
    }

    public OrdenDeRetiro generarOrdenDeRetiro(Personal personal){
        if(ValidadorDeDatos.esNull(personal))
            throw new DatoNullException("personal");

        return new OrdenDeRetiro(this, personal);
    }

    public Direccion getDireccion(){
        return ciudadano.obtenerDireccion();
    }

    public String getDescripcion(){
        return descripcion;
    }

    public Campania getCampania() {
    	return this.campania;
    }

    public Ciudadano getCiudadano(){
        return ciudadano;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public boolean isRequiereVehiculoDeCarga() {
        return requiereVehiculoDeCarga;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("PedidoDeRetiro{").append("fechaEmision=")
                .append(fechaEmision).append(", descripcion=").append(descripcion)
                .append(", requiereVehiculoDeCarga=").append(requiereVehiculoDeCarga)
                .append('}').toString();
    }
}
