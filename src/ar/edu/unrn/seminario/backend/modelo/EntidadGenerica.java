package ar.edu.unrn.seminario.backend.modelo;

public abstract class EntidadGenerica {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o instanceof EntidadGenerica)
            return this.id.equals(((EntidadGenerica) o).id);

        return false;
    }
}
