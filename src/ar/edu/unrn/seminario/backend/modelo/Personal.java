package ar.edu.unrn.seminario.backend.modelo;

import ar.edu.unrn.seminario.backend.exception.DatoNullException;
import ar.edu.unrn.seminario.backend.helpers.ValidadorDeDatos;

import java.util.ArrayList;
import java.util.List;

public class Personal extends EntidadGenerica {

    private String foto, email;
    private Persona persona;
    private List<OrdenDeRetiro> ordenesAsignadas;

    public Personal(Persona persona, String foto, String email) {

    	if(ValidadorDeDatos.esNull(persona))
    		throw new DatoNullException("persona");

        if(ValidadorDeDatos.esNull(email))
            throw new DatoNullException("email");

        this.persona = persona;
        this.foto = foto;
        this.email = email;
        this.ordenesAsignadas = new ArrayList<>();
    }

    public Personal(String nombre, String apellido, Direccion direccion, String dni, String email, String foto) {
        if(ValidadorDeDatos.esNull(email))
            throw new DatoNullException("email");

        this.persona = new Persona(nombre, apellido, direccion, dni);
    	this.foto = foto;
    	this.email = email;
    }

    public Persona getPersona() {
        return persona;
    }

    public String getFoto() {
        return foto;
    }

    public String getEmail() {
        return email;
    }

    public void asignarOrdenDeRetiro(OrdenDeRetiro ordenDeRetiro){
        this.ordenesAsignadas.add(ordenDeRetiro);
    }

    @Override
    public String toString() {
        return this.persona.toString();
    }
}
