package ar.edu.unrn.seminario.backend.modelo;

import ar.edu.unrn.seminario.backend.exception.StringInvalidoException;
import ar.edu.unrn.seminario.backend.helpers.ValidadorDeDatos;

public class Ciudad extends EntidadGenerica {

    private String nombre;

    public Ciudad(String nombre) {
        if (ValidadorDeDatos.esStringVacioONull(nombre))
            throw new StringInvalidoException("nombre");
        this.nombre = nombre;
    }

    public Ciudad(Long id, String nombre) {
        this(nombre);
        this.setId(id);
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Ciudad{")
                .append("nombre=")
                .append(nombre)
                .append('}')
                .toString();
    }
}
