package ar.edu.unrn.seminario.backend.modelo;

import ar.edu.unrn.seminario.backend.exception.DatoNullException;
import ar.edu.unrn.seminario.backend.helpers.ValidadorDeDatos;

import java.util.ArrayList;
import java.util.List;

public class Ciudadano extends EntidadGenerica {

    private Persona persona;
    private List<PedidoDeRetiro> pedidosDeRetiro;
    private List<Donacion> donaciones;

    public Ciudadano(Persona persona) {
    	if(ValidadorDeDatos.esNull(persona))
    		throw new DatoNullException("persona");

        this.persona = persona;
        this.pedidosDeRetiro = new ArrayList<>();
        this.donaciones = new ArrayList<>();
    }

    public Ciudadano(Long id, Persona persona){
        this(persona);
        this.setId(id);
    }

    public Direccion obtenerDireccion(){
        return persona.getDireccion();
    }

    public void agregarDonacion(Donacion donacion){
        this.donaciones.add(donacion);
    }

    public void agregarPedidoDeRetiro(PedidoDeRetiro pedidoDeRetiro){
        this.pedidosDeRetiro.add(pedidoDeRetiro);
    }

    public Persona getPersona() {
        return persona;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Ciudadano{")
                .append("persona=")
                .append(persona)
                .append('}')
                .toString();
    }
}
