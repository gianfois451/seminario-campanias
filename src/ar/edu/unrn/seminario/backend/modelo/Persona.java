package ar.edu.unrn.seminario.backend.modelo;

import ar.edu.unrn.seminario.backend.exception.DatoNullException;
import ar.edu.unrn.seminario.backend.exception.DniInvalidoException;
import ar.edu.unrn.seminario.backend.exception.StringInvalidoException;
import ar.edu.unrn.seminario.backend.helpers.ValidadorDeDatos;

public class Persona extends EntidadGenerica {

    private String nombre, apellido;
    private String dni;
    private Direccion direccion;

    public Persona(String nombre, String apellido, Direccion direccion, String dni) {

        if (ValidadorDeDatos.esStringVacioONull(nombre))
            throw new StringInvalidoException("nombre");

        if (ValidadorDeDatos.esDniInvalido(dni))
            throw new DniInvalidoException(dni);

        if (ValidadorDeDatos.esStringVacioONull(apellido))
            throw new StringInvalidoException("apellido");

        this.nombre = nombre;
        this.dni = dni;
        this.apellido = apellido;
        this.direccion = direccion;
    }

    public Persona(Long id, String nombre, String apellido, Direccion direccion, String dni) {
        this(nombre, apellido, direccion, dni);
        this.setId(id);
    }


    public Direccion getDireccion(){
        return direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getDni() {
        return dni;
    }

    public String getNombreCompleto(){
        return this.nombre + " " + this.apellido;
    }

    public String getNombreCompletoConDNI(){
        return this.nombre + " " + this.apellido + " (DNI " + dni + ")";
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Persona{")
                .append("nombre=")
                .append(nombre)
                .append(", apellido=")
                .append(apellido)
                .append(", dni=")
                .append(dni)
                .append(", direccion=")
                .append(direccion)
                .append('}')
                .toString();
    }
}
