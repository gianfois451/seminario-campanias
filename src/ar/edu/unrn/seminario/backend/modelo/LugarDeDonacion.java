package ar.edu.unrn.seminario.backend.modelo;

import ar.edu.unrn.seminario.backend.exception.StringInvalidoException;
import ar.edu.unrn.seminario.backend.helpers.ValidadorDeDatos;

public class LugarDeDonacion extends EntidadGenerica {

    private String nombre, contacto;
    private Direccion direccion;


    public LugarDeDonacion(String nombre, String contacto, Direccion direccion) {
        if (ValidadorDeDatos.esStringVacioONull(nombre))
            throw new StringInvalidoException("nombre");
        if (ValidadorDeDatos.esStringVacioONull(contacto))
            throw new StringInvalidoException("contacto");
        if (ValidadorDeDatos.esNull(direccion))
            throw new StringInvalidoException("direccion");

        this.nombre = nombre;
        this.contacto = contacto;
        this.direccion = direccion;
    }

    public LugarDeDonacion(Long id, String nombre, String contacto, Direccion direccion) {
        this(nombre, contacto, direccion);
        this.setId(id);
    }

    public LugarDeDonacion(String nombre) {
        if (ValidadorDeDatos.esStringVacioONull(nombre))
            throw new StringInvalidoException("nombre");
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public String getNombreYDireccion() {
        return nombre + " (" + this.direccion.getCalle() + ", " + direccion.getNumero() + ", " + direccion.getCiudad().getNombre() + ")";
    }

    public String getContacto() {
        return contacto;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("LugarDeDonacion{")
                .append("nombre=")
                .append(nombre)
                .append(", contacto=")
                .append(contacto)
                .append(", direccion=")
                .append(direccion)
                .append('}')
                .toString();
    }
}

