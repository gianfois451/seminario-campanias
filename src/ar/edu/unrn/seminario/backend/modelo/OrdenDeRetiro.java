package ar.edu.unrn.seminario.backend.modelo;

import ar.edu.unrn.seminario.backend.exception.CambioDeEstadoException;
import ar.edu.unrn.seminario.backend.exception.DatoNullException;
import ar.edu.unrn.seminario.backend.exception.OrdenFinalizadaException;
import ar.edu.unrn.seminario.backend.exception.OrdenNoFinalizadaException;
import ar.edu.unrn.seminario.backend.helpers.ValidadorDeDatos;
import com.sun.org.apache.xpath.internal.operations.Or;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

public class OrdenDeRetiro extends EntidadGenerica {

    private PedidoDeRetiro pedidoDeRetiro;
    private Estado estado;
    private Date fechaEmision;
    private Personal personalAsignado;
    private List<Visita> visitas;

    public enum Estado {
        SIN_RETIRAR("Sin retirar"),
        RETIRANDO("Retirando"),
        FINALIZADO("Finalizado");

        private String nombre;

        Estado(String nombre) {
            this.nombre = nombre;
        }

        public static Estado fromString(String value){
            return Stream.of(Estado.values()).filter(e -> e.nombre.equals(value)).findFirst().orElse(null);
        }

        @Override
        public String toString() {
            return nombre;
        }
    }

    public OrdenDeRetiro(PedidoDeRetiro pedidoDeRetiro, Personal personalAsignado) {
        if (ValidadorDeDatos.esNull(pedidoDeRetiro))
            throw new DatoNullException("pedido de retiro");

        if (ValidadorDeDatos.esNull(personalAsignado))
            throw new DatoNullException("personal asignado");

        this.pedidoDeRetiro = pedidoDeRetiro;
        this.personalAsignado = personalAsignado;
        this.fechaEmision = new Date();
        this.estado = Estado.SIN_RETIRAR;
        this.visitas = new ArrayList<>();
    }

    public OrdenDeRetiro(Long id, String estado, PedidoDeRetiro pedidoDeRetiro, Personal personalAsignado) {
        this(pedidoDeRetiro, personalAsignado);
        this.setId(id);
        this.estado = OrdenDeRetiro.Estado.fromString(estado);
    }


    public void finalizar() throws CambioDeEstadoException {
        if (this.estado == Estado.RETIRANDO) {
            this.estado = Estado.FINALIZADO;
        } else {
            throw new CambioDeEstadoException(this.estado.toString(), Estado.FINALIZADO.toString());
        }
    }

    public Donacion generarDonacion() throws OrdenNoFinalizadaException {
        if (this.estado == Estado.FINALIZADO) {
            return new Donacion(new Date(), pedidoDeRetiro.getDescripcion(),
                    pedidoDeRetiro.getDireccion());
        } else {
            throw new OrdenNoFinalizadaException();
        }
    }

    public void agregarVisita(String observacion) throws OrdenFinalizadaException {
        this.agregarVisita(new Date(), observacion);
    }

    public void agregarVisita(Date fecha, String observacion) throws OrdenFinalizadaException {
        if (this.estado != Estado.FINALIZADO) {
            if (this.estado == Estado.SIN_RETIRAR) {
                this.estado = Estado.RETIRANDO;
            }
            this.visitas.add(new Visita(fecha, observacion));
        } else {
            throw new OrdenFinalizadaException();
        }
    }

    public String getEstadoString() {
        return estado.toString();
    }

    public boolean estaFinalizada(){
        return this.estado.equals(Estado.FINALIZADO);
    }

    public PedidoDeRetiro getPedidoDeRetiro() {
        return pedidoDeRetiro;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public Personal getPersonalAsignado() {
        return personalAsignado;
    }

    public void setVisitas(List<Visita> visitas) {
        this.visitas = visitas;
    }

    public List<Visita> getVisitas() {
        return visitas;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("OrdenDeRetiro{")
                .append("pedidoDeRetiro=")
                .append(pedidoDeRetiro)
                .append(", estado=")
                .append(estado)
                .append(", fechaEmision=")
                .append(fechaEmision)
                .append(", personalAsignado=")
                .append(personalAsignado)
                .append('}').toString();
    }
}
