package ar.edu.unrn.seminario.backend.modelo;

import ar.edu.unrn.seminario.backend.exception.DatoNullException;
import ar.edu.unrn.seminario.backend.exception.StringInvalidoException;
import ar.edu.unrn.seminario.backend.helpers.ValidadorDeDatos;

import java.util.ArrayList;
import java.util.List;

public class Institucion extends EntidadGenerica {

    private String nombre, cuil, contacto;
    private List<Personal> personal;
    private List<Campania> campanias;
    private Direccion direccion;

    public Institucion(String nombre, String cuil, String contacto, Direccion direccion, List<Personal> personal) {

        if (ValidadorDeDatos.esStringVacioONull(nombre))
            throw new StringInvalidoException("nombre");

        if (ValidadorDeDatos.esStringVacioONull(cuil))
            throw new StringInvalidoException("cuil");

        this.nombre = nombre;
        this.cuil = cuil;
        this.contacto = contacto;
        this.direccion = direccion;
        this.personal = personal;
        this.campanias = new ArrayList<>();
    }

    public Institucion(String nombre, String cuil, String contacto, Direccion direccion) {
        this(nombre, cuil, contacto, direccion, new ArrayList<>());
    }

    public Institucion(Long id, String nombre, String cuil, String contacto, Direccion direccion) {
        this(nombre, cuil, contacto, direccion);
        this.setId(id);
    }

    public void agregarCampania(Campania campania){
        this.campanias.add(campania);
    }

    public void agregarPersonal(Personal personal){
        this.personal.add(personal);
    }

    public String getNombre() {
        return nombre;
    }

    public String getCuil() {
        return cuil;
    }

    public String getContacto() {
        return contacto;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("Institucion{").append("nombre=").append(nombre).append(", cuil=")
                .append(cuil).append(", contacto=").append(contacto).append('}').toString();
    }
}
