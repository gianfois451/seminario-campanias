package ar.edu.unrn.seminario.backend.modelo;

import ar.edu.unrn.seminario.backend.exception.CoordenadaInvalidaException;
import ar.edu.unrn.seminario.backend.exception.DatoNullException;
import ar.edu.unrn.seminario.backend.exception.StringInvalidoException;
import ar.edu.unrn.seminario.backend.helpers.ValidadorDeDatos;

public class Direccion extends EntidadGenerica {

    private String calle;
    private int numero;
    private double latitud;
    private double longitud;
    private Ciudad ciudad;

    public Direccion(String calle, int numero, Ciudad ciudad, double latitud, double longitud) {

    	if(ValidadorDeDatos.esStringVacioONull(calle))
    		throw new StringInvalidoException("calle");

    	if(ValidadorDeDatos.esNull(ciudad))
    		throw new DatoNullException("ciudad");

    	if(ValidadorDeDatos.esCoordenadaInvalida(latitud, longitud))
    		throw new CoordenadaInvalidaException(latitud, longitud);

        this.calle = calle;
        this.numero = numero;
        this.ciudad = ciudad;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public Direccion(Long id, String calle, int numero, Ciudad ciudad, double latitud, double longitud) {
        this(calle,numero,ciudad,latitud,longitud);
        this.setId(id);
    }

    public String getCalle() {
        return calle;
    }

    public int getNumero() {
        return numero;
    }

    public double getLatitud() {
        return latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    @Override
    public String toString() {
        return new StringBuffer()
        		.append(calle)
        		.append(" ")
        		.append(numero)
                .append(", ")
                .append(ciudad.getNombre())
        		.toString();
    }
}
