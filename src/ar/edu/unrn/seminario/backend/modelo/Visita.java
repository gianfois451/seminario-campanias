package ar.edu.unrn.seminario.backend.modelo;

import java.util.Date;

public class Visita extends EntidadGenerica {

    private Date fecha;
    private String observacion;

    public Visita(String observacion) {
        this.observacion = observacion;
        this.fecha = new Date();
    }

    public Visita(Date fecha, String observacion) {
        this.fecha = fecha;
        this.observacion = observacion;
    }

    public Visita(Long id, Date fecha, String observacion){
        this(fecha, observacion);
        this.setId(id);
    }

    public Date getFecha() {
        return fecha;
    }

    public String getObservacion() {
        return observacion;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("Visita{").append("fecha=").append(fecha).append(", observacion=")
                .append(observacion).append('}').toString();
    }
}
