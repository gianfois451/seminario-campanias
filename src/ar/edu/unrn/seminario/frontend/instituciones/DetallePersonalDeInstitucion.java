package ar.edu.unrn.seminario.frontend.instituciones;

import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Personal;
import ar.edu.unrn.seminario.frontend.personal.EditorPersonal;
import ar.edu.unrn.seminario.frontend.varios.ButtonEditor;
import ar.edu.unrn.seminario.frontend.varios.ButtonRenderer;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.util.List;

public class DetallePersonalDeInstitucion extends JFrame {

    private static IFacade facade = new ApiFacade();
    private String institucion;
    private JTable jTable;
    private final String[] columnas = {"Nombre", "Apellido", "Editar", "Eliminar"};
    private List<Personal> personal = new ArrayList<>();

    public DetallePersonalDeInstitucion(String nombreInstitucion){
        this.institucion = nombreInstitucion;

        setTitle("Personal de " + nombreInstitucion);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(120, 120, 500, 300);

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
        add(contentPane);

        this.cargarDatos();


        /* personal */
        JLabel personalLabel = new JLabel(personal.size() > 0 ?"Personal:" : "(No hay personal)");
        personalLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        personalLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
        contentPane.add(personalLabel);

        jTable = new JTable(this.generarDatosDeTabla(), columnas);
        jTable.setModel(new DefaultTableModel(this.generarDatosDeTabla(), columnas));
        JScrollPane scrollPane = new JScrollPane(jTable);
        contentPane.add(scrollPane);
        configurarAcciones();

        JButton personalButton = new JButton("Agregar personal");
        personalButton.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        personalButton.addActionListener(evento -> this.agregarPersonal());
        contentPane.add(personalButton);


        add(contentPane);


        this.setVisible(true);
    }

    private void configurarAcciones() {

        jTable.getColumn("Editar").setCellRenderer(new ButtonRenderer());
        jTable.getColumn("Editar").setCellEditor(new ButtonEditor<>(new JCheckBox(), personal, (selected) -> {
            new EditorPersonal(
                    selected.getPersona().getNombre(),
                    selected.getPersona().getApellido(),
                    selected.getPersona().getDni(),
                    selected.getPersona().getDireccion().getCalle(),
                    Integer.toString(selected.getPersona().getDireccion().getNumero()),
                    Double.toString(selected.getPersona().getDireccion().getLatitud()),
                    Double.toString(selected.getPersona().getDireccion().getLongitud()),
                    selected.getPersona().getDireccion().getCiudad().getNombre(),
                    selected.getEmail(),
                    (dni, nombre, apellido, calle, ciudad, numeroDireccion, latitud, longitud, email) -> {
                        try {
                            facade.modificarPersonal(selected.getPersona().getDni(), dni, nombre, apellido, calle,
                                    ciudad, numeroDireccion, latitud, longitud, email);
                            this.cargarDatos();
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(this,
                                    e.getMessage(),
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
            );
        }));


        jTable.getColumn("Eliminar").setCellRenderer(new ButtonRenderer());
        jTable.getColumn("Eliminar").setCellEditor(new ButtonEditor<>(new JCheckBox(), personal, (selected) -> {
            int dialogResult = JOptionPane.showConfirmDialog (this, "¿Seguro que desea eliminar a " + selected.getPersona().getNombreCompleto() + "?","Confirmar", JOptionPane.YES_NO_OPTION );
            if(dialogResult == JOptionPane.YES_OPTION){
                try {
                    facade.eliminarPersonalDeInstitucion(selected.getPersona().getDni());
                    this.cargarDatos();
                } catch (ErrorDeAccesoADatosException e) {
                    JOptionPane.showMessageDialog(this,
                            e.getMessage(),
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }));



    }


    private Object[][] generarDatosDeTabla() {
        Object[][] datos = new Object[personal.size()][4];
        for (int i = 0; i < personal.size(); i++) {
            Personal p = personal.get(i);
            datos[i][0] = p.getPersona().getNombre();
            datos[i][1] = p.getPersona().getApellido();
            datos[i][2] = "Editar";
            datos[i][3] = "Eliminar";
        }
        return datos;
    }

    private void cargarDatos() {
        try {
            personal = facade.listarPersonalPorInstitucion(this.institucion);
            if (jTable != null) {
                jTable.setModel(new DefaultTableModel(this.generarDatosDeTabla(), columnas));
                configurarAcciones();
            }
        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }


    private void agregarPersonal(){
        new EditorPersonal((dni, nombre, apellido, calle, ciudad, numeroDireccion, latitud, longitud, email) -> {
            try {
                facade.agregarPersonalAInstitucion(this.institucion, dni, nombre, apellido, calle, ciudad, numeroDireccion, latitud, longitud, email);
                this.cargarDatos();
            } catch (ErrorDeAccesoADatosException e) {
                JOptionPane.showMessageDialog(this,
                        e.getMessage(),
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        });
    }

}
