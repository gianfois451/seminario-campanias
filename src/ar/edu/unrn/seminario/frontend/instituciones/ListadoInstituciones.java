package ar.edu.unrn.seminario.frontend.instituciones;

import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Campania;
import ar.edu.unrn.seminario.backend.modelo.Institucion;
import ar.edu.unrn.seminario.frontend.campanias.EditorDeCampania;
import ar.edu.unrn.seminario.frontend.campanias.ListadoCampanias;
import ar.edu.unrn.seminario.frontend.varios.ButtonEditor;
import ar.edu.unrn.seminario.frontend.varios.ButtonRenderer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;

public class ListadoInstituciones extends JFrame {
    private List<Institucion> instituciones;
    private IFacade api = new ApiFacade();
    private JTable tabla;
    private final String[] columas = {"Nombre", "CUIL", "Contacto", "Editar", "Personal"};

    public ListadoInstituciones() {
        try {
            JPanel contentPane = new JPanel(new BorderLayout());
            add(contentPane);
            setTitle("Listado de instituciones");
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            setBounds(120, 120, 700, 300);
            this.cargarDatos();
            tabla = new JTable(this.generarDatosDeTabla(), columas);
            tabla.setModel(new DefaultTableModel(this.generarDatosDeTabla(), columas));
            configurarAcciones();

            JScrollPane scrollPane = new JScrollPane(tabla);
            contentPane.add(scrollPane, BorderLayout.NORTH);
            setVisible(true);
        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.setVisible(false);
            this.dispose();

        }
    }

    private Object[][] generarDatosDeTabla(){
        Object[][] datos = new Object[instituciones.size()][5];
        for (int i = 0; i < instituciones.size(); i++) {
            Institucion institucion = instituciones.get(i);
            datos[i][0] = institucion.getNombre();
            datos[i][1] = institucion.getCuil();
            datos[i][2] = institucion.getContacto();
            datos[i][3] = "Editar";
            datos[i][4] = "Personal";
        }
        return datos;
    }

    private void configurarAcciones(){
        tabla.getColumn("Editar").setCellRenderer(new ButtonRenderer());
        tabla.getColumn("Editar").setCellEditor(new ButtonEditor<>(new JCheckBox(), instituciones, (selected) -> {
            new EditorInstituciones(
                    selected.getNombre(),
                    selected.getCuil(),
                    selected.getContacto(),
                    selected.getDireccion().getCalle(),
                    Integer.toString(selected.getDireccion().getNumero()),
                    Double.toString(selected.getDireccion().getLatitud()),
                    Double.toString(selected.getDireccion().getLongitud()),
                    selected.getDireccion().getCiudad().getNombre(),
                    (nombre, cuil, contacto, calle, ciudad, numero, latitud, longitud) -> {
                        try {
                            this.api.modificarInstitucion(selected.getNombre(), nombre, cuil, contacto, calle,
                                    ciudad, numero, latitud, longitud);
                            this.cargarDatos();
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(this,
                                    e.getMessage(),
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
            );
        }));


        tabla.getColumn("Personal").setCellRenderer(new ButtonRenderer());
        tabla.getColumn("Personal").setCellEditor(new ButtonEditor<>(new JCheckBox(), instituciones, (selected) -> {
            new DetallePersonalDeInstitucion(selected.getNombre());
        }));
    }

    private void cargarDatos() throws ErrorDeAccesoADatosException {
        try {
            instituciones = api.listarInstituciones();
            if(tabla != null) {
                tabla.setModel(new DefaultTableModel(this.generarDatosDeTabla(), columas));
                configurarAcciones();
            }
        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            throw e;
        }
    }

    public static void main(String[] args) {
        ListadoInstituciones frame = new ListadoInstituciones();
        frame.setVisible(true);
    }
}
