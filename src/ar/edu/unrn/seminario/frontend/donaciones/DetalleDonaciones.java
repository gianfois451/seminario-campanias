package ar.edu.unrn.seminario.frontend.donaciones;

import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.helpers.DateHelper;
import ar.edu.unrn.seminario.backend.modelo.Donacion;
import ar.edu.unrn.seminario.backend.modelo.Visita;
import ar.edu.unrn.seminario.frontend.varios.ButtonEditor;
import ar.edu.unrn.seminario.frontend.varios.ButtonRenderer;
import ar.edu.unrn.seminario.frontend.visitas.EditorVisitas;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.text.ParseException;
import java.util.List;

public class DetalleDonaciones extends JFrame {

    private IFacade api = new ApiFacade();
    private List<Donacion> donaciones;
    private Long codigoCampania;
    private JTable jTable;
    private JButton addDonacionButton;
    private final String[] columnas = {"Fecha", "Descripcion", "Origen", "Eliminar"};

    public DetalleDonaciones(Long codigoCampania){
        this.codigoCampania = codigoCampania;
        this.cargarDatos();
        setTitle("Donaciones");
        setTitle("Donaciones");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(140, 140, 700, 450);

        JPanel contentPane = new JPanel();

        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
        add(contentPane);

        JPanel buttonsPanel = new JPanel(new FlowLayout());
        contentPane.add(buttonsPanel);

        addDonacionButton = new JButton("Agregar donacion");
        addDonacionButton.addActionListener(actionEvent -> {
            new AgregarDonacion(this.codigoCampania, (lugar, descripcion, fecha) -> {
                try {
                    api.guardarDonacion(this.codigoCampania, lugar, descripcion, fecha);
                    this.cargarDatos();
                } catch (ErrorDeAccesoADatosException | ParseException e) {
                    JOptionPane.showMessageDialog(this,
                            e.getMessage(),
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            });
        });
        buttonsPanel.add(addDonacionButton);

        jTable = new JTable(this.generarDatosDeTabla(), columnas);
        jTable.setModel(new DefaultTableModel(this.generarDatosDeTabla(), columnas));


        JScrollPane scrollPane = new JScrollPane(jTable);
        contentPane.add(scrollPane);


        configurarAcciones();

        this.setVisible(true);
    }

    private void cargarDatos(){
        try {
            donaciones = api.listarDonacionesPorCampania(this.codigoCampania);
            if (jTable != null) {
                jTable.setModel(new DefaultTableModel(this.generarDatosDeTabla(), columnas));
                configurarAcciones();
            }
        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    private Object[][] generarDatosDeTabla() {
        Object[][] datos = new Object[donaciones.size()][4];
        for (int i = 0; i < donaciones.size(); i++) {
            Donacion donacion = donaciones.get(i);
            datos[i][0] = DateHelper.fechaAString(donacion.getFecha());
            datos[i][1] = donacion.getDescripcion();
            datos[i][2] = donacion.getOrigen().toString();
            datos[i][3] = "Eliminar";
        }
        return datos;
    }

    private void configurarAcciones() {

        jTable.getColumn("Eliminar").setCellRenderer(new ButtonRenderer());
        jTable.getColumn("Eliminar").setCellEditor(new ButtonEditor<>(new JCheckBox(), donaciones, (selected) -> {
            int dialogResult = JOptionPane.showConfirmDialog(this, "¿Seguro que desea eliminar la visita?", "Confirmar", JOptionPane.YES_NO_OPTION);
            if (dialogResult == JOptionPane.YES_OPTION) {
                try {
                    api.eliminarDonacion(selected.getId());
                    this.cargarDatos();
                } catch (ErrorDeAccesoADatosException e) {
                    JOptionPane.showMessageDialog(this,
                            e.getMessage(),
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }));
    }

}
