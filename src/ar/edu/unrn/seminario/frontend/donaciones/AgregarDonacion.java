package ar.edu.unrn.seminario.frontend.donaciones;

import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.helpers.DateHelper;
import ar.edu.unrn.seminario.backend.modelo.Ciudadano;
import ar.edu.unrn.seminario.backend.modelo.LugarDeDonacion;
import ar.edu.unrn.seminario.frontend.interfaces_funcionales.OnSaveNewDonacion;
import ar.edu.unrn.seminario.frontend.interfaces_funcionales.OnSavePedido;
import ar.edu.unrn.seminario.frontend.varios.SelectorDeFecha;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

public class AgregarDonacion extends JFrame {
    private OnSaveNewDonacion onSave;
    private JTextField descripcionField;
    private List<LugarDeDonacion> lugares;
    private JComboBox<String> comboBox;
    private SelectorDeFecha selectorDeFecha;

    private static final IFacade facade = new ApiFacade();

    public AgregarDonacion(Long codigoCampania, OnSaveNewDonacion onSave) {
        this.onSave = onSave;

        /* Ventana */
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
        add(contentPane);


        setTitle("Nueva donacion");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(120, 120, 450, 330);

        /* Form panel */
        JPanel formPanel = new JPanel();
        formPanel.setLayout(new BoxLayout(formPanel, BoxLayout.Y_AXIS));

        contentPane.add(formPanel);

        JLabel descripcionLabel = new JLabel("Descripcion:");
        descripcionLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        descripcionLabel.setBorder(BorderFactory.createEmptyBorder(10, 10, 5, 5));
        formPanel.add(descripcionLabel);
        descripcionField = new JTextField("");
        descripcionField.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.DARK_GRAY),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        descripcionField.setBackground(this.getBackground());
        formPanel.add(descripcionField);

        selectorDeFecha = new SelectorDeFecha();
        selectorDeFecha.setBorder(
                BorderFactory.createTitledBorder("Fecha"));
        formPanel.add(selectorDeFecha);

        try {
            this.lugares = facade.findAllLugaresByCampania(codigoCampania);
            if(this.lugares.isEmpty()){
                JOptionPane.showMessageDialog(this, "La campania no posee lugares de donacion asignados", "", JOptionPane.INFORMATION_MESSAGE);
                this.dispose();
            } else {
                JLabel label = new JLabel("Lugar de origen:");
                label.setAlignmentX(JLabel.CENTER_ALIGNMENT);
                label.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
                formPanel.add(label);
                comboBox = new JComboBox<>(
                        lugares.stream()
                                .map(LugarDeDonacion::getNombreYDireccion)
                                .collect(Collectors.toList())
                                .toArray(new String[]{})
                );
                comboBox.setSelectedIndex(0);
                formPanel.add(comboBox);
                /* Botones */
                JPanel panel = new JPanel(new BorderLayout());
                panel.setBorder(BorderFactory.createEmptyBorder(25, 10, 10, 10));
                formPanel.add(panel);

                /* Guardar */
                JButton guardarButton = new JButton("Guardar");
                guardarButton.addActionListener(evento -> this.guardar());
                panel.add(guardarButton, BorderLayout.EAST);

                /* Cancelar */
                JButton cancelarButton = new JButton("Cancelar");
                cancelarButton.addActionListener(evento -> this.dispose());
                panel.add(cancelarButton, BorderLayout.WEST);

                pack();
                setVisible(true);
            }

        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.dispose();
        }
    }

    private void guardar() {


        try {
            selectorDeFecha.validate();
            this.onSave.save(lugares.get(comboBox.getSelectedIndex()).getNombre(), descripcionField.getText(), DateHelper.fechaAString(selectorDeFecha.getDate()));
            this.setVisible(false);
            this.dispose();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,
                    "error:" + e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }


    }
}
