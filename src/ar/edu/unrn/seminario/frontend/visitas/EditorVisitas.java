package ar.edu.unrn.seminario.frontend.visitas;

import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.helpers.DateHelper;
import ar.edu.unrn.seminario.backend.modelo.Institucion;
import ar.edu.unrn.seminario.frontend.interfaces_funcionales.OnSaveCampania;
import ar.edu.unrn.seminario.frontend.interfaces_funcionales.OnSaveVisita;
import ar.edu.unrn.seminario.frontend.lugares.SeleccionDeLugares;
import ar.edu.unrn.seminario.frontend.varios.SelectorDeFecha;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.text.ParseException;
import java.util.Set;
import java.util.stream.Collectors;

public class EditorVisitas extends JFrame {

    private OnSaveVisita onSave;
    private JTextArea observacionTextArea;
    private SelectorDeFecha selectorDeFecha;
    private static final IFacade facade = new ApiFacade();
    private boolean esNuevaVisita = false;

    public EditorVisitas(OnSaveVisita onSave) {
        this("", "", onSave);
        this.esNuevaVisita = true;
    }

    public EditorVisitas(String observacion, String fecha, OnSaveVisita onSave) {
        this.onSave = onSave;

        /* Ventana */
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
        add(contentPane);


        setTitle("Alta de campaña");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(160, 160, 450, 250);
        this.setResizable(false);

        /* Form panel */
        JPanel formPanel = new JPanel();

        formPanel.setLayout(new BoxLayout(formPanel, BoxLayout.Y_AXIS));
        contentPane.add(formPanel);

        /* descripcion */
        JLabel descripcionLabel = new JLabel("Observacion:");
        descripcionLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        formPanel.add(descripcionLabel);
        observacionTextArea = new JTextArea(observacion, 5, 20);
        observacionTextArea.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createCompoundBorder(
                                BorderFactory.createEmptyBorder(5, 5, 5, 5),
                                BorderFactory.createLineBorder(Color.DARK_GRAY)
                        ),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        observacionTextArea.setBackground(this.getBackground());
        formPanel.add(observacionTextArea);

        /* Fecha de inicio */
        try {
            selectorDeFecha = new SelectorDeFecha(fecha);
        } catch (ParseException e) {
            selectorDeFecha = new SelectorDeFecha();
        }
        selectorDeFecha.setBorder(
                BorderFactory.createTitledBorder("Fecha"));
        formPanel.add(selectorDeFecha);

        /* Botones */
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(BorderFactory.createEmptyBorder(25, 10, 10, 10));
        formPanel.add(panel);

        /* Guardar */
        JButton guardarButton = new JButton("Guardar");
        guardarButton.addActionListener(evento -> this.guardar());
        panel.add(guardarButton, BorderLayout.EAST);

        /* Cancelar */
        JButton cancelarButton = new JButton("Cancelar");
        cancelarButton.addActionListener(evento -> this.dispose());
        panel.add(cancelarButton, BorderLayout.WEST);

        setVisible(true);
    }

    private void guardar() {
        selectorDeFecha.validate();
        if (!selectorDeFecha.isValid()) {
            JOptionPane.showMessageDialog(this,
                    "Fecha invalida",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        } else {
            try {
                this.onSave.save(this.observacionTextArea.getText(), DateHelper.fechaAString(this.selectorDeFecha.getDate()));
                this.setVisible(false);
                this.dispose();

            } catch (Exception e){
                JOptionPane.showMessageDialog(this,
                        "error:"+e.getMessage(),
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }

    }

}
