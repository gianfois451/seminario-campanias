package ar.edu.unrn.seminario.frontend.varios;

import ar.edu.unrn.seminario.backend.helpers.DateHelper;
import com.toedter.calendar.JDateChooser;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class SelectorDeFecha extends JDateChooser {

    public SelectorDeFecha(){
        super();
        this.setCalendar(Calendar.getInstance());
        this.setDateFormatString("d/M/y");
    }

    public SelectorDeFecha(String fecha) throws ParseException {
        this(DateHelper.stringAFecha(fecha));
    }

    private SelectorDeFecha(Date date){
        this();
        this.setDate(date);
    }

    public String getFecha() {
        return DateHelper.fechaAString(this.getDate());
    }
}
