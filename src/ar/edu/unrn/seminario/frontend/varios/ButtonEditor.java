package ar.edu.unrn.seminario.frontend.varios;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.function.Consumer;

public class ButtonEditor<T> extends DefaultCellEditor {
    private JButton button;
    private String label;
    private boolean isPushed;
    private Consumer<T> accion;
    private List<T> datos;
    private T selected;

    public ButtonEditor(JCheckBox checkBox, List<T> datos, Consumer<T> accion) {
        super(checkBox);
        this.accion = accion;
        button = new JButton();
        this.datos = datos;
        button.setOpaque(true);
        button.addActionListener(e -> fireEditingStopped());
    }

    public Component getTableCellEditorComponent(JTable table, Object value,
                                                 boolean isSelected, int row, int column) {
        if (isSelected) {
            button.setForeground(table.getSelectionForeground());
            button.setBackground(table.getSelectionBackground());
        } else {
            button.setForeground(table.getForeground());
            button.setBackground(table.getBackground());
        }
        label = (value == null) ? "" : value.toString();
        button.setText(label);
        this.selected = datos.get(row);
        isPushed = true;
        return button;
    }

    public Object getCellEditorValue() {
        if (isPushed) {
            this.accion.accept(selected);
        }
        isPushed = false;
        return label;
    }

    public boolean stopCellEditing() {
        isPushed = false;
        return super.stopCellEditing();
    }

    protected void fireEditingStopped() {
        try{
            super.fireEditingStopped();
        } catch (ArrayIndexOutOfBoundsException e){}
    }
}
