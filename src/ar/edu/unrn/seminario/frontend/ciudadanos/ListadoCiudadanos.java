package ar.edu.unrn.seminario.frontend.ciudadanos;

import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Ciudadano;
import ar.edu.unrn.seminario.frontend.varios.ButtonEditor;
import ar.edu.unrn.seminario.frontend.varios.ButtonRenderer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;

public class ListadoCiudadanos extends JFrame {
    private List<Ciudadano> ciudadanos;
    private IFacade api = new ApiFacade();
    private JTable tabla;
    private final String[] columas = {"Nombre", "Apellido", "DNI", "Editar", "Detalle"};

    public ListadoCiudadanos() {
        try {
            JPanel contentPane = new JPanel(new BorderLayout());
            add(contentPane);
            setTitle("Listado de ciudadanos");
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            setBounds(120, 120, 700, 300);
            this.cargarDatos();
            tabla = new JTable(this.generarDatosDeTabla(), columas);
            tabla.setModel(new DefaultTableModel(this.generarDatosDeTabla(), columas));
            configurarAcciones();

            JScrollPane scrollPane = new JScrollPane(tabla);
            contentPane.add(scrollPane, BorderLayout.NORTH);
            setVisible(true);
        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.setVisible(false);
            this.dispose();

        }
    }

    private Object[][] generarDatosDeTabla() {
        Object[][] datos = new Object[ciudadanos.size()][5];
        for (int i = 0; i < ciudadanos.size(); i++) {
            Ciudadano c = ciudadanos.get(i);
            datos[i][0] = c.getPersona().getNombre();
            datos[i][1] = c.getPersona().getApellido();
            datos[i][2] = c.getPersona().getDni();
            datos[i][3] = "Editar";
            datos[i][4] = "Detalle";
        }
        return datos;
    }

    private void configurarAcciones() {
        tabla.getColumn("Editar").setCellRenderer(new ButtonRenderer());
        tabla.getColumn("Editar").setCellEditor(new ButtonEditor<>(new JCheckBox(), ciudadanos, (selected) -> {
            new EditorCiudadanos(
                    selected.getPersona().getNombre(),
                    selected.getPersona().getApellido(),
                    selected.getPersona().getDni(),
                    selected.getPersona().getDireccion().getCalle(),
                    Integer.toString(selected.getPersona().getDireccion().getNumero()),
                    Double.toString(selected.getPersona().getDireccion().getLatitud()),
                    Double.toString(selected.getPersona().getDireccion().getLongitud()),
                    selected.getPersona().getDireccion().getCiudad().getNombre(),
                    (dni, nombre, apellido, calle, ciudad, numeroDireccion, latitud, longitud) -> {
                        try {
                            this.api.modificarCiudadano(selected.getPersona().getDni(), dni, nombre, apellido, calle,
                                    ciudad, numeroDireccion, latitud, longitud);
                            this.cargarDatos();
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(this,
                                    e.getMessage(),
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
            );
        }));

        tabla.getColumn("Detalle").setCellRenderer(new ButtonRenderer());
        tabla.getColumn("Detalle").setCellEditor(new ButtonEditor<>(new JCheckBox(), ciudadanos, (selected) -> {
            //TODO
        }));
    }

    private void cargarDatos() throws ErrorDeAccesoADatosException {
        try {
            ciudadanos = api.listarCiudadanos();
            if (tabla != null) {
                tabla.setModel(new DefaultTableModel(this.generarDatosDeTabla(), columas));
                configurarAcciones();
            }
        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            throw e;
        }
    }
}
