package ar.edu.unrn.seminario.frontend.lugares;

import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Institucion;
import ar.edu.unrn.seminario.backend.modelo.LugarDeDonacion;
import ar.edu.unrn.seminario.frontend.instituciones.EditorInstituciones;
import ar.edu.unrn.seminario.frontend.varios.ButtonEditor;
import ar.edu.unrn.seminario.frontend.varios.ButtonRenderer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;

public class ListadoLugares extends JFrame {
    private List<LugarDeDonacion> lugares;
    private IFacade api = new ApiFacade();
    private JTable tabla;
    private final String[] columas = {"Nombre", "Contacto", "Editar", "Detalle"};

    public ListadoLugares() {
        try {
            JPanel contentPane = new JPanel(new BorderLayout());
            add(contentPane);
            setTitle("Listado de instituciones");
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            setBounds(120, 120, 700, 300);
            this.cargarDatos();
            tabla = new JTable(this.generarDatosDeTabla(), columas);
            tabla.setModel(new DefaultTableModel(this.generarDatosDeTabla(), columas));
            configurarAcciones();

            JScrollPane scrollPane = new JScrollPane(tabla);
            contentPane.add(scrollPane, BorderLayout.NORTH);
            setVisible(true);
        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.setVisible(false);
            this.dispose();

        }
    }

    private Object[][] generarDatosDeTabla(){
        Object[][] datos = new Object[lugares.size()][4];
        for (int i = 0; i < lugares.size(); i++) {
            LugarDeDonacion lugarDeDonacion = lugares.get(i);
            datos[i][0] = lugarDeDonacion.getNombre();
            datos[i][1] = lugarDeDonacion.getContacto();
            datos[i][2] = "Editar";
            datos[i][3] = "Detalle";
        }
        return datos;
    }

    private void configurarAcciones(){
        tabla.getColumn("Editar").setCellRenderer(new ButtonRenderer());
        tabla.getColumn("Editar").setCellEditor(new ButtonEditor<>(new JCheckBox(), lugares, (selected) -> {
            new EditorLugares(
                    selected.getNombre(),
                    selected.getContacto(),
                    selected.getDireccion().getCalle(),
                    Integer.toString(selected.getDireccion().getNumero()),
                    Double.toString(selected.getDireccion().getLatitud()),
                    Double.toString(selected.getDireccion().getLongitud()),
                    selected.getDireccion().getCiudad().getNombre(),
                    (nombre, contacto, calle, ciudad, numero, latitud, longitud) -> {
                        try {
                            this.api.updateLugarDeDonacion(selected.getNombre(), nombre, contacto, calle,
                                    ciudad, numero, latitud, longitud);
                            this.cargarDatos();
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(this,
                                    e.getMessage(),
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
            );
        }));

        tabla.getColumn("Detalle").setCellRenderer(new ButtonRenderer());
        tabla.getColumn("Detalle").setCellEditor(new ButtonEditor<>(new JCheckBox(), lugares, (selected) -> {
            //TODO
        }));
    }

    private void cargarDatos() throws ErrorDeAccesoADatosException {
        try {
            lugares = api.findAllLugares();
            if(tabla != null) {
                tabla.setModel(new DefaultTableModel(this.generarDatosDeTabla(), columas));
                configurarAcciones();
            }
        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            throw e;
        }
    }

    public static void main(String[] args) {
        ListadoLugares frame = new ListadoLugares();
        frame.setVisible(true);
    }
}
