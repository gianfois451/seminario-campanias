package ar.edu.unrn.seminario.frontend.lugares;

import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.LugarDeDonacion;
import ar.edu.unrn.seminario.frontend.interfaces_funcionales.OnSaveLugares;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SeleccionDeLugares extends JFrame {

    private IFacade facade = new ApiFacade();
    private List<LugarDeDonacion> allLugares;
    private List<LugarDeDonacion> lugaresDeLaCampania;
    private Set<String> lugaresSeleccionados;
    private boolean esNuevaCampania;
    private OnSaveLugares onSave;

    public SeleccionDeLugares(OnSaveLugares onSave){
        this(null, onSave);
    }

    public SeleccionDeLugares(Long codigoDeCampania, OnSaveLugares onSave){
        this.onSave = onSave;
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
        add(contentPane);

        setTitle("Seleccion de lugares");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(150, 150, 450, 600);
        this.setResizable(false);
        this.esNuevaCampania = codigoDeCampania != null;
        try {
            this.allLugares = facade.findAllLugares();
            if(esNuevaCampania) {
                this.lugaresDeLaCampania = facade.findAllLugaresByCampania(codigoDeCampania);
                this.lugaresSeleccionados = lugaresDeLaCampania.stream().map(LugarDeDonacion::getNombre).collect(Collectors.toSet());
            } else {
                this.lugaresSeleccionados = new HashSet<>();
            }

            System.out.println(allLugares);
            allLugares.forEach(lugar -> {
                JPanel panel = new JPanel(new BorderLayout());
                contentPane.add(panel);
                panel.add(new Label(lugar.getNombreYDireccion()), BorderLayout.WEST);
                JCheckBox checkBox = new JCheckBox();
                checkBox.setSelected(lugarSeleccionado(lugar.getNombre()));
                checkBox.addActionListener(actionEvent -> {
                    if(lugarSeleccionado(lugar.getNombre())){
                        lugaresSeleccionados.remove(lugar.getNombre());
                    } else {
                        lugaresSeleccionados.add(lugar.getNombre());
                    }
                });
                panel.add(checkBox, BorderLayout.EAST);
            });


            JPanel buttonsPanel = new JPanel(new BorderLayout());

            /* Guardar */
            JButton guardarButton = new JButton("Guardar");
            guardarButton.addActionListener(evento -> this.guardar());
            buttonsPanel.add(guardarButton, BorderLayout.EAST);

            /* Cancelar */
            JButton cancelarButton = new JButton("Cancelar");
            cancelarButton.addActionListener(evento -> this.dispose());
            buttonsPanel.add(cancelarButton, BorderLayout.WEST);

            contentPane.add(buttonsPanel);

            pack();
            setVisible(true);

        } catch (ErrorDeAccesoADatosException e) {
            mostrarError(e);
        }
    }

    private void guardar(){
       this.onSave.save(this.lugaresSeleccionados);
       this.setVisible(false);
       this.dispose();
    }

    private boolean lugarSeleccionado(String nombre){
        return this.lugaresSeleccionados.contains(nombre);
    }

    private void mostrarError(Exception e){
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.setVisible(false);
    }
}
