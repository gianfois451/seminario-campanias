package ar.edu.unrn.seminario.frontend.lugares;

import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Ciudad;
import ar.edu.unrn.seminario.frontend.interfaces_funcionales.OnSaveLugarDeDonacion;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.stream.Collectors;

public class EditorLugares extends JFrame {
    private OnSaveLugarDeDonacion onSave;
    private JTextField nombreField;
    private JTextField contactoField;
    private JTextField calleField;
    private JTextField numeroField;
    private JTextField latitudField;
    private JTextField longitudField;

    private JComboBox<String> ciudadCombo;

    private static final IFacade facade = new ApiFacade();

    public EditorLugares(OnSaveLugarDeDonacion onSave) {
        this("", "", "", "", "", "", "", onSave);
    }

    public EditorLugares(String nombre, String contacto, String calle, String numero,
                         String latitud, String longitud, String ciudad, OnSaveLugarDeDonacion onSave) {
        this.onSave = onSave;

        /* Ventana */
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
        add(contentPane);


        setTitle("Alta de institucion");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(120, 120, 450, 600);

        /* Form panel */
        JPanel formPanel = new JPanel();
        formPanel.setLayout(new BoxLayout(formPanel, BoxLayout.Y_AXIS));

//        JScrollPane scrollPane = new JScrollPane();
//        scrollPane.setLayout(new ScrollPaneLayout());
//
//        scrollPane.add(formPanel);
        contentPane.add(formPanel);

        /*Nombre*/
        JLabel nombreLabel = new JLabel("Nombre:");
        nombreLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        nombreLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
        formPanel.add(nombreLabel);
        nombreField = new JTextField(nombre);
        nombreField.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.DARK_GRAY),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        nombreField.setBackground(this.getBackground());
        formPanel.add(nombreField);

        /*Contacto*/
        JLabel contactoLabel = new JLabel("Contacto:");
        contactoLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        contactoLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
        formPanel.add(contactoLabel);
        contactoField = new JTextField(contacto);
        contactoField.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.DARK_GRAY),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        contactoField.setBackground(this.getBackground());
        formPanel.add(contactoField);

        /*Calle*/
        JLabel calleLabel = new JLabel("Calle:");
        calleLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        calleLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
        formPanel.add(calleLabel);
        calleField = new JTextField(calle);
        calleField.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.DARK_GRAY),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        calleField.setBackground(this.getBackground());
        formPanel.add(calleField);

        /*Numero*/
        JLabel numeroLabel = new JLabel("Numero:");
        numeroLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        numeroLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
        formPanel.add(numeroLabel);
        numeroField = new JTextField(numero);
        numeroField.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.DARK_GRAY),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        numeroField.setBackground(this.getBackground());
        formPanel.add(numeroField);

        /*Ciudad*/
        try {
            JLabel ciudadLabel = new JLabel("Ciudad:");
            ciudadLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
            ciudadLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
            formPanel.add(ciudadLabel);
            ciudadCombo = new JComboBox<>(
                    facade.listarCiudades()
                            .stream()
                            .map(Ciudad::getNombre)
                            .collect(Collectors.toList())
                            .toArray(new String[]{})
            );
            ciudadCombo.setSelectedItem(ciudad);
            formPanel.add(ciudadCombo);
        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.dispose();
        }

        /*Latitud*/
        JLabel latitudLabel = new JLabel("Latitud:");
        latitudLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        latitudLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
        formPanel.add(latitudLabel);
        latitudField = new JTextField(latitud);
        latitudField.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.DARK_GRAY),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        latitudField.setBackground(this.getBackground());
        formPanel.add(latitudField);

        /*Longitud*/
        JLabel longitudLabel = new JLabel("Longitud:");
        longitudLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        longitudLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
        formPanel.add(longitudLabel);
        longitudField = new JTextField(longitud);
        longitudField.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.DARK_GRAY),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        longitudField.setBackground(this.getBackground());
        formPanel.add(longitudField);


        /* Botones */
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(BorderFactory.createEmptyBorder(25, 10, 10, 10));
        formPanel.add(panel);

        /* Guardar */
        JButton guardarButton = new JButton("Guardar");
        guardarButton.addActionListener(evento -> this.guardar());
        panel.add(guardarButton, BorderLayout.EAST);

        /* Cancelar */
        JButton cancelarButton = new JButton("Cancelar");
        cancelarButton.addActionListener(evento -> this.dispose());
        panel.add(cancelarButton, BorderLayout.WEST);


        setVisible(true);
    }

    private void guardar() {


        try {
            Double latitud = Double.parseDouble(latitudField.getText());
            Double longitud = Double.parseDouble(longitudField.getText());
            int numero = Integer.parseInt(numeroField.getText());

            this.onSave.save(nombreField.getText(), contactoField.getText(), calleField.getText(),
                    ciudadCombo.getSelectedItem().toString(), numero, latitud, longitud);
            this.setVisible(false);
            this.dispose();

        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this,
                    "El numero, la latitud y la longitud deben ser numericos",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,
                    "error:" + e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }


    }

}
