package ar.edu.unrn.seminario.frontend.pedidos;

import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.CambioDeEstadoException;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.exception.OrdenFinalizadaException;
import ar.edu.unrn.seminario.backend.exception.OrdenNoFinalizadaException;
import ar.edu.unrn.seminario.backend.helpers.DateHelper;
import ar.edu.unrn.seminario.backend.modelo.OrdenDeRetiro;
import ar.edu.unrn.seminario.backend.modelo.Visita;
import ar.edu.unrn.seminario.frontend.varios.ButtonEditor;
import ar.edu.unrn.seminario.frontend.varios.ButtonRenderer;
import ar.edu.unrn.seminario.frontend.visitas.EditorVisitas;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;

public class DetalleOrden extends JFrame {

    private OrdenDeRetiro orden;
    private IFacade api = new ApiFacade();
    private Long numeroDeOrden;
    private JTable jTable;
    private JButton addVisitaButton;
    private JButton finalizarOrdenButton;
    private final String[] columnas = {"Observacion", "Fecha", "Editar", "Eliminar"};

    public DetalleOrden(Long numeroDeOrden) {
        this.numeroDeOrden = numeroDeOrden;
        this.cargarDatos();

        setTitle("Detalle");
        setTitle("Detalle");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(140, 140, 700, 450);

        JPanel contentPane = new JPanel();

        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
        add(contentPane);

        JPanel panelSuperior = new JPanel(new GridLayout(1, 2));
        contentPane.add(panelSuperior);
        JPanel datosPanel = new JPanel();
        datosPanel.setLayout(new BoxLayout(datosPanel, BoxLayout.Y_AXIS));

        JLabel codigoLabel = new JLabel("Codigo de orden: " + orden.getId());
        JLabel descripcionLabel = new JLabel("Descripcion: " + orden.getPedidoDeRetiro().getDescripcion());
        JLabel personalLabel = new JLabel("Personal asignado: " + orden.getPersonalAsignado().getPersona().getNombreCompleto());
        JLabel fechaLabel = new JLabel("Fecha: " + orden.getPedidoDeRetiro().getFechaEmision());
        JLabel ciuadanoLabel = new JLabel("Ciudadano: " + orden.getPedidoDeRetiro().getCiudadano().getPersona().getNombreCompleto());
        JLabel direccionLabel = new JLabel("Direccion: " + orden.getPedidoDeRetiro().getDireccion());

        datosPanel.add(codigoLabel);
        datosPanel.add(descripcionLabel);
        datosPanel.add(personalLabel);
        datosPanel.add(fechaLabel);
        datosPanel.add(ciuadanoLabel);
        datosPanel.add(direccionLabel);
        panelSuperior.add(datosPanel);

        JPanel buttonsPanel = new JPanel(new FlowLayout());
        panelSuperior.add(buttonsPanel);

        addVisitaButton = new JButton("Agregar visita");
        addVisitaButton.setEnabled(!this.orden.estaFinalizada());
        addVisitaButton.addActionListener(
                e -> {
                    new EditorVisitas((observacion, fecha) -> {
                        try {
                            api.agregarVisita(this.orden.getId(), observacion, fecha);
                            this.cargarDatos();
                        } catch (ErrorDeAccesoADatosException | OrdenFinalizadaException ex) {
                            JOptionPane.showMessageDialog(this,
                                    ex.getMessage(),
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    });
                });
        buttonsPanel.add(addVisitaButton);

        finalizarOrdenButton = new JButton(orden.estaFinalizada() ? "Generar donacion" : "Finalizar orden");
        finalizarOrdenButton.addActionListener(
                (evt) -> {
                    if (orden.estaFinalizada()) {
                        this.generarDonacion();
                    } else {
                        int dialogResult = JOptionPane.showConfirmDialog(this, "¿Seguro que desea finalizar esta orden?", "Confirmar", JOptionPane.YES_NO_OPTION);
                        if (dialogResult == JOptionPane.YES_OPTION) {
                            try {
                                api.finalizarOrden(this.orden.getId());
                                this.orden.finalizar();
                                addVisitaButton.setEnabled(false);
                                JOptionPane.showMessageDialog(this, "Orden finalizada", "", JOptionPane.INFORMATION_MESSAGE);
                                finalizarOrdenButton.setText("Generar donacion");
                                this.repaint();
                            } catch (ErrorDeAccesoADatosException | CambioDeEstadoException e) {
                                JOptionPane.showMessageDialog(this,
                                        e.getMessage() + ". Haga al menos una visita antes de finalizar la orden",
                                        "Error",
                                        JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }
                });
        buttonsPanel.add(finalizarOrdenButton);


        JPanel visitasPanel = new JPanel();
        visitasPanel.setLayout(new BoxLayout(visitasPanel, BoxLayout.Y_AXIS));
        contentPane.add(visitasPanel);

        JLabel visitasLabel = new JLabel(orden.getVisitas().isEmpty() ? "No hay visitas registradas" : "Visitas:");
        visitasLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        visitasLabel.setBorder(BorderFactory.createEmptyBorder(5, 5, 10, 5));
        visitasPanel.add(visitasLabel);


        jTable = new JTable(this.generarDatosDeTabla(), columnas);
        jTable.setModel(new DefaultTableModel(this.generarDatosDeTabla(), columnas));


        JScrollPane scrollPane = new JScrollPane(jTable);
        visitasPanel.add(scrollPane);


        configurarAcciones();

        add(contentPane);
        this.setVisible(true);

    }

    private void generarDonacion() {
        try {
            api.generarDonacion(this.orden.getId());
            JOptionPane.showMessageDialog(this, "Donacion generada correctamnete", "", JOptionPane.INFORMATION_MESSAGE);
            finalizarOrdenButton.setEnabled(false);
        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    "Esta donacion ya fue generada",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (OrdenNoFinalizadaException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    private Object[][] generarDatosDeTabla() {
        List<Visita> visitas = orden.getVisitas();
        Object[][] datos = new Object[visitas.size()][4];
        for (int i = 0; i < visitas.size(); i++) {
            Visita p = visitas.get(i);
            datos[i][0] = p.getObservacion();
            datos[i][1] = DateHelper.fechaAString(p.getFecha());
            datos[i][2] = "Editar";
            datos[i][3] = "Eliminar";
        }
        return datos;
    }

    private void configurarAcciones() {

        jTable.getColumn("Editar").setCellRenderer(new ButtonRenderer());
        jTable.getColumn("Editar").setCellEditor(new ButtonEditor<>(new JCheckBox(), orden.getVisitas(), (selected) -> {
            new EditorVisitas(selected.getObservacion(), DateHelper.fechaAString(selected.getFecha()), (observacion, fecha) -> {
                try {
                    api.modificarVisita(selected.getId(), observacion, fecha);
                    this.cargarDatos();
                } catch (ErrorDeAccesoADatosException e) {
                    JOptionPane.showMessageDialog(this,
                            e.getMessage(),
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            });
        }));

        jTable.getColumn("Eliminar").setCellRenderer(new ButtonRenderer());
        jTable.getColumn("Eliminar").setCellEditor(new ButtonEditor<>(new JCheckBox(), orden.getVisitas(), (selected) -> {
            int dialogResult = JOptionPane.showConfirmDialog(this, "¿Seguro que desea eliminar la visita?", "Confirmar", JOptionPane.YES_NO_OPTION);
            if (dialogResult == JOptionPane.YES_OPTION) {
                try {
                    api.eliminarVisita(selected.getId());
                    this.cargarDatos();
                } catch (ErrorDeAccesoADatosException e) {
                    JOptionPane.showMessageDialog(this,
                            e.getMessage(),
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }));
    }

    private void cargarDatos() {
        try {
            orden = api.buscarOrdenPorNumeroDePedido(this.numeroDeOrden);
            if (jTable != null) {
                jTable.setModel(new DefaultTableModel(this.generarDatosDeTabla(), columnas));
                configurarAcciones();
            }
        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

}
