package ar.edu.unrn.seminario.frontend.pedidos;

import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Ciudadano;
import ar.edu.unrn.seminario.frontend.interfaces_funcionales.OnSavePedido;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

public class EditorPedidos extends JFrame {
    private OnSavePedido onSave;
    private JTextField descripcionField;
    private JCheckBox vehiculoCheckBox;
    private List<Ciudadano> ciudadanos;
    private JComboBox<String> ciudadanoCombo;

    private static final IFacade facade = new ApiFacade();

    public EditorPedidos(OnSavePedido onSave) {
        this("", true, null, "",onSave);
    }

    public EditorPedidos(String descripcion, boolean requiereVehiculo, String dniCiudadano, String numeroPedido, OnSavePedido onSave) {
        this.onSave = onSave;

        /* Ventana */
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
        add(contentPane);


        setTitle("Pedido");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(120, 120, 450, 330);

        /* Form panel */
        JPanel formPanel = new JPanel();
        formPanel.setLayout(new BoxLayout(formPanel, BoxLayout.Y_AXIS));

        contentPane.add(formPanel);

        /* numero de pedido */
        if(numeroPedido != null && !numeroPedido.isEmpty()){
            JLabel codigoLabel = new JLabel("Numero de pedido: " + numeroPedido);
            codigoLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
            formPanel.add(codigoLabel);
        }


        /*Nombre*/
        JLabel descripcionLabel = new JLabel("Descripcion:");
        descripcionLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        descripcionLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
        formPanel.add(descripcionLabel);
        descripcionField = new JTextField(descripcion);
        descripcionField.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.DARK_GRAY),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        descripcionField.setBackground(this.getBackground());
        formPanel.add(descripcionField);

        vehiculoCheckBox = new JCheckBox("Requiere vehiculo de carga:");
        vehiculoCheckBox.setSelected(requiereVehiculo);
        vehiculoCheckBox.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        formPanel.add(vehiculoCheckBox);


        /*Ciudadano*/
        try {
            this.ciudadanos = facade.listarCiudadanos();
            JLabel ciudadanoLabel = new JLabel("Ciudadano:");
            ciudadanoLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
            ciudadanoLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
            formPanel.add(ciudadanoLabel);
            ciudadanoCombo = new JComboBox<>(
                    ciudadanos.stream()
                            .map(c -> c.getPersona().getNombreCompletoConDNI())
                            .collect(Collectors.toList())
                            .toArray(new String[]{})
            );
            if (dniCiudadano != null && !dniCiudadano.isEmpty()) {
                ciudadanoCombo.setSelectedItem(ciudadanos.stream()
                        .filter(f -> f.getPersona().getDni().equals(dniCiudadano))
                        .findFirst()
                        .get()
                        .getPersona()
                        .getNombreCompletoConDNI());
            }
            formPanel.add(ciudadanoCombo);
        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.dispose();
        }


        /* Botones */
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(BorderFactory.createEmptyBorder(25, 10, 10, 10));
        formPanel.add(panel);

        /* Guardar */
        JButton guardarButton = new JButton("Guardar");
        guardarButton.addActionListener(evento -> this.guardar());
        panel.add(guardarButton, BorderLayout.EAST);

        /* Cancelar */
        JButton cancelarButton = new JButton("Cancelar");
        cancelarButton.addActionListener(evento -> this.dispose());
        panel.add(cancelarButton, BorderLayout.WEST);

        pack();
        setVisible(true);
    }

    private void guardar() {


        try {
            String dni = this.ciudadanos.stream()
                    .filter(c -> c.getPersona().getNombreCompletoConDNI()
                            .equals(ciudadanoCombo.getSelectedItem().toString()))
                    .findFirst().get().getPersona().getDni();

            this.onSave.save(dni, descripcionField.getText(), vehiculoCheckBox.isSelected());
            this.setVisible(false);
            this.dispose();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,
                    "error:" + e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }


    }
}
