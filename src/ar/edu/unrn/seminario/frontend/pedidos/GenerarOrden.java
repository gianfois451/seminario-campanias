package ar.edu.unrn.seminario.frontend.pedidos;

import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Personal;
import ar.edu.unrn.seminario.frontend.interfaces_funcionales.OnGenerarOrden;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.List;

public class GenerarOrden extends JFrame {

    private JList<String> list;
    private IFacade facade = new ApiFacade();


    public GenerarOrden(Long numeroDePedido, OnGenerarOrden onGenerarOrden) {

        /* Ventana */
        setTitle("Generar orden");
        setTitle("Generar orden");
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
        add(contentPane);

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(120, 120, 450, 330);

        try {
            List<Personal> personalList = facade.listarPersonalDisponiblePorNumeroDePedido(numeroDePedido);
            list = new JList<>(personalList.stream().map(p -> p.getPersona().getNombreCompletoConDNI()).toArray(String[]::new));
            list.addListSelectionListener(listSelectionEvent -> {
                if (!listSelectionEvent.getValueIsAdjusting() && list.getSelectedIndex() != -1) {
                    Personal personal = personalList.get(list.getSelectedIndex());
                    System.out.println(personal);
                    onGenerarOrden.ordenGenerada(personal.getPersona().getDni());
                    this.dispose();
                }
            });
            JLabel label = new JLabel("Seleccione el personal a cargo de retirar el pedido:");
            label.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
            contentPane.add(label);
            JScrollPane scrollPane = new JScrollPane(list);
            contentPane.add(scrollPane, BorderLayout.CENTER);
            setVisible(true);

        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.dispose();
        }
    }

}
