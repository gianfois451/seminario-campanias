package ar.edu.unrn.seminario.frontend.personal;

import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Ciudad;
import ar.edu.unrn.seminario.frontend.interfaces_funcionales.OnSavePersonal;
import ar.edu.unrn.seminario.frontend.personas.SeleccionarPersona;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.stream.Collectors;

public class EditorPersonal extends JFrame {

    private OnSavePersonal onSave;
    private JTextField nombreField;
    private JTextField dniField;
    private JTextField apellidoField;
    private JTextField emailField;
    private JTextField calleField;
    private JTextField numeroField;
    private JTextField latitudField;
    private JTextField longitudField;
    private JComboBox<String> ciudadCombo;
    private boolean esNuevo;
    private static final IFacade facade = new ApiFacade();

    public EditorPersonal(OnSavePersonal onSave) {
        this("", "", "", "", "", "", "", "", "", onSave);
    }

    public EditorPersonal(String nombre, String apellido, String dni, String calle, String numero,
                            String latitud, String longitud, String ciudad, String email, OnSavePersonal onSave) {
        this.onSave = onSave;
        this.esNuevo = nombre.isEmpty();
        /* Ventana */
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
        add(contentPane);


        setTitle(esNuevo ? "Alta de personal" : "Editar personal");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(120, 120, 450, 700);

        /* Form panel */
        JPanel formPanel = new JPanel();
        formPanel.setLayout(new BoxLayout(formPanel, BoxLayout.Y_AXIS));

//        JScrollPane scrollPane = new JScrollPane();
//        scrollPane.setLayout(new ScrollPaneLayout());
//
//        scrollPane.add(formPanel);


        contentPane.add(formPanel);


        if(esNuevo){
            JButton crearConPersonaButton = new JButton("Crear a partir de persona");
            crearConPersonaButton.setBorder(BorderFactory.createCompoundBorder(
                    BorderFactory.createLineBorder(Color.DARK_GRAY),
                    BorderFactory.createEmptyBorder(5, 5, 5, 5)
            ));
            crearConPersonaButton.addActionListener(evento -> this.crearConPersona());
            crearConPersonaButton.setAlignmentX(JLabel.CENTER_ALIGNMENT);
            formPanel.add(crearConPersonaButton);
        }

        /*Nombre*/
        JLabel nombreLabel = new JLabel("Nombre:");
        nombreLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        nombreLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
        formPanel.add(nombreLabel);
        nombreField = new JTextField(nombre);
        nombreField.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.DARK_GRAY),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        nombreField.setBackground(this.getBackground());
        formPanel.add(nombreField);



        /*Apellido*/
        JLabel apellidoLabel = new JLabel("Apellido:");
        apellidoLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        apellidoLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
        formPanel.add(apellidoLabel);
        apellidoField = new JTextField(apellido);
        apellidoField.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.DARK_GRAY),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        apellidoField.setBackground(this.getBackground());
        formPanel.add(apellidoField);

        /*DNI*/
        JLabel dniLabel = new JLabel("DNI:");
        dniLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
        dniLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        formPanel.add(dniLabel);
        dniField = new JTextField(dni);
        dniField.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.DARK_GRAY),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        dniField.setBackground(this.getBackground());
        formPanel.add(dniField);

        /*Email*/
        JLabel emailLabel = new JLabel("Email:");
        emailLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
        emailLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        formPanel.add(emailLabel);
        emailField = new JTextField(dni);
        emailField.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.DARK_GRAY),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        emailField.setBackground(this.getBackground());
        formPanel.add(emailField);

        /*Calle*/
        JLabel calleLabel = new JLabel("Calle:");
        calleLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        calleLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
        formPanel.add(calleLabel);
        calleField = new JTextField(calle);
        calleField.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.DARK_GRAY),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        calleField.setBackground(this.getBackground());
        formPanel.add(calleField);

        /*Numero*/
        JLabel numeroLabel = new JLabel("Numero:");
        numeroLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        numeroLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
        formPanel.add(numeroLabel);
        numeroField = new JTextField(numero);
        numeroField.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.DARK_GRAY),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        numeroField.setBackground(this.getBackground());
        formPanel.add(numeroField);

        /*Ciudad*/
        try {
            JLabel ciudadLabel = new JLabel("Ciudad:");
            ciudadLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
            ciudadLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
            formPanel.add(ciudadLabel);
            ciudadCombo = new JComboBox<>(
                    facade.listarCiudades()
                            .stream()
                            .map(Ciudad::getNombre)
                            .collect(Collectors.toList())
                            .toArray(new String[]{})
            );
            ciudadCombo.setSelectedItem(ciudad);
            formPanel.add(ciudadCombo);
        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.dispose();
        }

        /*Latitud*/
        JLabel latitudLabel = new JLabel("Latitud:");
        latitudLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        latitudLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
        formPanel.add(latitudLabel);
        latitudField = new JTextField(latitud);
        latitudField.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.DARK_GRAY),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        latitudField.setBackground(this.getBackground());
        formPanel.add(latitudField);

        /*Longitud*/
        JLabel longitudLabel = new JLabel("Longitud:");
        longitudLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        longitudLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));
        formPanel.add(longitudLabel);
        longitudField = new JTextField(longitud);
        longitudField.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.DARK_GRAY),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        longitudField.setBackground(this.getBackground());
        formPanel.add(longitudField);


        /* Botones */
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(BorderFactory.createEmptyBorder(25, 10, 10, 10));
        formPanel.add(panel);

        /* Guardar */
        JButton guardarButton = new JButton("Guardar");
        guardarButton.addActionListener(evento -> this.guardar());
        panel.add(guardarButton, BorderLayout.EAST);

        /* Cancelar */
        JButton cancelarButton = new JButton("Cancelar");
        cancelarButton.addActionListener(evento -> this.dispose());
        panel.add(cancelarButton, BorderLayout.WEST);


        setVisible(true);
    }

    private void guardar() {


        try {
            Double latitud = Double.parseDouble(latitudField.getText());
            Double longitud = Double.parseDouble(longitudField.getText());
            int numero = Integer.parseInt(numeroField.getText());

            this.onSave.save(dniField.getText(), nombreField.getText(), apellidoField.getText(), calleField.getText(),
                    ciudadCombo.getSelectedItem().toString(), numero, latitud, longitud, emailField.getText());
            this.setVisible(false);
            this.dispose();

        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this,
                    "El numero, la latitud y la longitud deben ser numericos",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,
                    "error:" + e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }


    }

    private void crearConPersona(){
        new SeleccionarPersona((dni, nombre, apellido, calle, ciudad, numeroDireccion, latitud, longitud) -> {
            this.nombreField.setText(nombre);
            this.apellidoField.setText(apellido);
            this.dniField.setText(dni);
            this.calleField.setText(calle);
            this.ciudadCombo.setSelectedItem(ciudad);
            this.numeroField.setText(Integer.toString(numeroDireccion));
            this.latitudField.setText(Double.toString(latitud));
            this.longitudField.setText(Double.toString(longitud));
        }, SeleccionarPersona.Modo.Personal);
    }
}
