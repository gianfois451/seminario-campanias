package ar.edu.unrn.seminario.frontend.interfaces_funcionales;

import java.util.Set;

@FunctionalInterface
public interface OnSaveCampania {
    void save(Long codigo, String descripcion, String motivo, String fechaInicio, String fechaFin, String institucion, Set<String> lugares);
}
