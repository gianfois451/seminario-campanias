package ar.edu.unrn.seminario.frontend.interfaces_funcionales;

import java.util.Date;

@FunctionalInterface
public interface OnSaveVisita {

    void save(String observacion, String fecha);

}
