package ar.edu.unrn.seminario.frontend.interfaces_funcionales;

@FunctionalInterface
public interface OnSavePersonal {
    void save(String dni, String nombre, String apellido, String calle, String ciudad,
              int numeroDireccion, double latitud, double longitud, String email);
}
