package ar.edu.unrn.seminario.frontend.interfaces_funcionales;

@FunctionalInterface
public interface OnGenerarOrden {

    void ordenGenerada(String dniPersonal);

}
