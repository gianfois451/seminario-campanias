package ar.edu.unrn.seminario.frontend.interfaces_funcionales;

@FunctionalInterface
public interface OnSaveLugarDeDonacion {
    void save(String nombre, String contacto, String calle, String ciudad, int numero,
              double latitud, double longitud);
}
