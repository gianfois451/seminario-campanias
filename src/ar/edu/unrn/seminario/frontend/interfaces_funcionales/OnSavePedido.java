package ar.edu.unrn.seminario.frontend.interfaces_funcionales;
@FunctionalInterface
public interface OnSavePedido {
    void save(String dniCiudadado, String descripcion, boolean requiereVehiculoDeCarga);
}
