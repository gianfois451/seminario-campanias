package ar.edu.unrn.seminario.frontend.interfaces_funcionales;

import java.util.Set;

@FunctionalInterface
public interface OnSaveLugares {
    void save(Set<String> nombres);
}
