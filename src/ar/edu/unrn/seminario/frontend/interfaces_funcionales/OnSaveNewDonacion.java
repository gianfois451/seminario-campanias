package ar.edu.unrn.seminario.frontend.interfaces_funcionales;
@FunctionalInterface
public interface OnSaveNewDonacion {
    void save(String lugar, String descripcion, String fecha);
}
