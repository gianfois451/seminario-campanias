package ar.edu.unrn.seminario.frontend.interfaces_funcionales;

@FunctionalInterface
public interface OnSaveInstitucion {
    void save(String nombre, String cuil, String contacto, String calle, String ciudad, int numero,
              double latitud, double longitud);
}
