package ar.edu.unrn.seminario.frontend.ciudades;


import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Campania;
import ar.edu.unrn.seminario.backend.modelo.Ciudad;
import ar.edu.unrn.seminario.frontend.campanias.DetalleCampania;
import ar.edu.unrn.seminario.frontend.campanias.EditorDeCampania;
import ar.edu.unrn.seminario.frontend.varios.ButtonEditor;
import ar.edu.unrn.seminario.frontend.varios.ButtonRenderer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;

public class ListadoCiudades extends JFrame {
    private List<Ciudad> ciudades;
    private IFacade api = new ApiFacade();
    private JTable tabla;
    private final String[] columas = {"Nombre", "Editar"};

    public ListadoCiudades() {
        try {
            JPanel contentPane = new JPanel(new BorderLayout());
            add(contentPane);
            setTitle("Listado de campañas");
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            setBounds(120, 120, 700, 300);
            this.cargarDatos();
            tabla = new JTable(this.generarDatosDeTabla(), columas);
            tabla.setModel(new DefaultTableModel(this.generarDatosDeTabla(), columas));
            configurarAcciones();


            JScrollPane scrollPane = new JScrollPane(tabla);
            contentPane.add(scrollPane, BorderLayout.NORTH);
            setVisible(true);
        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.setVisible(false);
            this.dispose();
        }
    }

    private Object[][] generarDatosDeTabla(){
        Object[][] datos = new Object[ciudades.size()][2];
        for (int i = 0; i < ciudades.size(); i++) {
            Ciudad c = ciudades.get(i);
            datos[i][0] = c.getNombre();
            datos[i][1] = "Editar";
        }
        return datos;
    }

    private void configurarAcciones(){
        tabla.getColumn("Editar").setCellRenderer(new ButtonRenderer());
        tabla.getColumn("Editar").setCellEditor(new ButtonEditor<>(new JCheckBox(), ciudades, (selected) -> {
            String nombreAntiguo = selected.getNombre();
            new EditorDeCiudad(
                    selected.getNombre(),
                    (nombre) -> {
                        try {
                            this.api.updateCiudad(nombreAntiguo, nombre);
                            this.cargarDatos();
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(this,
                                    e.getMessage(),
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
            );
        }));
    }

    private void cargarDatos() throws ErrorDeAccesoADatosException {
        try {
            ciudades = api.listarCiudades();
            if(tabla != null) {
                tabla.setModel(new DefaultTableModel(this.generarDatosDeTabla(), columas));
                configurarAcciones();
            }
        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            throw e;
        }
    }

    public static void main(String[] args) {
        ListadoCiudades frame = new ListadoCiudades();
        frame.setVisible(true);
    }







}

