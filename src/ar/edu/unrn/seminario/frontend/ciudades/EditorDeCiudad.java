package ar.edu.unrn.seminario.frontend.ciudades;

import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.frontend.interfaces_funcionales.OnSaveCiudad;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class EditorDeCiudad extends JFrame {

    private OnSaveCiudad onSave;
    private JTextField nombreTextField;
    private static final IFacade facade = new ApiFacade();

    public EditorDeCiudad(OnSaveCiudad onSave) {
        this("", onSave);
    }

    public EditorDeCiudad(String nombre, OnSaveCiudad onSave) {
        this.onSave = onSave;


        /* Ventana */
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
        add(contentPane);

        setTitle(nombre.isEmpty() ? "Alta de ciudad" : "Editar ciudad");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(120, 120, 450, 150);
        this.setResizable(false);

        /* Form panel */
        JPanel formPanel = new JPanel();
        formPanel.setLayout(new BoxLayout(formPanel, BoxLayout.Y_AXIS));
        contentPane.add(formPanel);

        /* nombre */
        JLabel nombreLabel = new JLabel("Nombre:");
        nombreLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        formPanel.add(nombreLabel);
        nombreTextField = new JTextField(nombre);
        nombreTextField.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createCompoundBorder(
                                BorderFactory.createEmptyBorder(5, 5, 5, 5),
                                BorderFactory.createLineBorder(Color.DARK_GRAY)
                        ),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        nombreTextField.setBackground(this.getBackground());
        formPanel.add(nombreTextField);

        /* Botones */
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(BorderFactory.createEmptyBorder(25, 10, 10, 10));
        formPanel.add(panel);

        /* Guardar */
        JButton guardarButton = new JButton("Guardar");
        guardarButton.addActionListener(evento -> this.guardar());
        panel.add(guardarButton, BorderLayout.EAST);

        /* Cancelar */
        JButton cancelarButton = new JButton("Cancelar");
        cancelarButton.addActionListener(evento -> this.dispose());
        panel.add(cancelarButton, BorderLayout.WEST);


        setVisible(true);
    }

    private void guardar() {
            this.onSave.save(nombreTextField.getText());
            this.setVisible(false);
            this.dispose();
    }

    public static void main(String[] args) {
        EditorDeCiudad editor = new EditorDeCiudad("Test", System.out::println);
        editor.setVisible(true);
    }
}
