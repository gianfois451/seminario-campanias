package ar.edu.unrn.seminario.frontend;

import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.frontend.campanias.EditorDeCampania;
import ar.edu.unrn.seminario.frontend.campanias.ListadoCampanias;
import ar.edu.unrn.seminario.frontend.ciudadanos.EditorCiudadanos;
import ar.edu.unrn.seminario.frontend.ciudadanos.ListadoCiudadanos;
import ar.edu.unrn.seminario.frontend.ciudades.EditorDeCiudad;
import ar.edu.unrn.seminario.frontend.ciudades.ListadoCiudades;
import ar.edu.unrn.seminario.frontend.instituciones.EditorInstituciones;
import ar.edu.unrn.seminario.frontend.instituciones.ListadoInstituciones;
import ar.edu.unrn.seminario.frontend.lugares.EditorLugares;
import ar.edu.unrn.seminario.frontend.lugares.ListadoLugares;

import javax.swing.*;
import javax.swing.event.InternalFrameAdapter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.HashSet;

public class VentanaPrincipal extends JFrame {

    private static final IFacade facade = new ApiFacade();

    private VentanaPrincipal() {

        /* Ventana */
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        setLayout(new BorderLayout());

        setTitle("Sistema de gestion de campañas de donación");


        /* Menu bar */
        JMenuBar menuBar = new JMenuBar();
        JMenu menuCampania = new JMenu("Campañas");
        JMenu menuCiudadanos = new JMenu("Ciudadanos");
        JMenu menuInstituciones = new JMenu("Instituciones");
        JMenu menuLugares = new JMenu("Lugares de donación");
        JMenu menuCiudades = new JMenu("Ciudades");
        menuBar.add(menuCampania);
        menuBar.add(menuCiudadanos);
        menuBar.add(menuInstituciones);
        menuBar.add(menuCiudades);
        menuBar.add(menuLugares);
        setJMenuBar(menuBar);

        /* items campania */
        JMenuItem altaModificacionCampaniasMenuItem = new JMenuItem("Crear campaña");
        JMenuItem listadoCampaniasMenuItem = new JMenuItem("Listado de campañas");
        altaModificacionCampaniasMenuItem.addActionListener((ActionEvent e) -> this.crearCampania());
        listadoCampaniasMenuItem.addActionListener((ActionEvent e) -> new ListadoCampanias());
        menuCampania.add(altaModificacionCampaniasMenuItem);
        menuCampania.add(listadoCampaniasMenuItem);

        /* items ciudadanos */
        JMenuItem altaCiudadanoMenuItem = new JMenuItem("Alta de ciudadano");
        altaCiudadanoMenuItem.addActionListener(actionEvent -> {
            new EditorCiudadanos((dni, nombre, apellido, calle, ciudad, numeroDireccion, latitud, longitud) -> {
                try {
                    facade.registrarCiudadano(dni, nombre, apellido, calle, ciudad, numeroDireccion, latitud, longitud);
                } catch (ErrorDeAccesoADatosException e) {
                    JOptionPane.showMessageDialog(this,
                            e.getMessage(),
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            });
        });

        JMenuItem listadoCiudadanosMenuItem = new JMenuItem("Listado");
        listadoCiudadanosMenuItem.addActionListener(actionEvent -> new ListadoCiudadanos());
        menuCiudadanos.add(altaCiudadanoMenuItem);
        menuCiudadanos.add(listadoCiudadanosMenuItem);

        /* items instituciones */
        JMenuItem altaInstitucionMenuItem = new JMenuItem("Alta de institución");
        JMenuItem listadoInstitucionesMenuItem = new JMenuItem("Listado");
        listadoInstitucionesMenuItem.addActionListener(actionEvent -> new ListadoInstituciones());
        altaInstitucionMenuItem.addActionListener(actionEvent -> {
            new EditorInstituciones((nombre, cuil, contacto, calle, ciudad, numero, latitud, longitud) -> {
                try {
                    facade.altaDeInstitucion(nombre, cuil, contacto, calle, ciudad, numero, latitud, longitud);
                } catch (ErrorDeAccesoADatosException e) {
                    JOptionPane.showMessageDialog(this,
                            e.getMessage(),
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            });
        });
        menuInstituciones.add(altaInstitucionMenuItem);
        menuInstituciones.add(listadoInstitucionesMenuItem);

        /* items lugares */
        JMenuItem altaLugarMenuItem = new JMenuItem("Alta de lugar de donación");
        JMenuItem listadoLugaresMenuItem = new JMenuItem("Listado de lugares");
        listadoLugaresMenuItem.addActionListener(actionEvent -> new ListadoLugares());
        altaLugarMenuItem.addActionListener(actionEvent -> {
            new EditorLugares((nombre, contacto, calle, ciudad, numero, latitud, longitud) -> {
                try {
                    facade.crearLugarDeDonacion(nombre, contacto, calle, ciudad, numero, latitud, longitud);
                } catch (ErrorDeAccesoADatosException e) {
                    JOptionPane.showMessageDialog(this,
                            e.getMessage(),
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            });
        });
        menuLugares.add(altaLugarMenuItem);
        menuLugares.add(listadoLugaresMenuItem);


        /* items ciudades */

        JMenuItem altaCiudad = new JMenuItem("Nueva ciudad");
        JMenuItem listadoCiudades = new JMenuItem("Listado de ciudades");
        altaCiudad.addActionListener((ActionEvent e) -> {
            new EditorDeCiudad(nombre -> {
                try {
                    facade.crearCiudad(nombre);
                } catch (ErrorDeAccesoADatosException ex) {
                    JOptionPane.showMessageDialog(this,
                            "error:" + ex.getMessage(),
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            });
        });
        listadoCiudades.addActionListener((ActionEvent e) -> new ListadoCiudades());
        menuCiudades.add(altaCiudad);
        menuCiudades.add(listadoCiudades);

    }

    public static void main(String[] args) {
            VentanaPrincipal frame = new VentanaPrincipal();
            frame.setVisible(true);
    }

    private void crearCampania(){
        new EditorDeCampania(
                (codigo, descripcion, motivo, fechaInicio, fechaFin, institucion, nombresLugares) -> {
                    try {
                        facade.altaDeCampania(codigo, descripcion, motivo, fechaInicio, fechaFin, institucion,
                                nombresLugares != null ? nombresLugares : new HashSet<>());
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(this,
                                e.getMessage(),
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
        );
    }
}
