package ar.edu.unrn.seminario.frontend.campanias;


import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Campania;
import ar.edu.unrn.seminario.frontend.varios.ButtonEditor;
import ar.edu.unrn.seminario.frontend.varios.ButtonRenderer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.util.List;
import java.util.function.Consumer;

public class ListadoCampanias extends JFrame {
    private List<Campania> campanias;
    private IFacade api = new ApiFacade();
    private JTable tabla;
    private final String[] columas = {"Codigo","Descripcion", "Motivo", "Fecha de inicio", "Fecha de fin","Institucion", "Editar", "Detalle"};

    public ListadoCampanias() {
        try {
            JPanel contentPane = new JPanel(new BorderLayout());
            add(contentPane);
            setTitle("Listado de campañas");
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            setBounds(120, 120, 700, 300);
            this.cargarDatos();
            tabla = new JTable(this.generarDatosDeTabla(), columas);
            tabla.setModel(new DefaultTableModel(this.generarDatosDeTabla(), columas));
            configurarAcciones();


            JScrollPane scrollPane = new JScrollPane(tabla);
            contentPane.add(scrollPane, BorderLayout.NORTH);
            setVisible(true);
        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.setVisible(false);
            this.dispose();
        }
    }

    private Object[][] generarDatosDeTabla(){
        Object[][] datos = new Object[campanias.size()][8];
        for (int i = 0; i < campanias.size(); i++) {
            Campania c = campanias.get(i);
            datos[i][0] = c.getId();
            datos[i][1] = c.getDescripcion();
            datos[i][2] = c.getMotivo();
            datos[i][3] = c.getFechaInicioComoString();
            datos[i][4] = c.getFechaFinComoString();
            datos[i][5] = c.getInstitucion().getNombre();
            datos[i][6] = "Editar";
            datos[i][7] = "Detalle";
        }
        return datos;
    }

    private void configurarAcciones(){
        tabla.getColumn("Editar").setCellRenderer(new ButtonRenderer());
        tabla.getColumn("Editar").setCellEditor(new ButtonEditor<>(new JCheckBox(), campanias, (selected) -> {
            new EditorDeCampania(
                    selected.getId().toString(),
                    selected.getDescripcion(),
                    selected.getMotivo(),
                    selected.getFechaInicioComoString(),
                    selected.getFechaFinComoString(),
                    (codigo, descripcion, motivo, fechaInicio, fechaFin, institucion, nombres) -> {
                        try {
                            this.api.actualizarCampania(codigo, descripcion, motivo, fechaInicio, fechaFin, nombres);
                            this.cargarDatos();
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(this,
                                    e.getMessage(),
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
            );
        }));

        tabla.getColumn("Detalle").setCellRenderer(new ButtonRenderer());
        tabla.getColumn("Detalle").setCellEditor(new ButtonEditor<>(new JCheckBox(), campanias, (selected) -> {
            new DetalleCampania(selected.getId().toString(), selected.getDescripcion(), selected.getMotivo(),
                    selected.getFechaInicioComoString(), selected.getFechaFinComoString());
        }));
    }

    private void cargarDatos() throws ErrorDeAccesoADatosException {
        try {
            campanias = api.listarCampanias();
            if(tabla != null) {
                tabla.setModel(new DefaultTableModel(this.generarDatosDeTabla(), columas));
                configurarAcciones();
            }
        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            throw e;
        }
    }

    public static void main(String[] args) {
        ListadoCampanias frame = new ListadoCampanias();
        frame.setVisible(true);
    }







}

