package ar.edu.unrn.seminario.frontend.campanias;

import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.helpers.DateHelper;
import ar.edu.unrn.seminario.backend.modelo.LugarDeDonacion;
import ar.edu.unrn.seminario.backend.modelo.OrdenDeRetiro;
import ar.edu.unrn.seminario.backend.modelo.PedidoDeRetiro;
import ar.edu.unrn.seminario.frontend.donaciones.DetalleDonaciones;
import ar.edu.unrn.seminario.frontend.pedidos.DetalleOrden;
import ar.edu.unrn.seminario.frontend.pedidos.EditorPedidos;
import ar.edu.unrn.seminario.frontend.pedidos.GenerarOrden;
import ar.edu.unrn.seminario.frontend.varios.ButtonEditor;
import ar.edu.unrn.seminario.frontend.varios.ButtonRenderer;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

public class DetalleCampania extends JFrame {

    private List<PedidoDeRetiro> pedidos;
    private List<OrdenDeRetiro> ordenesDeRetiro;
    private IFacade api = new ApiFacade();
    private Long codigo;
    private List<LugarDeDonacion> lugares;
    private JTable jTable;
    private JTable jTableOrdenes;
    private final String[] columnas = {"Descripcion", "Fecha", "Requiere vehiculo", "Editar", "Generar orden"};
    private final String[] columnasOrdenes = {"Descripcion", "Personal asignado", "Estado", "Fecha", "Requiere vehiculo", "Editar", "Detalle"};

    public DetalleCampania(String codigo, String descripcion, String motivo, String fechaInicio, String fechaFin) {

        this.codigo = Long.parseLong(codigo);
        this.cargarDatos();

        setTitle("Detalle");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(120, 120, 700, 450);

        JPanel contentPane = new JPanel();

        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
        add(contentPane);

        JPanel panelSuperior = new JPanel(new GridLayout(1, 2));
        contentPane.add(panelSuperior);
        JPanel datosPanel = new JPanel();
        datosPanel.setLayout(new BoxLayout(datosPanel, BoxLayout.Y_AXIS));

        JLabel codigoLabel = new JLabel("Codigo: " + codigo);
        JLabel descripcionLabel = new JLabel("Descripcion: " + descripcion);
        JLabel motivoLabel = new JLabel("Motivo: " + motivo);
        JLabel desdeLabel = new JLabel("Desde: " + fechaInicio);
        JLabel hastaLabel = new JLabel("Hasta: " + fechaFin);
        JLabel lugaresLabel = new JLabel("Lugares de donacion: " + lugares.stream().map(LugarDeDonacion::getNombre).collect(Collectors.joining(", ")));

        datosPanel.add(codigoLabel);
        datosPanel.add(descripcionLabel);
        datosPanel.add(motivoLabel);
        datosPanel.add(desdeLabel);
        datosPanel.add(hastaLabel);

        if (lugares.size() > 0) {
            datosPanel.add(lugaresLabel);
        }

        panelSuperior.add(datosPanel);

        JPanel buttonsPanel = new JPanel(new FlowLayout());
        panelSuperior.add(buttonsPanel);

        JButton addPedidoButton = new JButton("Nuevo pedido");
        addPedidoButton.addActionListener(
                e -> new EditorPedidos((dniCiudadado, descripcion_pedido, requiereVehiculoDeCarga) -> {
                    try {
                        api.generarPedidoDeRetiro(Long.parseLong(codigo), dniCiudadado, descripcion_pedido, requiereVehiculoDeCarga);
                        this.cargarDatos();
                    } catch (ErrorDeAccesoADatosException ex) {
                        JOptionPane.showMessageDialog(this,
                                ex.getMessage(),
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                        this.dispose();
                    }
                }));
        buttonsPanel.add(addPedidoButton);

        JButton donacionesButton = new JButton("Ver donaciones");
        donacionesButton.addActionListener(
                e -> {
                    new DetalleDonaciones(this.codigo);
                });
        buttonsPanel.add(donacionesButton);

        JPanel pedidosPanel = new JPanel();
        pedidosPanel.setLayout(new BoxLayout(pedidosPanel, BoxLayout.Y_AXIS));
        contentPane.add(pedidosPanel);

        JLabel pedidosLabel = new JLabel(pedidos.isEmpty() ? "No hay pedidos pendientes" : "Pedidos pendientes:");
        pedidosLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        pedidosLabel.setBorder(BorderFactory.createEmptyBorder(5, 5, 10, 5));
        pedidosPanel.add(pedidosLabel);


        jTable = new JTable(this.generarDatosDeTabla(), columnas);
        jTable.setModel(new DefaultTableModel(this.generarDatosDeTabla(), columnas));


        JScrollPane scrollPane = new JScrollPane(jTable);
        pedidosPanel.add(scrollPane);

        JPanel ordenesPanel = new JPanel();
        ordenesPanel.setLayout(new BoxLayout(ordenesPanel, BoxLayout.Y_AXIS));
        contentPane.add(ordenesPanel);

        JLabel ordenesLabel = new JLabel(ordenesDeRetiro.isEmpty() ? "No hay ordenes" : "Ordenes:");
        ordenesLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        ordenesLabel.setBorder(BorderFactory.createEmptyBorder(5, 5, 10, 5));
        ordenesPanel.add(ordenesLabel);


        jTableOrdenes = new JTable(this.generarDatosDeTablaOrdenes(), columnasOrdenes);
        jTableOrdenes.setModel(new DefaultTableModel(this.generarDatosDeTablaOrdenes(), columnasOrdenes));

        configurarAcciones();

        JScrollPane scrollPaneOrdenes = new JScrollPane(jTableOrdenes);
        ordenesPanel.add(scrollPaneOrdenes);

        contentPane.add(ordenesPanel);

        add(contentPane);
        this.setVisible(true);

    }

    private Object[][] generarDatosDeTabla() {
        Object[][] datos = new Object[pedidos.size()][5];
        for (int i = 0; i < pedidos.size(); i++) {
            PedidoDeRetiro p = pedidos.get(i);
            datos[i][0] = p.getDescripcion();
            datos[i][1] = DateHelper.fechaAString(p.getFechaEmision());
            datos[i][2] = p.isRequiereVehiculoDeCarga() ? "SI" : "NO";
            datos[i][3] = "Editar";
            datos[i][4] = "Generar orden";
        }
        return datos;
    }

    private Object[][] generarDatosDeTablaOrdenes() {
        Object[][] datos = new Object[ordenesDeRetiro.size()][7];
        for (int i = 0; i < ordenesDeRetiro.size(); i++) {
            OrdenDeRetiro o = ordenesDeRetiro.get(i);
            datos[i][0] = o.getPedidoDeRetiro().getDescripcion();
            datos[i][1] = o.getPersonalAsignado().getPersona().getNombreCompleto();
            datos[i][2] = o.getEstadoString();
            datos[i][3] = DateHelper.fechaAString(o.getFechaEmision());
            datos[i][4] = o.getPedidoDeRetiro().isRequiereVehiculoDeCarga() ? "SI" : "NO";
            datos[i][5] = "Editar";
            datos[i][6] = "Detalle";
        }
        return datos;
    }

    private void configurarAcciones() {

        jTable.getColumn("Editar").setCellRenderer(new ButtonRenderer());
        jTable.getColumn("Editar").setCellEditor(new ButtonEditor<>(new JCheckBox(), pedidos, (selected) -> {
            new EditorPedidos(
                    selected.getDescripcion(),
                    selected.isRequiereVehiculoDeCarga(),
                    selected.getCiudadano().getPersona().getDni(),
                    selected.getId().toString(),
                    (dniCiudadado, descripcionPedido, requiereVehiculoDeCarga) -> {
                        try {
                            this.api.modificarPedidoDeRetiro(selected.getId(), dniCiudadado, descripcionPedido, requiereVehiculoDeCarga);
                            this.cargarDatos();
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(this,
                                    e.getMessage(),
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
            );
        }));

        jTable.getColumn("Generar orden").setCellRenderer(new ButtonRenderer());
        jTable.getColumn("Generar orden").setCellEditor(new ButtonEditor<>(new JCheckBox(), pedidos, (selected) -> {
            new GenerarOrden(selected.getId(), (dniPersonal -> {
                try {
                    api.generarOrdenDeRetiro(selected.getId(), dniPersonal);
                    JOptionPane.showMessageDialog(this, "Orden generada correctamente",
                            "Orden generada", JOptionPane.INFORMATION_MESSAGE);
                    this.cargarDatos();
                } catch (ErrorDeAccesoADatosException e) {
                    JOptionPane.showMessageDialog(this,
                            e.getMessage(),
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }));
        }));

        jTableOrdenes.getColumn("Editar").setCellRenderer(new ButtonRenderer());
        jTableOrdenes.getColumn("Editar").setCellEditor(new ButtonEditor<>(new JCheckBox(), ordenesDeRetiro, (selected) -> {
            new GenerarOrden(selected.getId(), (dniPersonal -> {
                try {
                    api.reasignarOrdenDeRetiro(selected.getId(), dniPersonal);
                    JOptionPane.showMessageDialog(this, "Orden reasignada correctamente",
                            "Orden generada", JOptionPane.INFORMATION_MESSAGE);

                    this.cargarDatos();
                } catch (ErrorDeAccesoADatosException e) {
                    JOptionPane.showMessageDialog(this,
                            e.getMessage(),
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }));
        }));

        jTableOrdenes.getColumn("Detalle").setCellRenderer(new ButtonRenderer());
        jTableOrdenes.getColumn("Detalle").setCellEditor(new ButtonEditor<>(new JCheckBox(), ordenesDeRetiro, (selected) -> {
            new DetalleOrden(selected.getId());
        }));
    }

    private void cargarDatos() {
        try {
            pedidos = api.listarPedidosDeRetiroPorCampania(codigo);
            if (jTable != null) {
                jTable.setModel(new DefaultTableModel(this.generarDatosDeTabla(), columnas));
                configurarAcciones();
            }
            lugares = api.findAllLugaresByCampania(codigo);
            ordenesDeRetiro = api.listarOrdenesPorCampania(codigo);
            if (jTableOrdenes != null) {
                jTableOrdenes.setModel(new DefaultTableModel(this.generarDatosDeTablaOrdenes(), columnasOrdenes));
                configurarAcciones();
            }
        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

}
