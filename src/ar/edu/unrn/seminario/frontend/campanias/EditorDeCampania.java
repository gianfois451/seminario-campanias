package ar.edu.unrn.seminario.frontend.campanias;

import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Institucion;
import ar.edu.unrn.seminario.frontend.interfaces_funcionales.OnSaveCampania;
import ar.edu.unrn.seminario.frontend.lugares.SeleccionDeLugares;
import ar.edu.unrn.seminario.frontend.varios.SelectorDeFecha;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.text.ParseException;
import java.util.Set;
import java.util.stream.Collectors;

public class EditorDeCampania extends JFrame {

    private OnSaveCampania onSave;
    private JTextArea descripcionTextArea;
    private JTextArea motivoTextArea;
    private JTextField codigoField;
    private JComboBox<String> institucionCombo;
    private SelectorDeFecha selectorDeFechaInicio;
    private SelectorDeFecha selectorDeFechaFin;
    private static final IFacade facade = new ApiFacade();
    private boolean esNuevaCampania = false;
    private Set<String> lugaresSeleccionados = null;

    public EditorDeCampania(OnSaveCampania onSave) {
        this("", "", "", "", "", onSave);
        this.esNuevaCampania = true;
    }

    public EditorDeCampania(String codigo, String descripcion, String motivo, String fechaInicio, String fechaFin, OnSaveCampania onSave) {
        this.onSave = onSave;

        /*solo puede editar institucion en la creacion, cuando el codigo viene vacio */
        boolean puedeEditarInstitucion = codigo.isEmpty();

        /* Ventana */
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
        add(contentPane);


        setTitle(codigo.isEmpty() ? "Alta de campaña" : "Editar campaña");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(120, 120, 450, 450);
        this.setResizable(false);

        /* Form panel */
        JPanel formPanel = new JPanel();

        formPanel.setLayout(new BoxLayout(formPanel, BoxLayout.Y_AXIS));
        contentPane.add(formPanel);

        /* codigo */
        JLabel codigoLabel = new JLabel("Codigo numerico:");
        codigoLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        formPanel.add(codigoLabel);
        codigoField = new JTextField(codigo);
        codigoField.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createCompoundBorder(
                                BorderFactory.createEmptyBorder(5, 5, 5, 5),
                                BorderFactory.createLineBorder(Color.DARK_GRAY)
                        ),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        codigoField.setBackground(this.getBackground());
        if (!codigo.isEmpty()) codigoField.disable();
        formPanel.add(codigoField);

        /* descripcion */
        JLabel descripcionLabel = new JLabel("Descripcion:");
        descripcionLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        formPanel.add(descripcionLabel);
        descripcionTextArea = new JTextArea(descripcion, 5, 20);
        descripcionTextArea.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createCompoundBorder(
                                BorderFactory.createEmptyBorder(5, 5, 5, 5),
                                BorderFactory.createLineBorder(Color.DARK_GRAY)
                        ),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        descripcionTextArea.setBackground(this.getBackground());
        formPanel.add(descripcionTextArea);

        /* motivo */
        JLabel motivoLabel = new JLabel("Motivo (opcional):");
        motivoLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        formPanel.add(motivoLabel);
        motivoTextArea = new JTextArea(motivo, 5, 20);
        motivoTextArea.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createCompoundBorder(
                                BorderFactory.createEmptyBorder(5, 5, 5, 5),
                                BorderFactory.createLineBorder(Color.DARK_GRAY)
                        ),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)
                )
        );
        motivoTextArea.setBackground(this.getBackground());
        formPanel.add(motivoTextArea);

        if (puedeEditarInstitucion) {
            JLabel institucionLabel = new JLabel("Institucion:");
            institucionLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
            formPanel.add(institucionLabel);
            try {
                institucionCombo = new JComboBox<>(
                        facade.listarInstituciones()
                                .stream()
                                .map(Institucion::getNombre)
                                .collect(Collectors.toList())
                                .toArray(new String[]{})
                );
            } catch (ErrorDeAccesoADatosException e) {
                JOptionPane.showMessageDialog(this,
                        e.getMessage(),
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
                this.setVisible(false);
                this.dispose();
            }
            institucionCombo.setBorder(
                    BorderFactory.createEmptyBorder(5, 5, 5, 5));
            formPanel.add(institucionCombo);
        }



        /* Fecha de inicio */
        try {
            selectorDeFechaInicio = new SelectorDeFecha(fechaInicio);
        } catch (ParseException e) {
            selectorDeFechaInicio = new SelectorDeFecha();
        }
        selectorDeFechaInicio.setBorder(
                BorderFactory.createTitledBorder("Fecha de inicio"));
        formPanel.add(selectorDeFechaInicio);

        /* Fecha de finalizacion */
        try {
            selectorDeFechaFin = new SelectorDeFecha(fechaFin);

        } catch (ParseException e) {
            selectorDeFechaFin = new SelectorDeFecha();
        }
        selectorDeFechaFin.setBorder(
                BorderFactory.createTitledBorder("Fecha de fin"));
        formPanel.add(selectorDeFechaFin);

        /* Botones */
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(BorderFactory.createEmptyBorder(25, 10, 10, 10));
        formPanel.add(panel);

        /* Guardar */
        JButton guardarButton = new JButton("Guardar");
        guardarButton.addActionListener(evento -> this.guardar());
        panel.add(guardarButton, BorderLayout.EAST);

        /* Cancelar */
        JButton cancelarButton = new JButton("Cancelar");
        cancelarButton.addActionListener(evento -> this.dispose());
        panel.add(cancelarButton, BorderLayout.WEST);

        JButton lugaresButton = new JButton("Seleccionar Lugares de donacion");
        lugaresButton.addActionListener(evento -> this.seleccionarLugaresDeDonacion());
        panel.add(lugaresButton, BorderLayout.CENTER);

        setVisible(true);
    }

    private void guardar() {
        selectorDeFechaFin.validate();
        selectorDeFechaInicio.validate();
        if (!selectorDeFechaInicio.isValid() || !selectorDeFechaFin.isValid()) {
            JOptionPane.showMessageDialog(this,
                    "Fechas invalidas",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        } else {

            try {
                String institucion = institucionCombo != null ? (String) institucionCombo.getSelectedItem() : null;
                Long codigo = Long.parseLong(codigoField.getText());
                this.onSave.save(codigo, descripcionTextArea.getText(), motivoTextArea.getText(),
                        selectorDeFechaInicio.getFecha(), selectorDeFechaFin.getFecha(), institucion, this.lugaresSeleccionados);
                this.setVisible(false);
                this.dispose();

            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this,
                        "El codigo debe ser un numero",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            } catch (Exception e){
                JOptionPane.showMessageDialog(this,
                        "error:"+e.getMessage(),
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }

    }

    private void seleccionarLugaresDeDonacion(){
        new SeleccionDeLugares(this.esNuevaCampania ? null : Long.parseLong(this.codigoField.getText()), nombres -> {
            this.lugaresSeleccionados = nombres;
        });
    }

    public static void main(String[] args) {
        new EditorDeCampania("5", "Desc", "Motiv", "16/09/1999", "18/10/2000",
                (codigo, descripcion, motivo, fechaInicio, fechaFin, institucion, lugares) -> {
                });
    }

}
