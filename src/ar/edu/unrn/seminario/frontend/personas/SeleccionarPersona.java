package ar.edu.unrn.seminario.frontend.personas;

import ar.edu.unrn.seminario.backend.api.ApiFacade;
import ar.edu.unrn.seminario.backend.api.IFacade;
import ar.edu.unrn.seminario.backend.exception.ErrorDeAccesoADatosException;
import ar.edu.unrn.seminario.backend.modelo.Persona;
import ar.edu.unrn.seminario.frontend.interfaces_funcionales.OnSelectPersona;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

public class SeleccionarPersona extends JFrame {

    public enum Modo {
            Ciudadanos,
            Personal
    }

    private OnSelectPersona onSelectPersona;
    private JList<String> list;
    private IFacade facade = new ApiFacade();

    public SeleccionarPersona(OnSelectPersona onSelect, Modo modo) {
        try {
            JPanel contentPane = new JPanel(new BorderLayout());
            add(contentPane);
            setTitle("Listado de personas");
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            setBounds(120, 120, 300, 300);
            List<Persona> personas;
            if(modo.equals(Modo.Ciudadanos)){
                personas = facade.listarPersonasNoCiudadanos();
            } else {
                personas = facade.listarPersonasNoPersonal();
            }
            if(personas.isEmpty()){
                JOptionPane.showMessageDialog(this,
                        modo.equals(Modo.Ciudadanos) ? "No hay personas que no sean ciudadanos" : "No hay personas que no sean personal",
                        "Sin resultados",
                        JOptionPane.INFORMATION_MESSAGE);
                this.dispose();
            } else {
                list = new JList<String>(personas.stream().map(Persona::getNombreCompletoConDNI).toArray(String[]::new));
                list.addListSelectionListener(listSelectionEvent -> {
                    if(!listSelectionEvent.getValueIsAdjusting() && list.getSelectedIndex() != -1){
                        Persona persona = personas.get(list.getSelectedIndex());
                        System.out.println(persona);
                        onSelect.select(
                                persona.getDni(),
                                persona.getNombre(),
                                persona.getApellido(),
                                persona.getDireccion().getCalle(),
                                persona.getDireccion().getCiudad().getNombre(),
                                persona.getDireccion().getNumero(),
                                persona.getDireccion().getLatitud(),
                                persona.getDireccion().getLongitud()
                        );
                        this.dispose();
                    }
                });
                JScrollPane scrollPane = new JScrollPane(list);
                contentPane.add(scrollPane, BorderLayout.CENTER);
                setVisible(true);
            }
        } catch (ErrorDeAccesoADatosException e) {
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.setVisible(false);
            this.dispose();

        }
    }
}
